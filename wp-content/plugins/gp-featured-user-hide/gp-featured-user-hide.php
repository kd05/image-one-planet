<?php
/*
Plugin Name: Hide Featured User On Home Page
Description: Hide Featured User On Home Page, which refresh Every XX Seconds
Author: Geekpower
Version: 1.0
*/




if ( ! defined( 'ABSPATH' ) ) {
    die( 'Invalid request.' );
}

if ( ! class_exists( 'GP_Hide_Users_Home_Page' ) ) :
    class GP_Hide_Users_Home_Page {

        function __construct()
        {

            add_action( 'admin_enqueue_scripts', array($this, 'gp_featured_user_hide_enqueue_script' ));
            add_action('admin_menu', array($this, 'gp_plugin_hide_featured_user_on_homepage'));


        }


        function gp_featured_user_hide_enqueue_script(){
            wp_enqueue_script(
                'gp-featured-user-hide',
                plugins_url( 'js/user-hide.js', __FILE__ ),
                array('jquery')
            );
        }


        function gp_plugin_hide_featured_user_on_homepage(){
            add_menu_page( 'User Hide on Home', 'Home - Hide Users', 'manage_options', 'gp-hide-users', array($this,'gp_hide_users_homepage') );
        }



        function gp_hide_users_homepage(){

            global $obj;
            $user_query = $obj->get_all_featured_users();

            if(isset($_POST['save_users'])){

                $user_selected = (isset($_POST['hide_on_home'])) ? $_POST['hide_on_home'] : "";
//                echo "<pre>".print_r($user_selected, true)."</pre>";
                foreach ($user_query->results as $user) {
                    $u_id = $user->ID;
                    if(is_array($user_selected) && in_array($u_id,$user_selected)){ update_user_meta( $u_id, 'hide_on_home', "yes" );}
                    else{ update_user_meta( $u_id, 'hide_on_home', "no" ); }
                }
            }


            if ( ! empty( $user_query->results ) ) {  ?>

                <form action="" method="post">
                    
                
                <table border="1" cellpadding="3" cellspacing="">
                    <tr>
                        <td>
                            <p>Hide on Home</p>
                            <input type="checkbox" name="select_all_users" id="select_all_users" value="yes">
                        </td>
                        <td>User ID</td>
                        <td>Full Name</td>
                        <td>Email</td>
                        <td>Featured</td>
                        <td>Profile</td>

                    </tr>
                
            <?php
                foreach ($user_query->results as $user) {
                    $user_id = $user->ID;
                    $author_info = get_userdata($user_id);
                    $author_fullname = $author_info->first_name . ' ' . $author_info->last_name;
                    $email_address =  $author_info->user_email;
                    $featured_user = esc_attr( get_the_author_meta( 'featured', $user_id) );
                    $user_url = get_author_posts_url($user_id);
                    $user_show_on_home = esc_attr( get_the_author_meta( 'hide_on_home', $user_id ));
                    ?>
                    <tr>
                        <td>

                            <input type="checkbox" class="hide_on_home_chk" name="hide_on_home[]" <?php if($user_show_on_home == "yes") echo "checked"; ?> value="<?php echo $user_id; ?>">
                        </td>
                        <td><?php echo $user_id; ?></td>
                        <td><?php echo $author_fullname; ?></td>
                        <td><?php echo $email_address; ?></td>
                        <td><?php echo $featured_user; ?></td>
                        <td><a target="_blank" href="<?php echo $user_url; ?>">Profile</a>  </td>
                    </tr>

                <?php }  ?>
                    <tr>
                        <td colspan="5">
                            <input type="submit" value="SAVE" name="save_users">
                        </td>
                    </tr>
                </table>
                </form>


        <?php



            }
        }


    }

new GP_Hide_Users_Home_Page();

endif;
?>