<?php
/*
Plugin Name: Download Contest Images
Description: Download Contest Images
Author: Geekpower
Version: 1.0
*/




if ( ! defined( 'ABSPATH' ) ) {
    die( 'Invalid request.' );
}

if ( ! class_exists( 'GP_Download_Contest_Images' ) ) :
    class GP_Download_Contest_Images {

        function __construct()
        {

            add_action( 'admin_enqueue_scripts', array($this, 'gp_download_images_enqueue_script' ));

            add_action('admin_menu', array($this, 'gp_download_contest_images_menu'));


        }


        function gp_download_images_enqueue_script(){
            wp_enqueue_script(
                'gp-download-contest-images',
                plugins_url( 'js/download-images.js', __FILE__ ),
                array('jquery')
            );
        }


        function gp_download_contest_images_menu(){
            add_menu_page( 'Download Contest Images', 'Download Contest Images', 'manage_options', 'gp-download-contest-images', array($this,'gp_download_contest_images') );
        }



        function gp_download_contest_images(){

            global $obj;
            $terms = get_terms( array(
                'taxonomy' => "contest_category",
                'hide_empty' => false,
            ) );

            $tags = get_terms( array(
                'taxonomy' => "submission_tag",
                'hide_empty' => false,
            ) );
            ?>

            <form action="" method="post" class="contest_images_download_form">
                <select name="contest_id" id="contest_id">
                    <?php foreach ($terms as $term) { ?>
                        <option value="<?php echo $term->term_id; ?>"  <?php if(isset($_POST['contest_id']) && $term->term_id == $_POST['contest_id']) { echo "selected"; } ?>  ><?php echo $term->name;  ?></option>
                    <?php } ?>
                </select>


                <select name="tag_id" id="tag_id">
                    <option value=""> -- Select Tag --</option>
                    <?php foreach ($tags as $tag) { ?>
                        <option value="<?php echo $tag->term_id; ?>" <?php if(isset($_POST['tag_id']) && $tag->term_id == $_POST['tag_id']) { echo "selected"; } ?>><?php echo $tag->name;  ?></option>
                    <?php } ?>
                </select>


                <input type="submit" value="Submit" name="contest_submit">
            </form>

            <?php

            if(isset($_POST['contest_id']) && isset($_POST['contest_submit'])){
                $contest_id = $_POST['contest_id'];

//                echo '<pre>' . print_r( $_POST, true ) . '</pre>';
                $tag_id =  $_POST['tag_id'];
                $tag_tax_args = "";
                if($tag_id != ""){
                    $tag_tax_args = array(
                        'taxonomy' => 'submission_tag',
                        'field' => 'term_id',
                        'terms' => $tag_id,
                    );
                }

                $submissions = get_posts(
                    array(
                        'posts_per_page' => -1,  'post_type' => 'contest_submission',
                        'tax_query' => array( 'relation' => 'AND',
                            array( 'taxonomy' => 'contest_category', 'field' => 'term_id', 'terms' => $contest_id),
                            $tag_tax_args
                        )
                    )
                );

                $total_submissions = count($submissions);

                if($total_submissions > 0) { ?>
                    
                    <div class="download-all">
                        <a href="javascript:void(0)" class="download_all_images">Download All</a>
                    </div>

                    <input type="hidden" id="total-download-items" value="<?php echo $total_submissions; ?>">
                    
                    <div class="admin-contest-img-wrapper">
                <?php
                foreach ($submissions as $submission_id) {
                        $submission_title = get_the_title($submission_id);
                        $thumbnail_img = get_the_post_thumbnail_url($submission_id, "thumbnail");
                        $full_img = get_the_post_thumbnail_url($submission_id, "full");
                        $user_id = get_post_meta($submission_id,"art_user_id",true);
                        $first_name = get_user_meta($user_id, 'first_name', true);
                        $last_name = get_user_meta($user_id, 'last_name', true);
                        $full_name = $first_name." ".$last_name;
                        ?>
                        <div class="single-img">
                            <img src="<?php echo $thumbnail_img; ?>" alt="">
                            <p><?php echo $submission_title; ?></p>
                            <p><a href="<?php  echo $full_img; ?>" class="single-download-link"  download>Download</a></p>
                            <p>User : <?php echo $full_name; ?></p>
                        </div>

                        <?php   }  ?>
                    </div>
                <?php       }
            }


        }









    }

new GP_Download_Contest_Images();

endif;
?>