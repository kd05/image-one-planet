<?php
/**
 * DEFAULT PAGE TEMPLATE
 *
 **/
get_header();
the_post(); ?>

    <section class="page-content">
        <h1><?php the_title(); ?></h1>
        <div>
            <?php the_content(); ?>

        </div>
    </section>

<?php get_footer(); ?>