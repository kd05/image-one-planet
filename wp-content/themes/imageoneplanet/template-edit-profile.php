<?php
/**
 * Template Name: Edit Profile
 * Template Post Type: Page
 *
 */


$page_allowed = ["participant","jury","administrator"];
$loginCheck->page_access_to_roles($page_allowed);

get_header();
the_post();

$user_id = get_current_user_id();
$user_info = get_userdata($user_id);
$email_address =  $user_info->user_email;
$headshot_id = get_user_meta( $user_id, 'headshot', true);
$first_name = get_user_meta($user_id, 'first_name', true);
$last_name = get_user_meta($user_id, 'last_name', true);
$usertype = get_user_meta($user_id, 'usertype', true);

$description = get_user_meta($user_id, 'description', true);

$country = get_user_meta($user_id, 'country', true);
$gallery_name = get_user_meta($user_id, 'gallery_name', true);

$website_link = get_user_meta($user_id, 'website-link', true);
$facebook_link = get_user_meta($user_id, 'facebook-link', true);
$instagram_link = get_user_meta($user_id, 'instagram-link', true);
$birth_date = get_user_meta($user_id, 'birth-date', true);

?>

    <div class="main-container" id="submit-entry">
        <section class="page-content">
            <?php get_template_part( 'template-parts/user-sidebar-menu/user', 'sidebar' ); ?>
            <h1><?php the_title(); ?></h1>

            <div class="message-instruction"><p><?php echo $obj->get_site_messages("inst_upload_art"); ?></p></div>

            <div class="edit-profile-container">
                <form class="edit-profile-form form" id="edit-profile-form"   enctype="multipart/form-data">
                    <div class="message-alert"><?php  echo $obj->message_display_get_method("edit_profile_success"); ?></div>

                    <input type="hidden" id="security" name="security" value="<?php echo wp_create_nonce('security-edit-profile-nonce'); ?>">
                    <input type="hidden" name="action" id="action" value="edit_profile" />

                    <div class="input-wrapper">
                        <label for="usertype">User Type*</label>
                        <div class="radio-wrap">
                            <div class="radio-field">
                                <input type="radio" name="usertype" value="artist" <?php  if($usertype == "artist") { echo "checked"; } ?> class="required">
                            </div>
                            <span class="user-type label">Artist</span>
                        </div>
                        <div class="radio-wrap">
                            <div class="radio-field">
                                <input type="radio" name="usertype" value="photographer" <?php  if($usertype == "photographer") { echo "checked"; } ?> >
                            </div>
                            <span class="user-type label">Photographer</span>
                        </div>
                    </div> <!-- /input-wrapper -->

                    <?php  if($headshot_id) {  $headshot_image = current(wp_get_attachment_image_src( $headshot_id, 'thumbnail'));  ?>

                        <div class="input-wrapper">
                            <div class="headshot-img">
                                <input type="hidden" name="headshot_id" id="headshot_id" value="<?php echo $headshot_id; ?>">
                                <img src="<?php echo $headshot_image; ?>" alt="">
                            </div>
                        </div><!-- /input-wrapper -->

                    <?php } ?>


                    <!-- images -->
                    <div class="input-wrapper">
                        <label for="headshot-image">Headshot Image*</label>
                        <input type="file" name="headshot-image" id="headshot-image" >
                    </div><!-- /input-wrapper -->


                    <div class="input-wrapper">
                        <label for="first-name">First Name*</label>
                        <input type="text" name="first-name" id="first-name" class="required" value="<?php echo $first_name; ?>" placeholder="First Name">
                    </div> <!-- /input-wrapper -->


                    <div class="input-wrapper">
                        <label for="last-name">Last Name*</label>
                        <input type="text" name="last-name" id="last-name" class="required" value="<?php echo $last_name; ?>" placeholder="Last Name">
                    </div> <!-- /input-wrapper -->


                    <div class="input-wrapper">
                        <label for="email-address">Email Address*</label>
                        <input type="text" name="email-address" id="email-address" disabled class="" value="<?php echo $email_address; ?>" placeholder="Email Address">
                    </div> <!-- /input-wrapper -->





                    <div class="input-wrapper select-user-cat artist-cat <?php  if($usertype != "artist") { echo "hidden"; } ?>">
                        <label for="email-address">Select Category*</label>
                        <?php
                        $get_artist_cats = get_user_meta( $user_id,'artist-category',true );
                        $explode_artist_cats = array_filter(explode(",",$get_artist_cats));
                        $artist_categories = $terms = get_terms( 'artist_category', array('hide_empty' => false,) );
                        foreach ($artist_categories as $artist_cat) {
                            $a_cat_id = $artist_cat->term_id;
                            $a_cat_name = $artist_cat->name;
                            ?>

                            <div class="radio-wrap">
                                <div class="radio-field">
                                    <input type="checkbox" name="artist-category[]" value="<?php echo $a_cat_id; ?>" <?php if(in_array($a_cat_id, $explode_artist_cats)){ echo "checked"; } ?> >
                                </div>
                                <span class="user-type label"><?php echo $a_cat_name; ?></span>
                            </div>

                        <?php } ?>
                    </div> <!-- /input-wrapper -->


                    <div class="input-wrapper select-user-cat photographer-cat <?php  if($usertype != "photographer") { echo "hidden"; } ?>">
                        <label for="email-address">Select Category*</label>
                        <?php
                        $get_photographer_cats = get_user_meta( $user_id,'photographer-category',true);
                        $explode_photographer_cats = array_filter(explode(",",$get_photographer_cats));
                        $photographer_categories = $terms = get_terms( 'photographer_category', array('hide_empty' => false,) );
                        foreach ($photographer_categories as $photographer_cat) {
                            $p_cat_id = $photographer_cat->term_id;
                            $p_cat_name = $photographer_cat->name;
                            ?>
                            <div class="radio-wrap">
                                <div class="radio-field">
                                    <input type="checkbox" name="photographer-category[]" value="<?php echo $p_cat_id; ?>" <?php if(in_array($p_cat_id, $explode_photographer_cats)){ echo "checked"; } ?> >
                                </div>
                                <span class="user-type label"><?php echo $p_cat_name; ?></span>
                            </div>
                        <?php } ?>
                    </div> <!-- /input-wrapper -->


                    <div class="input-wrapper ">
                        <label for="image-description">Description*</label>
                        <textarea  name="image-description" id="image-description"  class="required" placeholder="Enter a description"><?php echo $description; ?></textarea>
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper">
                        <label for="birth-date">Year of Birth*</label>
                        <input type="text" name="birth-date" id="birth-date" class="required" value="<?php echo $birth_date; ?>" placeholder="Birth Date">
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper">
                        <label for="country">Country*</label>
                        <input type="text" name="country" id="country" class="required" value="<?php echo $country; ?>" placeholder="Country">
                    </div> <!-- /input-wrapper -->


                    <div class="input-wrapper">
                        <label for="website-link">Website Link</label>
                        <input type="text" name="website-link" id="website-link" class="valid-url" value="<?php echo $website_link; ?>" placeholder="Website Link">
                    </div> <!-- /input-wrapper -->


                    <div class="input-wrapper">
                        <label for="facebook-link">Facebook Link</label>
                        <div class="input-wrapper-flex">
                            <label for="">https://www.facebook.com/ </label>
                            <input type="text" name="facebook-link" id="facebook-link" class="" value="<?php echo $facebook_link; ?>" placeholder="Your Profile Name/ID">
                        </div>
                    </div> <!-- /input-wrapper -->


                    <div class="input-wrapper">
                        <label for="instagram-link">Instagram Link</label>
                        <div class="input-wrapper-flex">
                            <label for="">https://www.instagram.com/ </label>
                            <input type="text" name="instagram-link" id="instagram-link" class="" value="<?php echo $instagram_link; ?>" placeholder="Your Profile Name/ID">
                        </div>
                    </div> <!-- /input-wrapper -->



                    <div class="input-wrapper">
                        <label for="country">Gallery Name if under Gallery Representation</label>
                        <input type="text" name="gallery_name" id="gallery_name" class="" value="<?php echo $gallery_name; ?>" placeholder="Gallery Name">
                    </div> <!-- /input-wrapper -->


                    <div class="button-wrapper">
                        <input type="submit" value="Save Profile" tabindex="5" id="edit_profile_submit" name="edit_profile_submit" class="thread-button button blue" />
                    </div> <!-- /button-wrapper -->
                </form>
            </div>


        </section>
    </div>  <!-- /main-container -->

<?php get_footer(); ?>