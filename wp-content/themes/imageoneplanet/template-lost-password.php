<?php
/**
 * Template Name: Lost Password
 * Template Post Type: Page
 *
 */


get_header();
the_post();
global $obj;

?>

    <div class="main-container" id="submit-entry">

        <section class="page-content">
            <h1><?php the_title(); ?></h1>

            <?php

                if(is_user_logged_in()) {
                    echo "<p>You are already Logged In.</p>";
                }
                else {   the_content(); }

           ?>


        </section>
    </div>  <!-- /main-container -->

<?php get_footer(); ?>