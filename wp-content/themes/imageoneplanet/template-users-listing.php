<?php
/**
 * Template Name: Users Listing
 * Template Post Type: Page
 */

get_header();
the_post();
global $wp;

$page_slug = $post->post_name;
if($page_slug == "artists"){
    $title = "Artist";
    $cat_tax = "artist_category";
    $user_category_name = "artist-category";
    $user_type = "artist";
}
else{
    $title = "Photographer";
    $user_category_name = "photographer-category";
    $cat_tax = "photographer_category";
    $user_type = "photographer";
}

if(!isset($_GET['order-by']) || (isset($_GET['order-by']) && $_GET['order-by'] == "asc")){
//    $order_by = array('meta_value' => 'DESC','registered' => 'ASC');
    $order_by = " ORDER BY um.meta_value DESC, user_registered ASC ";
    $order_by_hidden = "asc";
    $order_oldest_class = " blue ";
    $order_newest_class = " ";
} else{
//    $order_by = array('meta_value' => 'DESC','registered' => 'DESC');
    $order_by = " ORDER BY um.meta_value DESC, user_registered DESC ";
    $order_by_hidden = "desc";
    $order_oldest_class = " ";
    $order_newest_class = " blue ";
}

$selected_cat = (isset($_GET['user-category']) && $_GET['user-category'] != "") ? $_GET['user-category'] : "";
$selected_cat_inner_query = ($selected_cat != "") ? " INNER JOIN $wpdb->usermeta AS mt3 ON ( $wpdb->users.ID = mt3.user_id ) " :  "";
$selected_cat_query = ($selected_cat != "") ? " AND (mt3.meta_key = 'artist-category' AND mt3.meta_value LIKE '%,".$_GET['user-category'].",%' )  "  : "";


$query  = "  SELECT $wpdb->users.*  FROM $wpdb->users 
                INNER JOIN $wpdb->usermeta as um ON ( $wpdb->users.ID = um.user_id ) 
                INNER JOIN $wpdb->usermeta AS mt1 ON ( $wpdb->users.ID = mt1.user_id ) 
                INNER JOIN $wpdb->usermeta AS mt2 ON ( $wpdb->users.ID = mt2.user_id ) 
                $selected_cat_inner_query
                INNER JOIN $wpdb->postmeta AS pm ON ( $wpdb->users.ID = pm.meta_value AND pm.meta_key='participant_user_id' ) 
                RIGHT JOIN $wpdb->postmeta AS pm2 ON ( pm.post_id = pm2.post_id AND pm2.meta_key='listing-image-type' AND pm2.meta_value = 'listing_image' ) 
                WHERE 1=1 
                AND (um.meta_key = 'featured') 
                AND (mt1.meta_key = 'usertype' AND mt1.meta_value = '$user_type' ) 
                AND (mt2.meta_key = 'wp_capabilities' AND (mt2.meta_value  LIKE  '%participant%' OR mt2.meta_value  LIKE  '%jury%' ))
                $selected_cat_query

";

//ORDER BY um.meta_value DESC, user_registered ASC LIMIT $offset , ".USER_PER_PAGE;

$total_query = "SELECT COUNT(1) FROM (${query}) AS combined_table";
$total = $wpdb->get_var( $total_query );

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$offset = ($paged ==1 ) ? 0 : ($paged - 1)* USER_PER_PAGE;

$results = $wpdb->get_results( $query . " $order_by LIMIT $offset , ".USER_PER_PAGE );

?>


    <div class="main-container dark-background" id="user-listings-page">
        <section class="page-content">

            <h1><?php the_title(); ?></h1>

            <div class="card-wrapper">
                <form class="search-user-form form" id="search-user-form" action="<?php  echo get_the_permalink(get_the_ID()); ?>" >
                    <div class="flex-wrapper">
                        <div class="input-wrapper select-wrapper">
                            <label for="image-year-taken"><?php echo $title; ?> Category</label>
                            <input type="hidden" name="order-by" value="<?php echo $order_by_hidden; ?>">

                            <?php $category_names = get_terms( $cat_tax, array('hide_empty' => false,) );  ?>
                            <select name="user-category" id="user-category">
                                <option selected value="">- Show All -</option>
                                <?php
                                foreach ($category_names as $user_cat) {
                                    $cat_id = $user_cat->term_id;
                                    $cat_name = $user_cat->name;
                                    ?>
                                    <option value="<?php echo $cat_id; ?>" <?php if($selected_cat == $cat_id) echo "selected"; ?>><?php echo $cat_name; ?></option>
                                <?php } ?>
                                ?>
                            </select>
                        </div> <!-- /input-wrapper -->

                        <div class="button-wrapper">
                            <input type="submit" value="Search" tabindex="5" id="search_user" name="search_user" class="thread-button button blue" />
                        </div> <!-- /button-wrapper -->
                    </div>
                </form>
            </div> <!-- /card-wrapper -->


            <div class="card-wrapper orderby-btns-wrapper">
                <div class="orderby-btns">
                    <div class="btn"><a href="<?php echo site_url()."/".$page_slug; ?>?order-by=asc<?php if($selected_cat != ""){ echo "&user-category=".$selected_cat; } ?>" class="button <?php echo $order_oldest_class; ?>">Oldest</a></div>
                    <div class="btn"><a href="<?php echo site_url()."/".$page_slug; ?>?order-by=desc<?php if($selected_cat != ""){ echo "&user-category=".$selected_cat; } ?>" class="button <?php echo $order_newest_class; ?>">Newest</a></div>
                </div>
            </div>

            <div class="card-wrapper user-listing-container">
                <div class="wookmark-container">
                    <?php

                    if ( count($results) > 0) {
                        foreach ( $results as $user )
                        {
                            $user_id = $user->ID;
                            $author_info = get_userdata($user_id);
                            $author_fullname = $author_info->first_name . ' ' . $author_info->last_name;
                            $headshot_id = esc_attr( get_the_author_meta( 'headshot', $user_id) );
                            if($headshot_id != ""){  $headshot_image = current(wp_get_attachment_image_src( $headshot_id, 'thumbnail')); } else { $headshot_image = ""; };
                            if($headshot_image == ""){ $headshot_image = get_image_not_found(); }
                            $featured_user = esc_attr( get_the_author_meta( 'featured', $user_id) );
                            $user_url = get_author_posts_url($user_id);

                            // TODO: get $prof_gallery (first gallery image for user), then uncomment below

                            $meta_array = array("participant_user_id" => $user_id, "listing-image-type" => "listing_image");
                            $profile_galleries = $obj->get_post_by_multiple_meta("user_profile_gallery", $meta_array);
                            if(is_array($profile_galleries) && count($profile_galleries) > 0){
                                $prof_gallery = current($profile_galleries);
                                $gallery_id = $prof_gallery->ID;
                                $gallery_img_url  = get_the_post_thumbnail_url($gallery_id, "imageone-gallery");
                                // get image height to determine whether an <img> or background-image should be used
                                // this is so the image-overlay does not get cut off on shorter images
                                $gallery_img_meta = wp_get_attachment_metadata(get_post_thumbnail_id($gallery_id));
//                                $gallery_img_size = $gallery_img_meta['sizes']['imageone-gallery']['height'];
                                $gallery_img_size = isset($gallery_img_meta['sizes']['imageone-gallery']['height']) ? $gallery_img_meta['sizes']['imageone-gallery']['height'] : 250;

                                $gallery_title    = get_the_title($gallery_id);
                                $gallery_price    = get_post_meta($gallery_id,"image-price",true);
                                $gallery_year     = get_post_meta($gallery_id,"image-year",true);
                                $gallery_content  = substr($prof_gallery->post_content,0,50)."...";

                            }else{
                                $gallery_img_size = 0;
                                $gallery_img_url = '#';
                                $gallery_title = "";
                                $gallery_price= "";
                                $gallery_year = "";
                                $gallery_content = "";
                            }

                            ?>
                            <div class="single-user-wrapper" >
                                <div class="user-header flex-wrapper">
                                    <a href="<?php echo $user_url; ?>" class="user-link flex-wrapper">
                                        <div class="user-headshot" style="background-image: url('<?php echo $headshot_image; ?>')"></div>
                                        <div class="user-name"><?php echo $author_fullname; ?></div>
                                    </a>
                                    <?php if($featured_user == "yes") { ?> <div class="user-featured"><i class="fas fa-star"></i></div> <?php } ?>
                                </div>
                                <a href="<?php echo $user_url; ?>" class="user-link">
                                    <?php if ($gallery_img_size > 250) { ?>
                                        <img src="<?php echo $gallery_img_url; ?>" alt="">
                                    <?php } else { ?>
                                        <div class="first-gallery-image" style="background-image: url('<?php echo $gallery_img_url; ?>')"></div>
                                    <?php } ?>
                                </a>

                                <div class="image-overlay">
                                    <?php if (!empty($gallery_price)) { ?> <p class="gal-price">$<?php echo $gallery_price; ?></p> <?php } ?>
                                    <?php if (!empty($gallery_title)) { ?> <p class="gal-title"><?php echo $gallery_title; ?></p> <?php } ?>
                                    <?php if (!empty($gallery_year)) { ?> <p class="gal-year"><?php echo $gallery_year; ?></p> <?php } ?>
                                    <?php if (!empty($gallery_content)) { ?> <p class="gal-description"><?php echo nl2br($gallery_content); ?></p> <?php } ?>
                                    <!-- TODO: only show icons if own profile -->
                                    <div class="gal-icons">
                                        <a href="<?php echo $user_url; ?>" title="View <?php echo $author_fullname ?> Profile"><i class="fas fa-eye"></i></a>
                                    </div>
                                </div> <!-- /image-overlay -->
                            </div>

                            <?php
                        }
                    } else {
                        echo "<p>".$obj->get_site_messages("no_user_profile")."</p>";
                    }
                    ?>
                </div><!-- /wookmark-container -->

                <?php


                    echo paginate_links( array(
                        'base' => add_query_arg( 'paged', '%#%' ),
                        'format' => '',
                        'type' => 'list',
                        'prev_text' => __( ' <i class="fas fa-caret-left"></i> ', 'imageoneplanet' ),
                        'next_text' => __( ' <i class="fas fa-caret-right"></i> ', 'imageoneplanet' ),
                        'total' => ceil($total / USER_PER_PAGE),
                        'current' => $paged
                    ));

                ?>

            </div> <!-- /card-wrapper -->
        </section>
    </div>
    <!-- /main-container -->

<?php get_footer(); ?>