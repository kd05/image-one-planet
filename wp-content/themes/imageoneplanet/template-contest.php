<?php
/**
 * Template Name: Contest Page
 * Page template for the contest page
 */

//$page_allowed = ["participant","administrator"];
//$loginCheck->page_access_to_roles($page_allowed);

get_header();
the_post();

$args = array(
    'taxonomy'   => 'contest_category',
    'hide_empty' => false,
    'meta_query' => array(
        array(
            'key'       => 'contest_status',
            'value'     => 'active',
            'compare'   => '='
        )
    )
);

global $obj;
$current_contest      = get_terms($args);
$contest_id           = $current_contest[0]->term_id;
$upload_main_image_id = get_term_meta ($contest_id, 'upload_main_image_id',true);
$contest_mode         = get_term_meta ($contest_id, 'contest_mode', true);
$contest_name         = $current_contest[0]->name;
$contest_deadline     = get_term_meta ($contest_id, 'contest_deadline',true);
$sponsor_image_id     = get_term_meta ($contest_id, 'sponsor_image_id', true);


// contest mode
$submissionMode = $obj->get_active_contest_mode($contest_id) == "submission_mode" ? true : false;
$publishMode    = $obj->get_active_contest_mode($contest_id) == "publish" ? true : false;
$finalMode      = $obj->get_active_contest_mode($contest_id) == "final" ? true : false;

if ($submissionMode || $publishMode) {
    $hero_bg = $upload_main_image_id ? wp_get_attachment_image_src($upload_main_image_id, 'custom-size-2000')[0] : get_template_directory_uri() . '/assets/images/placeholders/placeholder-contest-hero.jpg';
} else if ($finalMode) {
    $hero_bg = get_template_directory_uri() . '/assets/images/placeholders/placeholder-contest-over.jpg';
}

//echo '<pre>' . print_r(get_term_meta($contest_id), true) . '</pre>';
?>

    <div class="main-container" id="contest-page">

        <!-- FIRST PANEL -->
        <section class="hero-section<?php echo $finalMode ? ' final-mode-hero' : ''; ?>">
            <div class="hero-bg" style="background-image: url('<?php echo $hero_bg; ?>')"></div>
            <div class="text-wrapper">

            <?php if ($submissionMode || $publishMode) { ?>
                <div class="regular-text">
                    <h1>
                        <?php echo $contest_name; ?>
                    </h1>
                    <h2>Submission deadline</h2>
                    <h3><?php echo $contest_deadline; ?></h3>

                    <?php
                    if ($submissionMode) {
                        echo '<a href="' . site_url() . '/upload-art/" class="button blue-text">Submit an Entry</a>';
                    } else if ($publishMode) {
                        echo '<h4 class="green-text">Submission Deadline is Up!</h4>';
                    }
                    ?>
                </div> <!-- /regular-text -->
            <?php } else if ($finalMode) { ?>
                <div class="final-text">
                    <h2>THANK YOU FOR YOUR SUBMISSIONS!</h2>
                    <h1>The <?php echo $contest_name; ?> is Complete!</h1>
                    <h3>The Winners Have Been Chosen</h3>
                    <div class="down-arrow">
                        <i class="fas fa-arrow-down"></i>
                    </div>
                </div> <!-- /final-text -->
            <?php } ?>
            </div> <!-- /text-wrapper -->
            <?php if ($submissionMode || $publishMode) { ?>
                <div class="down-arrow bottom-arrow">
                    <i class="fas fa-arrow-down"></i>
                </div>
            <?php } ?>
        </section> <!-- /hero-section -->

        <!-- SECOND PANEL GROUP -->
    <?php if ($submissionMode) { ?>

        <section class="contest-details-section" id="scroll-target">
            <div class="inner-container flex-wrapper">
                <div class="image-wrapper">
                    <?php
                    $contest_details_image = get_field('contest_details_image')? get_field('contest_details_image') : get_template_directory_uri() . '/assets/images/placeholders/placeholder-contest-details.jpg';
                    ?>
                    <img src="<?php echo $contest_details_image; ?>" alt="Submission thanks image">
                </div>
                <div class="text-wrapper">
                    <?php  echo get_field('contest_detail') ?>
                </div>
            </div>
        </section>

    <?php } else if ($publishMode) { ?>

        <section class="top-100-section" id="scroll-target">
            <div class="inner-container">
                <h1>Top 100 Submitted Works</h1>
                <p>Vote for your top picks by liking them on Instagram to see them featured!</p>
                <div class="thumb-gallery-wrapper">

                   <?php
                   $tax_query = array( array( 'taxonomy' => 'submission_tag', 'field' => 'slug', 'terms' =>  'top-100' ),
                       array( 'taxonomy' => 'contest_category', 'field' => 'id', 'terms' =>  $contest_id ));
                   $top_100_arts = $obj->get_post_by_multiple_meta("contest_submission",array(),-1, $tax_query,"ids");

                   foreach ($top_100_arts as $top_100_single_id) :
                       $winner_art_img = get_the_post_thumbnail_url($top_100_single_id,"medium");
                       $winner_art_full_img = get_the_post_thumbnail_url($top_100_single_id,"custom-size-2000");
                       $winner_art_title = get_the_title($top_100_single_id);
                       $winner_price = get_term_meta($contest_id, 'prize_data_bronze', true);
                       $winner_user_id = get_post_meta($top_100_single_id,"art_user_id",true);
                       $first_name = get_user_meta($winner_user_id, 'first_name', true);
                       $last_name = get_user_meta($winner_user_id, 'last_name', true);
                       $winner_full_name = $first_name." ".$last_name;
                       $submission_social_link = get_post_meta( $top_100_single_id, 'submission_social_link', true );
                       ?>

                       <div class="thumb-wrapper">
                           <div class="thumb-image" style="background-image: url('<?php echo $winner_art_img; ?>')">
                               <div class="open-lightbox voteable"
                                    data-full-img="<?php echo $winner_art_full_img; ?>"
                                    data-img-title="<?php echo $winner_art_title; ?>"
                                    data-instagram="<?php echo $submission_social_link ? $submission_social_link : '#'; ?>"
                                    data-artist-name="<?php echo $winner_full_name; ?>">
                                   <i class="fas fa-search"></i>
                                   <span>View & Vote</span>
                               </div>
                           </div>
                           <div class="thumb-details">
                               <p class="artist"><?php echo $winner_full_name; ?></p>
                               <p class="art-title"><?php echo $winner_art_title; ?></p>
                           </div>
                       </div>

                   <?php endforeach;  ?>
                </div> <!-- /thumb-gallery-wrapper -->
            </div> <!-- /inner-container -->
        </section>

    <?php  } else if ($finalMode) { ?>

        <section class="winners-section" id="scroll-target">
            <div class="inner-container">

                <?php
                $tax_query = array( array( 'taxonomy' => 'submission_tag', 'field' => 'slug', 'terms' =>  'winner-gold' ),
                                    array( 'taxonomy' => 'contest_category', 'field' => 'id', 'terms' =>  $contest_id ));
                $winner_art_id = current($obj->get_post_by_multiple_meta("contest_submission",array(),1, $tax_query,"ids"));
                $winner_art_img = get_the_post_thumbnail_url($winner_art_id,"large");
                $winner_art_full_img = get_the_post_thumbnail_url($winner_art_id,"custom-size-2000");
                $winner_art_title = get_the_title($winner_art_id);
                $winner_price = get_term_meta($contest_id, 'prize_data_gold', true);
                $winner_user_id = get_post_meta($winner_art_id,"art_user_id",true);
                $first_name = get_user_meta($winner_user_id, 'first_name', true);
                $last_name = get_user_meta($winner_user_id, 'last_name', true);
                $winner_full_name = $first_name . " " . $last_name;
                $submission_social_link = get_post_meta( $winner_art_id, 'submission_social_link', true );
                ?>
                <div class="flex-wrapper first-prize">
                    <div class="image-wrapper">
                        <div class="open-lightbox"
                             data-full-img="<?php echo $winner_art_full_img; ?>"
                             data-img-title="<?php echo $winner_art_title; ?>"
                             data-instagram-view="<?php echo $submission_social_link ? $submission_social_link : '#'; ?>"
                             data-artist-name="<?php echo $winner_full_name; ?>">
                            <i class="fas fa-search"></i>
                            <span>View</span>
                        </div>
                        <img src="<?php echo $winner_art_img; ?>" class="has-border" alt="First Place Winner">
                    </div> <!-- /image-wrapper -->
                    <div class="text-wrapper">
                        <div class="prize-unit">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/prize-trophy-first.png" alt="First Trophy">
                            <p class="artist"><?php echo $winner_full_name; ?></p>
                            <p class="art-title"><?php echo $winner_art_title; ?></p>
                            <p class="prize gold-text"><?php echo $winner_price; ?></p>
                        </div> <!-- /prize-unit -->
                    </div> <!-- /text-wrapper -->
                </div> <!-- /flex-wrapper -->


                <?php
                $tax_query = array( array( 'taxonomy' => 'submission_tag', 'field' => 'slug', 'terms' =>  'winner-silver' ),
                                    array( 'taxonomy' => 'contest_category', 'field' => 'id', 'terms' =>  $contest_id ));
                $winner_art_id = current($obj->get_post_by_multiple_meta("contest_submission",array(),1, $tax_query,"ids"));
                $winner_art_img = get_the_post_thumbnail_url($winner_art_id,"medium");
                $winner_art_full_img = get_the_post_thumbnail_url($winner_art_id,"custom-size-2000");
                $winner_art_title = get_the_title($winner_art_id);
                $winner_price = get_term_meta($contest_id, 'prize_data_silver', true);
                $winner_user_id = get_post_meta($winner_art_id,"art_user_id",true);
                $first_name = get_user_meta($winner_user_id, 'first_name', true);
                $last_name = get_user_meta($winner_user_id, 'last_name', true);
                $winner_full_name = $first_name." ".$last_name;
                $submission_social_link = get_post_meta( $winner_art_id, 'submission_social_link', true );
                ?>
                <div class="flex-wrapper second-prize">
                    <div class="image-wrapper">
                        <div class="open-lightbox"
                             data-full-img="<?php echo $winner_art_full_img; ?>"
                             data-img-title="<?php echo $winner_art_title; ?>"
                             data-instagram-view="<?php echo $submission_social_link ? $submission_social_link : '#'; ?>"
                             data-artist-name="<?php echo $winner_full_name; ?>">
                            <i class="fas fa-search"></i>
                            <span>View</span>
                        </div>
                        <img src="<?php echo $winner_art_img; ?>" class="has-border" alt="Second Place Winner">
                    </div> <!-- /image-wrapper -->
                    <div class="text-wrapper">
                        <div class="prize-unit flex-wrapper">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/prize-trophy-second.png" alt="Second Trophy">
                            <div>
                                <p class="artist"><?php echo $winner_full_name; ?></p>
                                <p class="art-title"><?php echo $winner_art_title; ?></p>
                                <p class="prize blue-text"><?php echo $winner_price; ?></p>
                            </div>
                        </div> <!-- /prize-unit -->
                    </div> <!-- /text-wrapper -->
                </div> <!-- /flex-wrapper -->


                <?php
                $tax_query = array( array( 'taxonomy' => 'submission_tag', 'field' => 'slug', 'terms' =>  'winner-bronze' ),
                                    array( 'taxonomy' => 'contest_category', 'field' => 'id', 'terms' =>  $contest_id ));
                $winner_art_id = current($obj->get_post_by_multiple_meta("contest_submission",array(),1, $tax_query,"ids"));
                $winner_art_img = get_the_post_thumbnail_url($winner_art_id,"medium");
                $winner_art_full_img = get_the_post_thumbnail_url($winner_art_id,"custom-size-2000");
                $winner_art_title = get_the_title($winner_art_id);
                $winner_price = get_term_meta($contest_id, 'prize_data_bronze', true);
                $winner_user_id = get_post_meta($winner_art_id,"art_user_id",true);
                $first_name = get_user_meta($winner_user_id, 'first_name', true);
                $last_name = get_user_meta($winner_user_id, 'last_name', true);
                $winner_full_name = $first_name." ".$last_name;
                $submission_social_link = get_post_meta( $winner_art_id, 'submission_social_link', true );
                ?>
                <div class="flex-wrapper third-prize">
                    <div class="text-wrapper">
                        <div class="prize-unit flex-wrapper">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/prize-trophy-third.png" alt="Third Trophy">
                            <div>
                                <p class="artist"><?php echo $winner_full_name; ?></p>
                                <p class="art-title"><?php echo $winner_art_title; ?></p>
                                <p class="prize green-text"><?php echo $winner_price; ?></p>
                            </div>
                        </div> <!-- /prize-unit -->
                    </div> <!-- /text-wrapper -->
                    <div class="image-wrapper">
                        <div class="open-lightbox"
                             data-full-img="<?php echo $winner_art_full_img; ?>"
                             data-img-title="<?php echo $winner_art_title; ?>"
                             data-instagram-view ="<?php echo $submission_social_link ? $submission_social_link : '#'; ?>"
                             data-artist-name="<?php echo $winner_full_name; ?>">
                            <i class="fas fa-search"></i>
                            <span>View</span>
                        </div>
                        <img src="<?php echo $winner_art_img; ?>" class="has-border" alt="Third Place Winner">
                    </div> <!-- /image-wrapper -->
                </div> <!-- /flex-wrapper -->


            </div> <!-- /inner-container -->
        </section>


        <?php get_template_part( 'template-parts/gallery/gallery', 'mostpopular' ); ?>


        <?php get_template_part( 'template-parts/gallery/gallery', 'top20' ); ?>



    <?php } ?>

    <!-- THIRD PANEL -->
    <?php if ($publishMode || $finalMode) { ?>

        <section class="submission-thanks-section">
            <div class="inner-container flex-wrapper">
                <div class="image-wrapper">
                    <?php
                    $submission_thanks_image = get_field('submission_thanks_image') ? get_field('submission_thanks_image') : get_template_directory_uri() . '/assets/images/placeholders/placeholder-submission-thanks.jpg';
                    $submission_thanks_content = get_field('submission_thanks_content');
                    ?>
                    <img src="<?php echo $submission_thanks_image; ?>" alt="Submission thanks image">
                </div> <!-- /image-wrapper -->
                <div class="text-wrapper">
                    <?php echo $submission_thanks_content; ?>
                </div> <!-- /text-wrapper -->
            </div> <!-- /inner-container -->
        </section> <!-- /submission-thanks-section -->

    <?php } ?>

    <!-- FOURTH PANEL -->
    <?php if ($submissionMode || $publishMode) { ?>

        <section class="jury-section">
            <div class="left-bg"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/placeholder-pawprints-left.png" alt=""></div>
            <div class="inner-container flex-wrapper">
                <div class="text-wrapper">
                    <h2><?php  echo get_term_meta ($contest_id, 'jury_content_title',true); ?></h2>
                    <p><?php  echo nl2br(get_term_meta ($contest_id, 'jury_content',true)); ?></p>
                </div>
                <div class="jury-slider-wrapper">
                    <div class="control left hide">
                        <i class="fas fa-arrow-left"></i>
                    </div>
                    <div class="juror-wrapper">
                        <div class="juror-inner-wrapper">

                        <?php
                        if (get_term_meta( $contest_id, 'jury_title_1',true)) {

                            $juror_name1 = get_term_meta($contest_id, 'jury_title_1',true);
                            $juror_site1 = get_term_meta($contest_id, 'jury_website_1',true);
                            $juror_image_id1 = get_term_meta($contest_id, 'jury_image_1_id',true);
                            $juror_image1 = wp_get_attachment_image_url($juror_image_id1, 'large');

                            if (empty($juror_image1) || !isset($juror_image1)) {
                                $juror_image1 = get_no_profile_pic_found();
                            }
                        ?>

                            <div class="juror-unit">
                                <div class="juror-image-wrapper">
                                    <div class="juror-image" style="background-image: url('<?php echo $juror_image1; ?>')"></div>
                                </div> <!-- /juror-image-wrapper -->
                                <div class="juror-details">
                                    <p class="juror-name"><?php echo $juror_name1 ? $juror_name1 : 'Juror'; ?></p>
                                    <?php if ($juror_site1) { ?>
                                        <a href="<?php echo $juror_site1; ?>"><?php echo $juror_site1; ?></a>
                                    <?php } ?>
                                </div> <!-- /juror-details -->
                            </div> <!-- /juror-unit -->

                        <?php
                        }

                        if (get_term_meta( $contest_id, 'jury_title_2',true)) {

                            $juror_name2 = get_term_meta($contest_id, 'jury_title_2',true);
                            $juror_site2 = get_term_meta($contest_id, 'jury_website_2',true);
                            $juror_image_id2 = get_term_meta($contest_id, 'jury_image_2_id',true);
                            $juror_image2 = wp_get_attachment_image_url($juror_image_id2, 'large');

                            if (empty($juror_image2) || !isset($juror_image2)) {
                                $juror_image2 = get_no_profile_pic_found();
                            }
                        ?>

                            <div class="juror-unit">
                                <div class="juror-image-wrapper">
                                    <div class="juror-image" style="background-image: url('<?php echo $juror_image2; ?>')"></div>
                                </div> <!-- /juror-image-wrapper -->
                                <div class="juror-details">
                                    <p class="juror-name"><?php echo $juror_name2 ? $juror_name2 : 'Juror'; ?></p>
                                    <?php if ($juror_site2) { ?>
                                        <a href="<?php echo $juror_site2; ?>"><?php echo $juror_site2; ?></a>
                                    <?php } ?>
                                </div> <!-- /juror-details -->
                            </div> <!-- /juror-unit -->

                        <?php
                        }

                        if (get_term_meta( $contest_id, 'jury_title_3',true)) {

                            $juror_name3 = get_term_meta($contest_id, 'jury_title_3',true);
                            $juror_site3 = get_term_meta($contest_id, 'jury_website_3',true);
                            $juror_image_id3 = get_term_meta($contest_id, 'jury_image_3_id',true);
                            $juror_image3 = wp_get_attachment_image_url($juror_image_id3, 'large');

                            if (empty($juror_image3) || !isset($juror_image3)) {
                                $juror_image3 = get_no_profile_pic_found();
                            }
                        ?>

                            <div class="juror-unit">
                                <div class="juror-image-wrapper">
                                    <div class="juror-image" style="background-image: url('<?php echo $juror_image3; ?>')"></div>
                                </div> <!-- /juror-image-wrapper -->
                                <div class="juror-details">
                                    <p class="juror-name"><?php echo $juror_name3 ? $juror_name3 : 'Juror'; ?></p>
                                    <?php if ($juror_site3) { ?>
                                        <a href="<?php echo $juror_site3; ?>"><?php echo $juror_site3; ?></a>
                                    <?php } ?>
                                </div> <!-- /juror-details -->
                            </div> <!-- /juror-unit -->

                        <?php
                        }

                        if (get_term_meta( $contest_id, 'jury_title_4',true)) {

                            $juror_name4 = get_term_meta($contest_id, 'jury_title_4',true);
                            $juror_site4 = get_term_meta($contest_id, 'jury_website_4',true);
                            $juror_image_id4 = get_term_meta($contest_id, 'jury_image_4_id',true);
                            $juror_image4 = wp_get_attachment_image_url($juror_image_id4, 'large');

                            if (empty($juror_image4) || !isset($juror_image4)) {
                                $juror_image4 = get_no_profile_pic_found();
                            }
                        ?>

                            <div class="juror-unit">
                                <div class="juror-image-wrapper">
                                    <div class="juror-image" style="background-image: url('<?php echo $juror_image4; ?>')"></div>
                                </div> <!-- /juror-image-wrapper -->
                                <div class="juror-details">
                                    <p class="juror-name"><?php echo $juror_name4 ? $juror_name4 : 'Juror'; ?></p>
                                    <?php if ($juror_site4) { ?>
                                        <a href="<?php echo $juror_site4; ?>"><?php echo $juror_site4; ?></a>
                                    <?php } ?>
                                </div> <!-- /juror-details -->
                            </div> <!-- /juror-unit -->

                        <?php
                        }

                        if (get_term_meta( $contest_id, 'jury_title_5',true)) {

                            $juror_name5 = get_term_meta($contest_id, 'jury_title_5',true);
                            $juror_site5 = get_term_meta($contest_id, 'jury_website_5',true);
                            $juror_image_id5 = get_term_meta($contest_id, 'jury_image_5_id',true);
                            $juror_image5 = wp_get_attachment_image_url($juror_image_id5, 'large');

                            if (empty($juror_image5) || !isset($juror_image5)) {
                                $juror_image5 = get_no_profile_pic_found();
                            }
                        ?>

                            <div class="juror-unit">
                                <div class="juror-image-wrapper">
                                    <div class="juror-image" style="background-image: url('<?php echo $juror_image5; ?>')"></div>
                                </div> <!-- /juror-image-wrapper -->
                                <div class="juror-details">
                                    <p class="juror-name"><?php echo $juror_name5 ? $juror_name5 : 'Juror'; ?></p>
                                    <?php if ($juror_site5) { ?>
                                        <a href="<?php echo $juror_site5; ?>"><?php echo $juror_site5; ?></a>
                                    <?php } ?>
                                </div> <!-- /juror-details -->
                            </div> <!-- /juror-unit -->

                        <?php
                        }
                        ?>

                        </div>
                    </div> <!-- /juror-wrapper -->
                    <div class="control right">
                        <i class="fas fa-arrow-right"></i>
                    </div>
                </div>
            </div>
            <div class="right-bg"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/placeholder-branches.png" alt=""></div>
        </section> <!-- /jury-section -->

    <?php } ?>

    <!-- FIFTH PANEL -->
    <?php if ($submissionMode || $publishMode) { ?>

        <section class="prize-section">
           <div class="inner-container">
               <div class="prize-header">
                   <h1>Our Prizes</h1>
                   <p>Nulla pellentesque dolor in neque tincidunt aliquet. Nulla tincidunt interdum dolor,
                       vel imperdiet leo vestibulum sed.</p>
               </div>
               <div class="prize-wrapper flex-wrapper">
                   <div class="prize-unit">
                       <img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/prize-trophy-first.png" alt="First Trophy">
                       <p>Grand Prize Winner will receive <span class="gold-text"><?php echo get_term_meta($contest_id, 'prize_data_gold', true); ?></span></p>
                   </div> <!-- /prize-unit -->
                   <div class="prize-unit">
                       <img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/prize-trophy-second.png" alt="Second Trophy">
                       <p>Runner Up Winner will receive <span class="blue-text"><?php echo get_term_meta($contest_id, 'prize_data_silver', true); ?></span></p>
                   </div> <!-- /prize-unit -->
                   <div class="prize-unit">
                       <img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/prize-trophy-third.png" alt="Third Trophy">
                       <p>Third Place Runner Up Winner will receive <span class="green-text"><?php echo get_term_meta($contest_id, 'prize_data_bronze', true); ?></span></p>
                   </div> <!-- /prize-unit -->
               </div> <!-- /prize-wrapper -->
               <div class="prize-detail flex-wrapper">
                   <img src="<?php echo get_template_directory_uri(); ?>/assets/images/instagram-colour-logo.png" alt="Instagram Logo">
                   <p>The top ten entries with most social media likes will be featured as "Most Popular" along with the winners.</p>
               </div>
           </div> <!-- /inner-container -->
        </section> <!-- /prize-section -->

    <?php } ?>

    <!-- SIXTH PANEL -->

    <?php if (isset($sponsor_image_id) && !empty($sponsor_image_id)) { ?>

        <section class="sponsor-section">
            <div class="left-bg"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/placeholder-leaves.png" alt=""></div>
            <div class="inner-container flex-wrapper">
                <div class="image-wrapper">
                    <img src="<?php echo wp_get_attachment_image_src($sponsor_image_id, 'large')[0]; ?>" alt="Sponsor Image">
                </div>
                <?php
                $sponsor_title = get_term_meta($contest_id, 'sponsor_title',true);
                $sponsor_description = nl2br(get_term_meta($contest_id, 'sponsor_description',true));
                ?>
                <div class="text-wrapper">
                    <h1><?php echo $sponsor_title; ?></h1>
                    <p><?php echo $sponsor_description; ?></p>
                </div>
            </div> <!-- /inner-container -->
        </section> <!-- /sponsor-section -->

    <?php } ?>

    <!-- SEVENTH PANEL -->
    <?php if ($submissionMode) {
        $left_img = get_field('win_left_img') ? get_field('win_left_img') : get_template_directory_uri()."/assets/images/placeholders/placeholder-enter-left.jpg";
        $right_img = get_field('win_right_img')? get_field('win_right_img') : get_template_directory_uri()."/assets/images/placeholders/placeholder-enter-right.jpg";
        $entertowin_content = get_field('enter_to_win_description');
        ?>

        <section class="enter-now-section">
            <div class="flex-wrapper">
                <div class="image-third image-left">
                    <div class="image-wrapper has-border">
                        <div class="inner-image" style="background-image: url('<?php echo $left_img; ?>')"></div>
                    </div>
                </div> <!-- /image-left -->
                <div class="mid-text-wrapper">
                    <?php  echo $entertowin_content; ?>
                </div>
                <div class="image-third image-right">
                    <div class="image-wrapper has-border">
                        <div class="inner-image" style="background-image: url('<?php echo $right_img; ?>')"></div>
                    </div>
                </div> <!-- /image-right -->
            </div> <!-- /flex-wrapper -->
        </section> <!-- /enter-now-section -->

    <?php }

    if ($publishMode || $finalMode) {
        get_template_part('template-parts/gallery/gallery', 'lightbox');
    }
    ?>

    </div>  <!-- /main-container -->

<?php get_footer(); ?>