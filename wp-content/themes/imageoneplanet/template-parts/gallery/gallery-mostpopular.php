<section class="runner-ups-section">
    <div class="inner-container">
        <h1>Your Runner Up Winners</h1>
        <p>Most Popular dolor sit amet. Praesent sed rhoncus diam.</p>
        <div class="thumb-gallery-wrapper">
        <?php
        global $obj;
        //If This Template is called from Archive Taxonomy -> Single Taxonomy or Current Contest Page
        $contest_id  = is_tax() ? get_queried_object()->term_id : $obj->get_active_contest_id();

        $tax_query = array( array( 'taxonomy' => 'submission_tag', 'field' => 'slug', 'terms' =>  'most-popular' ),
                            array( 'taxonomy' => 'contest_category', 'field' => 'id', 'terms' =>  $contest_id ));
        $top_mp_arts = $obj->get_post_by_multiple_meta("contest_submission",array(),-1, $tax_query,"ids");

        if(count($top_mp_arts) > 0) {
            foreach ($top_mp_arts as $top_mp_single_id) :
                $winner_art_img = get_the_post_thumbnail_url($top_mp_single_id, "medium");
                $winner_art_full_img = get_the_post_thumbnail_url($top_mp_single_id, "custom-size-2000");
                $winner_art_title = get_the_title($top_mp_single_id);
                $winner_price = get_term_meta($contest_id, 'prize_data_bronze', true);
                $winner_user_id = get_post_meta($top_mp_single_id, "art_user_id", true);
                $first_name = get_user_meta($winner_user_id, 'first_name', true);
                $last_name = get_user_meta($winner_user_id, 'last_name', true);
                $winner_full_name = $first_name . " " . $last_name;
                $submission_social_link = get_post_meta( $top_mp_single_id, 'submission_social_link', true );
                ?>

                <div class="thumb-wrapper" id="thumb-">
                    <div class="thumb-image" style="background-image: url('<?php echo $winner_art_img; ?>')">
                        <div class="open-lightbox voteable"
                             data-full-img="<?php echo $winner_art_full_img; ?>"
                             data-img-title="<?php echo $winner_art_title; ?>"
                             data-instagram-view="<?php echo $submission_social_link ? $submission_social_link : '#'; ?>"
                             data-artist-name="<?php echo $winner_full_name; ?>">
                            <i class="fas fa-search"></i>
                            <span>View</span>
                        </div>
                    </div>
                    <div class="thumb-details">
                        <p class="artist"><?php echo $winner_full_name; ?></p>
                        <p class="art-title"><?php echo $winner_art_title; ?></p>
                    </div>
                </div>

            <?php endforeach;
        }  else { ?>
            <div class="message-alert"> <?php echo $obj->get_site_messages("no_record"); ?></div>
        <?php } ?>

        </div> <!-- /thumb-gallery-wrapper -->
    </div> <!-- /inner-container -->
</section>


