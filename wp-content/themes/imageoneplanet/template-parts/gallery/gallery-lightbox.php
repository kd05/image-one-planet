<div class="gallery-lightbox">
    <div class="lightbox-inner">
        <div class="close-lightbox"><i class="fas fa-times"></i></div>
        <div class="image-wrapper">
            <!-- populated with JS -->
            <img src="" class="full-image" alt="">
        </div>
        <div class="lightbox-details">
            <div class="image-details">
                <h4 class="artist"><!-- populated with JS --></h4>
                <h5 class="art-title"><!-- populated with JS --></h5>
            </div>
            <!-- vote-wrapper: populated with JS -->
        </div>
    </div>
    <div class="gallery-overlay"></div>
</div>