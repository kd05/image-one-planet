<?php
/**
 * Template part for displaying posts with excerpts
 *
 * Used in Search Results and for Recent Posts in Front Page panels.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>




    <article class="single-post" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>



        <div class="entry-summary">
            <?php $content = get_the_content();
            $content = gp_excerptize($content, 90);
            $content .= '<a class="read-more" href="'.get_the_permalink().'">Read More</a>';
            $feat_image = get_the_post_thumbnail_url();
            if($feat_image){
                ?>
                <div class="gp-blog-two-col gp-blog-common-col">
                    <div class="left"  data-aos="fade-right" data-aos-duration="1000" >
                        <div class="bg-image" style="background-image: url('<?php echo $feat_image;?>')"></div>
                    </div>
                    <div class="right"  data-aos="fade-left" data-aos-duration="1000">
                        <a href="<?php echo get_the_permalink(); ?>"><h4 class="title"><?php echo the_title(); ?></h4></a>
                        <div class="date-author">
                            <span class="date"><?php echo get_the_date();?></span>
                        </div>
                        <p><?php echo $content; ?></p>

                    </div>
                </div>
                <?php
            } else {
                ?>
                <div class="gp-blog-one-col gp-blog-common-col"  data-aos="fade-up" data-aos-duration="1000">
                    <div class="content" >
                        <a href="<?php echo get_the_permalink(); ?>"><h4 class="title"><?php echo the_title(); ?></h4></a>
                        <div class="date-author">
                            <span class="date"><?php echo get_the_date();?></span>
                        </div>
                        <p><?php echo $content; ?></p>
                    </div>
                </div>
                <?php
            }
            ?>

        </div><!-- .entry-summary -->

    </article><!-- #post-## -->


