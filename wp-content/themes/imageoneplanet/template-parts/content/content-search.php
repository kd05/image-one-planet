

<?php

$gallery_id       = get_the_ID();
$gallery_img_url  = get_the_post_thumbnail_url($gallery_id, "imageone-gallery");
$gallery_title    = get_the_title($gallery_id);
$gallery_content  = substr(get_the_content(),0,50)."...";
$gallery_url      = get_the_permalink($gallery_id);
$gallery_price    = get_post_meta($gallery_id,"image-price",true);
$gallery_year     = get_post_meta($gallery_id,"image-year",true);
$is_featured      = get_post_meta($gallery_id,"image-type",true);
$is_listing_image = get_post_meta($gallery_id,"listing-image-type",true);

// get image height to determine whether an <img> or background-image should be used
// this is so the image-overlay does not get cut off on shorter images
$gallery_img_meta = wp_get_attachment_metadata(get_post_thumbnail_id($gallery_id));
$gallery_img_size = $gallery_img_meta['sizes']['imageone-gallery']['height'];

?>

<div class="single-user-wrapper" >

    <a href="<?php echo $gallery_url; ?>" class="user-link">
        <?php if ($gallery_img_size > 250) { ?>
            <img src="<?php echo $gallery_img_url; ?>" alt="">
        <?php } else { ?>
            <div class="first-gallery-image" style="background-image: url('<?php echo $gallery_img_url; ?>')"></div>
        <?php } ?>
    </a>

    <div class="image-overlay">
        <?php if (!empty($gallery_price)) { ?> <p class="gal-price">$<?php echo $gallery_price; ?></p> <?php } ?>
        <?php if (!empty($gallery_title)) { ?> <p class="gal-title"><?php echo $gallery_title; ?></p> <?php } ?>
        <?php if (!empty($gallery_year)) { ?> <p class="gal-year"><?php echo $gallery_year; ?></p> <?php } ?>
        <?php if (!empty($gallery_content)) { ?> <p class="gal-description"><?php echo nl2br($gallery_content); ?></p> <?php } ?>
        <!-- TODO: only show icons if own profile -->
        <div class="gal-icons">
            <a href="<?php echo $gallery_url; ?>" title="View <?php echo $author_fullname ?> Profile"><i class="fas fa-eye"></i></a>
        </div>
    </div> <!-- /image-overlay -->
</div>