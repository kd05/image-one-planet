<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="search-pop-up-overlay"></div>
<div class="search-pop-up-container">
    <div class="search-wrap">
        <?php // echo get_search_form(); ?>

        <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ) ?>">
            <label>
                <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
                <input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" />
            </label>

            <input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" />
        </form>

<!--        <div class="search-btn button">-->
<!--            <a class="search-submit" href="javascript:void(0);"><i class="fa fa-search"></i></a>-->
<!--        </div>-->
    </div>
    <div class="close-btn">
        <i class="fa fa-close"></i>
    </div>
</div>