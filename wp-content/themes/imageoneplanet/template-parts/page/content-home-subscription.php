

<div class="sign-up-container" id="sign-up-container" style="background-image: url('<?php echo get_template_directory_uri()."/assets/images/sign-up-bg-img.png" ?>');">

    <div class="signup-overlap-gradient"></div>

    <div class="signup-title">
        <h3>W. Gifford-Jones, MD: 90+ and going strong.</h3>
        <p>Stay informed by joining our mailing list.</p>
    </div>



    <!--        <div class="sign-up-form" id="contactForm">-->
    <!--            <form id="form-contact-page">-->
    <!--                <div class="form-container checkout-step1 checkout-step">-->
    <!--                    <div class="message-step"></div>-->
    <!---->
    <!--                    <input type="hidden" id="security-contact" value="--><?php //echo wp_create_nonce('security-contact-nonce'); ?><!--">-->
    <!---->
    <!--                    <div class="field-row">-->
    <!--                        <div class="input-container">-->
    <!--                            <input type="text"  class="required" placeholder="Full Name*"  name="full-name" id="full-name" value="">-->
    <!--                            <i class="fa fa-edit"></i>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!---->
    <!--                    <div class="field-row">-->
    <!--                        <div class="input-container">-->
    <!--                            <input type="email"  class="required chk-email" placeholder="Email*" name="email-address" id="email-address" value="">-->
    <!--                            <i class="fa fa-envelope"></i>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!---->
    <!--                    <div class="field-row">-->
    <!--                        <div class="common-btn-container">-->
    <!--                            <a href="javascript:void(0)"  class="contact-submit">Submit &rarr;</a>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!---->
    <!--                </div>-->
    <!--            </form>-->
    <!--        </div>-->


    <!-- Begin Mailchimp Signup Form -->
    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">

    <div id="mc_embed_signup" class="sign-up-form" >
        <form action="https://docgiff.us7.list-manage.com/subscribe/post?u=9e9d969e91aa9d916bf0a977a&amp;id=8fcd0b4639" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">

                <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>

                <div class="field-row">
                    <div class="mc-field-group input-container">
                        <input type="email" value="" name="EMAIL" class="required email"  placeholder="Email*"  id="mce-EMAIL">
                    </div>
                </div>

                <div class="field-row">
                    <div class="mc-field-group input-container">
                        <input type="text" value="" name="FNAME" class="required"  placeholder="First Name*"  id="mce-FNAME">
                    </div>
                </div>

                <div class="field-row">
                    <div class="mc-field-group input-container">
                        <input type="text" value="" name="LNAME" class="required"  placeholder="Last Name*"  id="mce-LNAME">
                    </div>
                </div>

                <div class="field-row">
                    <div class="mc-field-group input-container">
                        <input type="text" value="" name="CITY" class="required"  placeholder="City*"  id="mce-CITY">
                    </div>
                </div>

                <div class="field-row hidden-field-row">
                    <div class="mc-field-group input-container">
                        <input type="text" value="" name="COUNTRY" id="mce-COUNTRY">
                    </div>
                </div>

                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>
                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9e9d969e91aa9d916bf0a977a_8fcd0b4639" tabindex="-1" value=""></div>
                <div class="clear">

                    <div class="subscribe-btn">
                        <input type="submit" value="Subscribe &rarr;" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                </div>

            </div>
        </form>
    </div>

    <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
    <script type='text/javascript'>
        (function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[4]='CITY';ftypes[4]='text';fnames[7]='COUNTRY';ftypes[7]='text';}(jQuery));var $mcj = jQuery.noConflict(true);
    </script>
    <!--End mc_embed_signup-->



</div>