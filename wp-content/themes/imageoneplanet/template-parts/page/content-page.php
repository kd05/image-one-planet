<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    $shop_banner_bg_img_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
    $shop_banner_bg_img = is_array($shop_banner_bg_img_array) ? current($shop_banner_bg_img_array) : get_default_banner();
    $banner_gradient = get_field("banner_shadow");
    ?>
    <div class="woocommerce-banner common-banner-page "  style=" background-image: url('<?php echo $shop_banner_bg_img; ?>')">

        <?php  if($banner_gradient != "no-shadow") { ?>
        <div class="common-banner-gradient"></div>
        <?php  } ?>
        <div class="common-banner-content">
            <div class="info-content">
                <h1><?php  echo get_the_title(); ?></h1>
            </div>
        </div>
    </div>



	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
