



<div class="user-sidebar-wrapper">
    <ul>

        <li class="has-submenu">
            <span>Member Account</span>
            <ul class="submenu">
                <li>
                    <a class="top-atag" href="<?php echo site_url();?>/profile">My Profile</a>
                    <ul class="submenu-lvl2">
                        <li><a href="<?php echo site_url();?>/edit-profile">Edit Profile</a></li>
                        <li><a href="<?php echo site_url();?>/upload-profile-gallery-image">Upload Gallery Image</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo site_url(); ?>/upgrade-membership">Upgrade Membership</a>
                    <ul class="submenu-lvl2">
                        <li><a href="<?php echo site_url();?>/benefit">Benefit</a></li>
                        <li><a href="<?php echo site_url();?>/upgrade-membership">Payment</a></li>
                    </ul>
                </li>
            </ul>
        </li>

        <li class="has-submenu">
            <span>Contest Submissions</span>
            <ul class="submenu">
                <li><a href="<?php echo site_url(); ?>/buy-credit">Entry Fee / Buy Credits</a></li>
                <li><a href="<?php echo site_url(); ?>/upload-art/">Upload New Entry / Image</a></li>
                <li><a href="<?php echo site_url(); ?>/arts-listing/">My Saved Entry / Image</a></li>
                <li><a href="<?php echo site_url(); ?>/my-submissions">My Submission</a></li>

                <?php  global $loginCheck;  $loggedin_user_role = $loginCheck->get_logged_in_user_role();
                if($loggedin_user_role == "jury" || $loggedin_user_role == "administrator") {
                    ?>
                    <li><a href="<?php echo site_url(); ?>/jury-view-submissions/">User Submissions</a></li>
                <?php } ?>

                <li><a href="<?php echo site_url(); ?>/help">Help / Q&A</a></li>

            </ul>
        </li>


    </ul>
</div>