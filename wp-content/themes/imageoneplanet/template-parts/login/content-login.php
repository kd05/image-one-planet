<div class="entry-content">
    <?php

    if (is_user_logged_in()) {
        echo '<div class="logout"> 
                    <p>You are already logged in. </p><br />
                    <p><a id="wp-submit" class="logout" href="'. wp_logout_url(site_url()."/login-page/").'" title="Logout">Logout</a></p>
            </div>';
    } else {
        $args = array(
            'echo'           => true,
            'redirect'       => site_url()."/profile",
            'label_log_in'   => __( 'Log in' ),
            'form_id'        => 'seminar-login',
            'label_username' => __( 'Email Address' ),
            'label_password' => __( 'Password' ),
            'label_remember' => __( 'Remember Me' ),
            'id_username'    => 'user_login',
            'id_password'    => 'user_pass',
            'id_submit'      => 'wp-submit',
            'remember'       => false,
            'value_username' => NULL,
            'value_remember' => true
        );
        wp_login_form($args);
    }


    ?>
</div><!-- .entry-content -->