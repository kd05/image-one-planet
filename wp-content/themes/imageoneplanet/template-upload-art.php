<?php
/**
 * Template Name: Upload Art Page
 * Template Post Type: Page
 *
 */


$page_allowed = ["participant","jury","administrator"];
$loginCheck->page_access_to_roles($page_allowed);
$user_arts = $obj->get_user_arts();
$total_arts = count($user_arts);

get_header();
the_post();



?>

    <div class="main-container" id="submit-entry">
        <section class="page-content">
            <?php get_template_part( 'template-parts/user-sidebar-menu/user', 'sidebar' ); ?>
            <h1><?php the_title(); ?></h1>

            <div class="message-instruction"><p><?php echo $obj->get_site_messages("inst_upload_art"); ?></p></div>

        <?php  if($total_arts < GP_MAX_USER_ARTS) { ?>
            <div class="upload-art-container">
                <form class="add-user-art-form form" id="add-user-art-form" action=""  enctype="multipart/form-data">
                    <div class="message-alert"></div>

                    <input type="hidden" id="security-user-art" value="<?php echo wp_create_nonce('security-user-art-nonce'); ?>">
                    <input type="hidden" name="action" id="action" value="add_user_art" />

                    <!-- images -->
                    <div class="input-wrapper">
                        <label for="images">Upload entry</label>
                        <input type="file" name="user-image-featured" id="user-image-featured" >
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper">
                        <label for="image-title">Title of entry</label>
                        <input type="text" name="image-title" id="image-title" class="required" value="" placeholder="Enter a title">
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper">
                        <label for="image-description">Description of entry</label>
                        <textarea  name="image-description" id="image-description"  class="required" placeholder="Enter a description"></textarea>
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper select-wrapper">
                        <label for="image-year-taken">Year taken</label>
                        <select name="image-year-taken" id="image-year-taken">
                            <option selected disabled>Select a year</option>
                            <?php
                            for ($x = 1920; $x <= date("Y"); $x++) {
                                echo '<option value="' . $x . '">' . $x . '</option>';
                            }
                            ?>
                        </select>
                    </div> <!-- /input-wrapper -->

                    <div class="button-wrapper">
                        <input type="submit" value="Save Post" tabindex="5" id="user_art_submit" name="user_art_submit" class="thread-button button blue" />
                    </div> <!-- /button-wrapper -->
                </form>

                <div class="instruction-page-content">
                    <?php  the_content(); ?>
                </div>

            </div>
            <?php } else {   ?>
                <p><?php echo $obj->get_site_messages("art_upload_limit_exceeds"); ?></p>
            <?php } ?>

        </section>
    </div>  <!-- /main-container -->

<?php get_footer(); ?>