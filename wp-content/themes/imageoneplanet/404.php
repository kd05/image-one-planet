<?php
/**
 * 404 TEMPLATE
 *
 **/
get_header();
?>

    <section class="page-content">
        <h1>404 Page Not Found</h1>
        <div>
            Page not found!
        </div>
    </section>

<?php get_footer(); ?>