                <?php


get_header();
the_post();

$post_id = get_the_ID();
// ####  Get User Data ####
$user_id = get_post_meta($post_id,"participant_user_id", true);
$user_info = $obj->get_user_data($user_id);
$user_url = get_author_posts_url($user_id);
extract($user_info);

// ####  Get Profile Galleries ####
$profile_galleries = $obj->get_user_profile_gallery_images($user_id);
$galleries_ids = array_map(function ($pg) {return $pg->ID;}, $profile_galleries);
//        converting array starting from 1 instead of 0
$galleries_ids = array_filter(array_merge(array(0), $galleries_ids));


?>

      <div class="main-container dark-background" id="user-profile-page">
        <section class="page-content">

            <div class="user-header">
                <h1>
                    <span class="user-name"><?php echo $full_name; ?></span><?php echo $featured == "yes" ? '<span class="featured"><i class="fas fa-star"></i></span>' : ''; ?>
                </h1>
                <h2><?php echo $usertype; ?></h2>

                <a href="<?php echo $user_url; ?>" class="button blue">See My Profile</a>
            </div>


            <div class="card-wrapper gallery-img-wrapper" id="gallery-img-wrapper" >

                <?php
                    $gallery_img = get_the_post_thumbnail_url($post_id, "custom-size-2000");
                    $gallery_title = get_the_title();
                    $gallery_content = get_the_content();
                    $year = get_post_meta($post_id,"image-year",true);
                    $status = get_post_meta($post_id,"image-status",true);
                    $price = get_post_meta($post_id,"image-price",true);
                $size = get_post_meta($post_id,"image-size",true);
                $frame = get_post_meta($post_id,"image-frame",true);
                $tag = get_the_excerpt($post_id);

                    ?>
                <div class="gallery-img" >
                    <img src="<?php echo $gallery_img; ?>" alt="">
                </div>

                <?php if(count($galleries_ids) > 1) {
                    $current_key = array_search($post_id, $galleries_ids);
                    $prev_link = $obj->get_previous_profile_galleries_link($galleries_ids,$current_key);
                    $next_link = $obj->get_next_profile_galleries_link($galleries_ids,$current_key);
                    ?>
                    <div class="gallery-nav">
                        <a href="<?php echo $prev_link; ?>#gallery-img-wrapper"><i class="fas fa-caret-left"></i></a>
                        <a href="<?php echo $next_link; ?>#gallery-img-wrapper"><i class="fas fa-caret-right"></i></a>
                    </div>
                 <?php } ?>

                <div class="gallery-info">
                    <h2><?php echo $gallery_title; ?></h2>
                    <p><?php echo nl2br($gallery_content); ?></p>

                    <p class="field-para"><span class="label">Year: </span><?php echo $year; ?> </p>

                    <p class="field-para"><span class="label">Status: </span><?php echo $status; ?> </p>

                    <?php if($price != "") { ?>
                        <p class="field-para"><span class="label">Price: </span><?php echo GP_CURRENCY_SYMBOL.$price; ?> </p>
                    <?php } ?>

                    <?php if($size != "") { ?>
                        <p class="field-para"><span class="label">Size: </span><?php echo $size; ?> </p>
                    <?php } ?>

                    <?php if($frame != "") { ?>
                        <p class="field-para"><span class="label">Frame: </span><?php echo $frame; ?> </p>
                    <?php } ?>

                    <?php if($tag != "") {  $tags = explode(",", $tag);    ?>
                        <p class="field-para"><span class="label">Tag: </span>
                            <span class="tags-listing">
                            <?php foreach ($tags as $single_tag){  ?>
                                <a href="<?php  echo site_url(); ?>/art-images/?search-tag=<?php echo $single_tag; ?>"><?php echo $single_tag; ?></a>
                            <?php } ?>
                            </span>

                        </p>
                    <?php } ?>

                    <p class="field-para"><span class="label">ID#: </span><?php echo $post_id; ?> </p>
                </div>



                <div class="profile-gallery-form">
                    <h2>Inquiry</h2>
                    <div class="message-alert"></div>

                    <form action="#" method="post" id="profile-gallery-interest">
                        <input type="hidden" name="gallery-id" value="<?php echo $post_id; ?>">
                        <input type="hidden" id="security-user-gallery-form" value="<?php echo wp_create_nonce('security-user-gallery-form-nonce'); ?>">

                        <div class="input-wrapper">
<!--                            <label for="pg-name">Name*</label>-->
                            <input type="text" name="pg-name" id="pg-name" class="required" value="" placeholder="Name*">
                        </div> <!-- /input-wrapper -->

                        <div class="input-wrapper">
<!--                            <label for="pg-email">Email*</label>-->
                            <input type="text" name="pg-email" id="pg-email" class="required chk-email" value="" placeholder="Email*">
                        </div> <!-- /input-wrapper -->

                        <div class="input-wrapper">
<!--                            <label for="pg-email">Message*</label>-->
                            <textarea name="pg-message" id="pg-message" cols="30" rows="10" placeholder="Message*"></textarea>
                        </div> <!-- /input-wrapper -->

                        <div class="button-wrapper">
                            <input type="submit" value="Submit Inquiry" tabindex="5" id="gallery_interest_submit" name="gallery_interest_submit" class="thread-button button blue" />
                        </div> <!-- /button-wrapper -->
                    </form>
                </div>


            </div>
            <!--            gallery-img-wrapper-->


        </section>
    </div>  <!-- /main-container -->

<?php get_footer(); ?>