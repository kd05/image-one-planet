<?php

get_header();
global $obj;

$current_contest = get_queried_object();

$contest_id           = $current_contest->term_id;
$contest_name         = $current_contest->name;

$upload_main_image_id = get_term_meta ($contest_id, 'upload_main_image_id',true);
$contest_mode         = get_term_meta ($contest_id, 'contest_mode', true);
$contest_deadline     = get_term_meta ($contest_id, 'contest_deadline',true);
$sponsor_image_id     = get_term_meta ($contest_id, 'sponsor_image_id', true);


// contest mode (All the old Contest should be in final mode, so just activating variable into final)
$finalMode      =  true;
$hero_bg = get_template_directory_uri() . '/assets/images/placeholders/placeholder-contest-over.jpg';


//echo '<pre>' . print_r(get_term_meta($contest_id), true) . '</pre>';
?>

    <div class="main-container" id="contest-page">

        <!-- FIRST PANEL -->
        <section class="hero-section<?php echo $finalMode ? ' final-mode-hero' : ''; ?>">
            <div class="hero-bg" style="background-image: url('<?php echo $hero_bg; ?>')"></div>
            <div class="text-wrapper">

                <div class="final-text">
                    <h1>The <?php echo $contest_name; ?> is Complete!</h1>
                    <h3>The Winners Have Been Chosen</h3>
                    <div class="down-arrow">
                        <i class="fas fa-arrow-down"></i>
                    </div>
                </div> <!-- /final-text -->

            </div> <!-- /text-wrapper -->
        </section> <!-- /hero-section -->



        <!-- SECOND PANEL GROUP -->
        <section class="winners-section" id="scroll-target">
            <div class="inner-container">

                <?php
                $tax_query = array( array( 'taxonomy' => 'submission_tag', 'field' => 'slug', 'terms' =>  'winner-gold' ),
                                    array( 'taxonomy' => 'contest_category', 'field' => 'id', 'terms' =>  $contest_id ));
                $winner_art_id = current($obj->get_post_by_multiple_meta("contest_submission",array(),1, $tax_query,"ids"));
                $winner_art_img = get_the_post_thumbnail_url($winner_art_id,"large");
                $winner_art_full_img = get_the_post_thumbnail_url($winner_art_id,"custom-size-2000");
                $winner_art_title = get_the_title($winner_art_id);
                $winner_price = get_term_meta($contest_id, 'prize_data_gold', true);
                $winner_user_id = get_post_meta($winner_art_id,"art_user_id",true);
                $first_name = get_user_meta($winner_user_id, 'first_name', true);
                $last_name = get_user_meta($winner_user_id, 'last_name', true);
                $winner_full_name = $first_name." ".$last_name;
                $submission_social_link = get_post_meta( $winner_art_id, 'submission_social_link', true );
                ?>
                <div class="flex-wrapper first-prize">
                    <div class="image-wrapper">
                        <div class="open-lightbox"
                             data-full-img="<?php echo $winner_art_full_img; ?>"
                             data-img-title="<?php echo $winner_art_title; ?>"
                             data-instagram-view="<?php echo $submission_social_link ? $submission_social_link : '#'; ?>"
                             data-artist-name="<?php echo $winner_full_name; ?>">
                            <i class="fas fa-search"></i>
                            <span>View</span>
                        </div>
                        <img src="<?php echo $winner_art_img; ?>" class="has-border" alt="First Place Winner">
                    </div> <!-- /image-wrapper -->
                    <div class="text-wrapper">
                        <div class="prize-unit">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/prize-trophy-first.png" alt="First Trophy">
                            <p class="artist"><?php echo $winner_full_name; ?></p>
                            <p class="art-title"><?php echo $winner_art_title; ?></p>
                            <p class="prize gold-text"><?php echo $winner_price; ?></p>
                        </div> <!-- /prize-unit -->
                    </div> <!-- /text-wrapper -->
                </div> <!-- /flex-wrapper -->


                <?php
                $tax_query = array( array( 'taxonomy' => 'submission_tag', 'field' => 'slug', 'terms' =>  'winner-silver' ),
                                    array( 'taxonomy' => 'contest_category', 'field' => 'id', 'terms' =>  $contest_id ));
                $winner_art_id = current($obj->get_post_by_multiple_meta("contest_submission",array(),1, $tax_query,"ids"));
                $winner_art_img = get_the_post_thumbnail_url($winner_art_id,"medium");
                $winner_art_full_img = get_the_post_thumbnail_url($winner_art_id,"custom-size-2000");
                $winner_art_title = get_the_title($winner_art_id);
                $winner_price = get_term_meta($contest_id, 'prize_data_silver', true);
                $winner_user_id = get_post_meta($winner_art_id,"art_user_id",true);
                $first_name = get_user_meta($winner_user_id, 'first_name', true);
                $last_name = get_user_meta($winner_user_id, 'last_name', true);
                $winner_full_name = $first_name." ".$last_name;
                $submission_social_link = get_post_meta( $winner_art_id, 'submission_social_link', true );

                ?>
                <div class="flex-wrapper second-prize">
                    <div class="image-wrapper">
                        <div class="open-lightbox" data-full-img="<?php echo $winner_art_full_img; ?>"
                             data-img-title="<?php echo $winner_art_title; ?>"
                             data-instagram-view="<?php echo $submission_social_link ? $submission_social_link : '#'; ?>"
                             data-artist-name="<?php echo $winner_full_name; ?>">
                            <i class="fas fa-search"></i>
                            <span>View</span>
                        </div>
                        <img src="<?php echo $winner_art_img; ?>" class="has-border" alt="Second Place Winner">
                    </div> <!-- /image-wrapper -->
                    <div class="text-wrapper">
                        <div class="prize-unit flex-wrapper">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/prize-trophy-second.png" alt="Second Trophy">
                            <div>
                                <p class="artist"><?php echo $winner_full_name; ?></p>
                                <p class="art-title"><?php echo $winner_art_title; ?></p>
                                <p class="prize blue-text"><?php echo $winner_price; ?></p>
                            </div>
                        </div> <!-- /prize-unit -->
                    </div> <!-- /text-wrapper -->
                </div> <!-- /flex-wrapper -->


                <?php
                $tax_query = array( array( 'taxonomy' => 'submission_tag', 'field' => 'slug', 'terms' =>  'winner-bronze' ),
                                    array( 'taxonomy' => 'contest_category', 'field' => 'id', 'terms' =>  $contest_id ));
                $winner_art_id = current($obj->get_post_by_multiple_meta("contest_submission",array(),1, $tax_query,"ids"));
                $winner_art_img = get_the_post_thumbnail_url($winner_art_id,"medium");
                $winner_art_full_img = get_the_post_thumbnail_url($winner_art_id,"custom-size-2000");
                $winner_art_title = get_the_title($winner_art_id);
                $winner_price = get_term_meta($contest_id, 'prize_data_bronze', true);
                $winner_user_id = get_post_meta($winner_art_id,"art_user_id",true);
                $first_name = get_user_meta($winner_user_id, 'first_name', true);
                $last_name = get_user_meta($winner_user_id, 'last_name', true);
                $winner_full_name = $first_name." ".$last_name;
                $submission_social_link = get_post_meta( $winner_art_id, 'submission_social_link', true );
                ?>
                <div class="flex-wrapper third-prize">
                    <div class="text-wrapper">
                        <div class="prize-unit flex-wrapper">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/prize-trophy-third.png" alt="Third Trophy">
                            <div>
                                <p class="artist"><?php echo $winner_full_name; ?></p>
                                <p class="art-title"><?php echo $winner_art_title; ?></p>
                                <p class="prize green-text"><?php echo $winner_price; ?></p>
                            </div>
                        </div> <!-- /prize-unit -->
                    </div> <!-- /text-wrapper -->
                    <div class="image-wrapper">
                        <div class="open-lightbox" data-full-img="<?php echo $winner_art_full_img; ?>"
                             data-img-title="<?php echo $winner_art_title; ?>"
                             data-instagram-view="<?php echo $submission_social_link ? $submission_social_link : '#'; ?>"
                             data-artist-name="<?php echo $winner_full_name; ?>">
                            <i class="fas fa-search"></i>
                            <span>View</span>
                        </div>
                        <img src="<?php echo $winner_art_img; ?>" class="has-border" alt="Third Place Winner">
                    </div> <!-- /image-wrapper -->
                </div> <!-- /flex-wrapper -->


            </div> <!-- /inner-container -->
        </section>

        <?php get_template_part( 'template-parts/gallery/gallery', 'mostpopular' ); ?>



        <?php get_template_part( 'template-parts/gallery/gallery', 'top20' ); ?>



    <!-- THIRD PANEL -->

    <!-- FOURTH PANEL -->

    <!-- FIFTH PANEL -->

    <!-- SIXTH PANEL -->

    <?php if (isset($sponsor_image_id) && !empty($sponsor_image_id)) { ?>

        <section class="sponsor-section">
            <div class="left-bg"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/placeholder-leaves.png" alt=""></div>
            <div class="inner-container flex-wrapper">
                <div class="image-wrapper">
                    <img src="<?php echo wp_get_attachment_image_src($sponsor_image_id, 'large')[0]; ?>" alt="Sponsor Image">
                </div>
                <?php
                $sponsor_title = get_term_meta($contest_id, 'sponsor_title',true);
                $sponsor_description = nl2br(get_term_meta($contest_id, 'sponsor_description',true));
                ?>
                <div class="text-wrapper">
                    <h1><?php echo $sponsor_title; ?></h1>
                    <p><?php echo $sponsor_description; ?></p>
                </div>
            </div> <!-- /inner-container -->
        </section> <!-- /sponsor-section -->

    <?php } ?>


    <!-- SEVENTH PANEL -->
    <?php

    if ($finalMode) {
        get_template_part('template-parts/gallery/gallery', 'lightbox');
    }
    ?>

    </div>  <!-- /main-container -->

<?php get_footer(); ?>