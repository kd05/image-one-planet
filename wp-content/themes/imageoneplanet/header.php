<?php
/**
 * HEADER TEMPLATE FILE
 *
 */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8">
    <title><?php wp_title(); ?></title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,300i,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.min.css">

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon.ico" type="image/x-icon">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>  oncontextmenu="return false;">
<div class="custom-site-outer">
    <div class="custom-site-wrapper">
        <header class="imone-header<?php echo is_front_page() ? ' home-header' : ''; ?>">
            <div class="inner">
                <a href="<?php echo get_site_url(); ?>" class="logo">
<!--                    <img src="--><?php //echo get_template_directory_uri() ;?><!--/assets/images/imageoneplanet-logo.png" alt="Image One Planet logo">-->
                    <img src="<?php echo get_template_directory_uri() ;?>/assets/images/logo.png" alt="Image One Planet logo">
                </a>
                <div class="header-nav">
                    <nav class="nav">
                        <?php
                        wp_nav_menu($args = array(
                            'menu'      => 'header-menu',
                            'container' => false,
                            'depth'     => 2
                        ));
                        ?>
                    </nav>

                    <div class="action-nav">
                        <?php if(is_user_logged_in()) { ?>
                            <a href="<?php echo site_url() ?>/profile/" class="button">Your Profile</a>
                            <a href="<?php echo wp_logout_url(site_url()."/login-page/"); ?>" class="button">Logout</a>
                        <?php  } else {  ?>
                            <a href="<?php echo site_url() ?>/create-an-account/" class="button">Create an Account</a>
                            <a href="<?php echo site_url() ?>/login-page/" class="button">Login</a>
                        <?php } ?>
                    </div>


<!--                    <div class="header-search-icon">-->
<!--                        <i class="fa fa-search search-icon" aria-hidden="true"></i>-->
<!--                    </div>-->

                </div> <!-- /header-nav -->
                <div id="mobile-icon">
                    <i class="fas fa-bars"></i>
                </div>
            </div> <!-- /inner -->
        </header> <!-- /header -->


        <!--    Search Form Pop Up-->
        <?php  get_template_part( 'template-parts/page/content', 'search-form-gp' );  ?>


        <div id="mobile-menu">
            <div class="mobile-menu-inner">

            </div>
        </div>
        <div class="imone-container">