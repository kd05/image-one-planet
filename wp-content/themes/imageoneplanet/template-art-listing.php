<?php
/**
 * Template Name: User Arts Listing Page
 * Template Post Type: Page
 *
 */

$page_allowed = ["participant","jury","administrator"];
$loginCheck->page_access_to_roles($page_allowed);
$user_arts = $obj->get_user_arts();


global $wp;
get_header();
the_post();

?>

    <section class="page-content">



            <?php get_template_part( 'template-parts/user-sidebar-menu/user', 'sidebar' ); ?>
            <h1><?php the_title(); ?></h1>

            <div class="message-alert"><?php  echo $obj->message_display_get_method("art_deleted_success"); ?></div>

            <?php  if(count($user_arts) > 0) { ?>

                <div class="art-listing-wrapper">

                <?php  foreach ($user_arts as $user_art) {
                    $art_id = $user_art->ID;
                    $art_link = get_the_permalink($art_id);
                    $title = get_the_title($art_id);
                    $description = substr($obj->get_the_content_by_id($art_id),0,100)."...";
                    $art_img = current(wp_get_attachment_image_src(get_post_thumbnail_id($art_id), 'thumbnail'));
                    $art_img_full = current(wp_get_attachment_image_src(get_post_thumbnail_id($art_id), 'custom-size-2000'));
                    ?>
                    <div class="art-single-container">
                        <div class="art-single">
                            <div class="art-img open-lightbox voteable"
                                 data-full-img="<?php echo $art_img_full; ?>"
                                 data-img-title="<?php echo $title; ?>">
                                <img src="<?php echo $art_img; ?>" alt="">
                            </div>
                            <h5><?php echo $title; ?></h5>
                            <p><?php echo nl2br($description); ?></p>

                            <div class="art-actions">
                                <a href="<?php echo site_url(); ?>/edit-art?art_id=<?php echo base64_encode($art_id); ?>"><i class="fa fa-edit"></i></a>
                                &nbsp; | &nbsp;
                                <a href="javascript:void(0);" class="delete-user-art" redirect-success="<?php echo home_url($wp->request); ?>"
                                   delete-art-title="<?php echo $title; ?>" delete-art-id="<?php echo $art_id; ?>"><i class="fa fa-trash"></i></a>
                            </div>
                            <div class="submit-to-contest-btn">
                                <a href="<?php echo $art_link; ?>" class="button blue">Submit to Contest</a>
                            </div>
                        </div>
                    </div>

                <?php } ?>
                </div>
           <?php } else {
                ?>   <div class="message-alert"><p><?php  echo $obj->get_site_messages("upload_art_to_submit_in_contest"); ?></p></div>   <?php
            }    ?>


    </section>

<?php
get_template_part('template-parts/gallery/gallery', 'lightbox');
get_footer();
?>