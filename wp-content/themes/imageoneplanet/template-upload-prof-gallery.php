<?php
/**
 * Template Name: Upload Profile Gallery Image
 * Template Post Type: Page
 *
 */


$page_allowed = ["participant","jury","administrator"];
$loginCheck->page_access_to_roles($page_allowed);

$user_id = get_current_user_id();
// ########  Get Profile Galleries ########
$meta_array = array("participant_user_id" => $user_id);
$profile_galleries = $obj->get_post_by_multiple_meta("user_profile_gallery",$meta_array);
$total_prof_images = count($profile_galleries);

$status_array = array("For Sale","Not for sale","Sold");

get_header();
the_post();


?>

    <div class="main-container" id="submit-entry">
        <section class="page-content">
            <?php get_template_part( 'template-parts/user-sidebar-menu/user', 'sidebar' ); ?>
            <h1><?php the_title(); ?></h1>

            <div class="message-instruction"><p><?php echo $obj->get_site_messages("inst_upload_art"); ?></p></div>

            <?php  if($total_prof_images < GP_MAX_USER_PROFILE_IMAGES) { ?>
            <div class="upload-prof-gallery-container">
                
                <form class="add-prof-gallery-form form" id="add-prof-gallery-form" action="" >
                    <div class="message-alert"></div>

                    <input type="hidden" id="security" value="<?php echo wp_create_nonce('security-prof-gallery-nonce'); ?>">
                    <input type="hidden" name="action" id="action" value="add_prof_gallery" />

                    <!-- images -->
                    <div class="input-wrapper">
                        <label for="images">Upload Image*</label>
                        <input type="file" name="prof-gallery-image" id="prof-gallery-image" />
                    </div> <!-- /input-wrapper -->

                    <!-- Submit button-->
                    <div class="input-wrapper">
                        <label for="image-title">Title*</label>
                        <input type="text" name="image-title" id="image-title" class="required" value="" placeholder="Enter a title">
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper">
                        <label for="image-description">Description*</label>
                        <textarea  name="image-description" id="image-description"  class="required" placeholder="Enter a description"></textarea>
                    </div> <!-- /input-wrapper -->


                    <div class="input-wrapper">
                        <label for="image-price">Size</label>
                        <input type="text" name="image-size" id="image-size" value="" placeholder="Size">
                        <div class="radio-flex">
                            <div class="radio-field">
                                <input type="radio" name="image-frame" id="image-frame" value="Frame Included" >
                                <span>Frame Included</span>
                            </div>
                            <div class="radio-field">
                                <input type="radio" name="image-frame" id="image-frame" value="Frame Excluded" >
                                <span>Frame Excluded</span>
                            </div>
                        </div>
                    </div> <!-- /input-wrapper -->


                    <div class="input-wrapper">
                        <label for="image-type">Select as a Profile Image (Only <?php echo GP_MAX_USER_PROFILE_FEATURED_IMAGES; ?> can be selected as a Profile Images)</label>
                        <p class="tooltip-info">(If you are a featured user, all 50 images will appear in the profile page)</p>
                        <div class="radio-wrap">
                            <div class="radio-field">
                                <input type="checkbox" name="image-type" id="image-type" value="featured_image" class="image-type">
                            </div>
                        </div>
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper">
                        <label for="listing-image-type">Select as a Listing Image (Only <?php echo GP_MAX_USER_PROFILE_LISTING_IMAGES; ?> can be selected as a Listing Image.)</label>
                        <div class="radio-wrap">
                            <div class="radio-field">
                                <input type="checkbox" name="listing-image-type" id="listing-image-type" value="listing_image" class="listing-image-type">
                            </div>
                        </div>
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper select-wrapper">
                        <label for="image-year-taken">Year*</label>
                        <select name="image-year-taken" id="image-year-taken" class="required">
                            <option value="">Select a year</option>
                            <?php
                            for ($x = 1920; $x <= date("Y"); $x++) {
                                echo '<option value="' . $x . '">' . $x . '</option>';
                            }
                            ?>
                        </select>
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper select-wrapper">
                        <label for="image-status">Status*</label>
                        <select name="image-status" id="image-status" class="required">
                            <?php foreach($status_array as $status){ ?>
                                <option value="<?php echo $status ?>"  ><?php echo $status ?></option>
                            <?php } ?>
                        </select>
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper">
                        <label for="image-price">Price/Value</label>
                        <input type="text" name="image-price" id="image-price" value="" placeholder="Price/Value">
                    </div> <!-- /input-wrapper -->


                    <div class="input-wrapper">
                        <label for="image-price">Tags (Comma separated)</label>
                        <p class="tooltip-info">(We recommend you to tag the art/image.  Tags allow website visitors to easily search your arts/images.)</p>
                        <input type="text" name="image-tag" id="image-tag" value="" placeholder="Tags">
                    </div> <!-- /input-wrapper -->



                    <div class="button-wrapper">
                        <input type="submit" value="Add Profile Gallery Image" tabindex="5" id="add_prof_gallery" name="add_prof_gallery" class="thread-button button blue" />
                    </div> <!-- /button-wrapper -->

                </form>
            </div>

            <div class="instruction-page-content">
                <?php  // the_content(); ?>
            </div>

            <?php } else {   ?>

                <p><?php echo $obj->get_site_messages("prof_gallery_upload_limit_exceeds"); ?></p>

            <?php } ?>

        </section>
    </div>  <!-- /main-container -->

<?php get_footer(); ?>