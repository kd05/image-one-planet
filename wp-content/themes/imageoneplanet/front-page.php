<?php
/**
 * FRONT PAGE TEMPLATE
 *
 **/
get_header();
the_post();

?>

<div class="main-container" id="homepage">

    <section class="hero-section">
        <?php $hero_bg = get_the_post_thumbnail() ? get_the_post_thumbnail_url() : get_template_directory_uri() . '/assets/images/placeholders/placeholder-home-hero.jpg'; ?>
        <div class="hero-bg" style="background-image: url('<?php echo $hero_bg; ?>')"></div>
        <div class="text-wrapper">
            <div>
                <h1>
                    <?php echo get_field('hero_heading'); ?>
                </h1>
                <div class="button-wrapper">
                    <a href="<?php echo site_url() ?>/arts-listing/" class="button">Submit Entry</a>
                    <?php if(!is_user_logged_in()) { ?> <a href="<?php echo site_url() ?>/create-an-account/" class="button">Create an Account</a> <?php } ?>

                </div>
            </div>
        </div> <!-- /text-wrapper -->

        <?php
        $meta_array = array("old_contest" => "previous");
        $prev_contest = $obj->get_terms_by_multiple_meta("contest_category",$meta_array);
        if(is_array($prev_contest) && count($prev_contest) > 0){
            $prev_contest_id = current($prev_contest)->term_id;
            $prev_contest_title = current($prev_contest)->name;
            $prev_contest_link = get_term_link($prev_contest_id);
        ?>
        <div class="contest-link-wrapper">
            <a href="<?php echo $prev_contest_link; ?>" class="last-contest-link">
                <!-- TODO: add dynamic last contest title -->
                <h2>
                    View Results from our last contest: <?php echo $prev_contest_title; ?>
                </h2>
                <i class="fas fa-angle-right"></i>
            </a>
        </div>
        <!-- /contest-link-wrapper -->
        <?php  } ?>

        <?php
        if (get_field('hero_background_artist_name')) {
            $hero_artist_link = get_field('hero_background_artist_link') ? get_field('hero_background_artist_link') : '#';
        ?>

        <div class="credit">
            <a href="<?php echo $hero_artist_link; ?>" class="button">Photo By <?php echo get_field('hero_background_artist_name'); ?></a>
        </div> <!-- /credit -->

        <?php } ?>

    </section> <!-- /hero-section -->



    <section class="cta-section">
        <?php
        $cta1_bg = get_field('cta_1_background_image') ? get_field('cta_1_background_image') : get_template_directory_uri() . '/assets/images/placeholders/placeholder-home-cta-1.jpg';
        $cta2_bg = get_field('cta_2_background_image') ? get_field('cta_2_background_image') : get_template_directory_uri() . '/assets/images/placeholders/placeholder-home-cta-2.jpg';
        $cta3_bg = get_field('cta_3_background_image') ? get_field('cta_3_background_image') : get_template_directory_uri() . '/assets/images/placeholders/placeholder-home-cta-3.jpg';
        ?>
        <a href="<?php echo get_field('cta_1_link'); ?>" class="cta" style="background-image: url('<?php echo $cta1_bg; ?>')">
            <div class="text-wrapper blue">
                <h3><?php echo get_field('cta_1_title'); ?></h3>
            </div>
        </a>
        <a href="<?php echo get_field('cta_2_link'); ?>" class="cta" style="background-image: url('<?php echo $cta2_bg; ?>')">
            <div class="text-wrapper green">
                <h3><?php echo get_field('cta_2_title'); ?></h3>
            </div>
        </a>
        <a href="<?php echo get_field('cta_3_link'); ?>" class="cta" style="background-image: url('<?php echo $cta3_bg; ?>')">
            <div class="text-wrapper purple">
                <h3><?php echo get_field('cta_3_title'); ?></h3>
            </div>
        </a>
    </section> <!-- /cta-section -->

    <section class="intro-section">
        <div class="left-bg"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/placeholder-intro-left.jpg" alt=""></div>
        <div class="inner-container">
            <div class="text-wrapper">
                <?php the_content(); ?>
<!--                <a href="--><?php //echo site_url(); ?><!--/about" class="button blue">Read More</a>-->
            </div> <!-- /text-wrapper -->
        </div> <!-- /inner-container -->
        <div class="right-bg"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/placeholder-intro-right.jpg" alt=""></div>
    </section> <!-- /into-section -->



    <section class="home-featured-wrapper">
        <?php

        $feat_artist_args = array('meta_query'=>
            array( 'relation' => 'AND',
                array('key' => 'usertype','value' => 'artist', 'compare' => "=",),
                array('key' => 'hide_on_home','value' => 'no', 'compare' => "=",),
                array('key' => 'featured','value' => 'yes','compare' => "=",)));
        $feat_artist_users = get_users( $feat_artist_args );
        echo $obj->home_page_featured_user_section($feat_artist_users,"Artist");

        $feat_photographer_args = array('meta_query'=>
            array( 'relation' => 'AND',
                array('key' => 'usertype','value' => 'photographer', 'compare' => "=",),
                array('key' => 'hide_on_home','value' => 'no', 'compare' => "=",),
                array('key' => 'featured','value' => 'yes','compare' => "=",)));
        $feat_photographer_users = get_users( $feat_photographer_args );
        echo $obj->home_page_featured_user_section($feat_photographer_users,"Photographer");
        ?>
    </section>



    <?php

    //$latest_news =  get_field('latest_news');
    $latest_post_arg = array("numberposts" => 1);
    $latest_post_array = wp_get_recent_posts($latest_post_arg);
    if(count($latest_post_array) > 0 ) {

    $latest_post = $latest_post_array[0];
    $latest_post_id = $latest_post['ID'];
    $gallery_img = get_the_post_thumbnail_url($latest_post_id, "large");
    $latest_post_title = $latest_post['post_title'];
    $content = $obj-> get_the_content_by_id($latest_post_id);
    $latest_post_content = gp_excerptize($content, 68);

    ?>

        <section class="newsletter-section latest-news" id="home-whats-new">
            <div class="inner-container">
                <div class="newsletter-wrapper">
                    <h1><?php echo $latest_post_title; ?></h1>
                    <div class="date-author">
                    </div>

                    <?php  if($gallery_img != "") { ?>
                        <div class="blog-img">
                            <img src="<?php echo $gallery_img; ?>" alt="">
                        </div>
                    <?php  } ?>

                    <p><?php echo $latest_post_content; ?></p>

                    <a href="<?php  echo get_the_permalink($latest_post_id) ?>" class="button blue">Read More</a>
                    <?php //echo get_field('latest_news'); ?>

                </div> <!-- /newsletter-wrapper -->
            </div> <!-- /inner-container -->
        </section> <!-- /newsletter-section -->
    <?php } ?>



    <section class="newsletter-section">
        <div class="left-bg cornelius"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/placeholder-newsletter-left.png" alt=""></div>
        <div class="inner-container">
            <div class="newsletter-wrapper">
                <?php echo get_field('newsletter_content'); ?>

                <!-- TODO: delete commented code here -->
<!--                <h1>Join the Image One Planet Newsletter</h1>-->
<!--                <p>Nulla pellentesque dolor in neque tincidunt aliquet. Nulla tincidunt-->
<!--                    interdum dolor, vel imperdiet leo vestibulum sed. </p>-->


<!--                <form action="" class="form">-->
<!--                    <div class="input-wrapper">-->
<!--                        <input type="text" placeholder="Enter Your Email">-->
<!--                        <button type="submit" class="button gold">Subscribe</button>-->
<!--                    </div>-->
<!--                </form>-->


                <div id="mc_embed_signup">
                    <form action="https://gmail.us20.list-manage.com/subscribe/post?u=f7bb37eae323b6a74b9fc24ca&amp;id=0aba5259d0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form" target="_blank" novalidate>

                        <div class="input-wrapper">

                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL"  placeholder="Enter Your Email">

                            <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button gold">
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_f7bb37eae323b6a74b9fc24ca_0aba5259d0" tabindex="-1" value=""></div>
                        <!--                        </div>-->
                    </form>
                </div>
                <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                <!--End mc_embed_signup-->

            </div> <!-- /newsletter-wrapper -->
        </div> <!-- /inner-container -->
        <div class="right-bg"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/placeholder-newsletter-right.jpg" alt=""></div>
    </section> <!-- /newsletter-section -->






    <section class="submit-cta-section">
        <?php
        $submit_cta_snapshot_1 = get_field('submit_cta_main_image') ? get_field('submit_cta_main_image') : get_template_directory_uri() . '/assets/images/placeholders/placeholder-snapshot2.jpg';
        $submit_cta_snapshot_2 = get_field('submit_cta_top_image') ? get_field('submit_cta_top_image') : get_template_directory_uri() . '/assets/images/placeholders/placeholder-snapshot1.jpg';
        $submit_cta_snapshot_3 = get_field('submit_cta_bottom_image') ? get_field('submit_cta_bottom_image') : get_template_directory_uri() . '/assets/images/placeholders/placeholder-snapshot3.jpg';
        ?>
        <div class="left-bg"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/placeholders/placeholder-submit-cta-left.jpg" alt=""></div>
        <div class="inner-container">
            <div class="text-wrapper">
                <?php echo get_field('submit_cta_content'); ?>

                <!-- TODO: delete commented code here -->
<!--                <h1>Submit Your Entry Today!</h1>-->
<!--                <p>Nulla pellentesque dolor in neque tincidunt aliquet. Nulla tincidunt interdum-->
<!--                    dolor, vel imperdiet leo vestibulum sed.</p>-->
<!--                <a href="#" class="button blue">Submit an Entry</a>-->

            </div> <!-- /text-wrapper -->
            <div class="snapshot-wrapper">
                <div class="snap has-border"><div class="inner-snap" style="background-image: url('<?php echo $submit_cta_snapshot_1; ?>')"></div></div>
                <div class="snap has-border"><div class="inner-snap" style="background-image: url('<?php echo $submit_cta_snapshot_2; ?>')"></div></div>
                <div class="snap has-border"><div class="inner-snap" style="background-image: url('<?php echo $submit_cta_snapshot_3; ?>')"></div></div>
            </div> <!-- /snapshot-wrapper -->
        </div> <!-- /inner-container -->
    </section> <!-- /submit-cta-section -->
</div>

<?php get_footer(); ?>
