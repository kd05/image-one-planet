<?php
/**
 * Template Name: Buy Credit Page
 * Template Post Type: Page
 *
 */


$page_allowed = ["participant","jury","administrator"];
$loginCheck->page_access_to_roles($page_allowed);

$meta_array = array("payment_type" => "credit");
$payment_types = $obj->get_terms_by_multiple_meta("payment_type" , $meta_array);

$contest_id = $obj->get_active_contest_id();
get_header();
the_post();
?>

    <div class="main-container" id="submit-entry">
        <section class="page-content">
            <?php get_template_part( 'template-parts/user-sidebar-menu/user', 'sidebar' ); ?>
            <h1><?php the_title(); ?></h1>

            <div class="buy-credit-container">

                <span class="payment-errors"></span>
                <div class="message-alert"></div>

                <?php if($contest_id && $obj->get_active_contest_mode($contest_id) == "submission_mode"){  ?>

                <div class="payment-credit-card-wrapper">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/payment-online.png" alt="">
                </div>

                <form id="paymentFrm" class="paymentFrm" action="" method="post">

                    <input type="hidden" id="security-stripe-pay" value="<?php echo wp_create_nonce('security-stripe-pay-nonce'); ?>">
                    <input type="hidden" name="action" id="action" value="buy_credit_or_upgrade_membership" />
                    <input type="hidden" name="payment_type" id="payment_type" value="credit" />

                    <div class="input-wrapper select-wrapper">
                        <label for="package-id">Select a Credit Package*</label>
                        <select name="package-id" id="package-id" class="required">
                            <option value="">-- Select Package --</option>
                            <?php
                            $payment_tax_setting = get_option( 'payment_tax_setting', '' );
                            foreach ($payment_types as $payment_type){
                                    $pay_type_id = $payment_type->term_id;
                                    $pay_type_name = $payment_type->name;
                                    $pay_type_price = get_term_meta ( $pay_type_id, 'payment_price',true);
                                echo '<option value="' . $payment_type->term_id . '">' . $pay_type_name. " (" .GP_CURRENCY_SYMBOL . $pay_type_price . ' + TAX '.$payment_tax_setting.'%)</option>';
                            }
                            ?>
                        </select>
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper">
                        <label for="card-number">Card Number*</label>
                        <input type="text" name="card_num" maxlength="20" value="" autocomplete="off"  placeholder="Card Number"  class="card-number required" id="card-number"/>
                    </div> <!-- /input-wrapper -->


                    <div class="flex-wrapper-100">
                        <div class="flex-wrapper-50">
                            <div class="input-wrapper">
                                <label for="cvc">CVC*</label>
                                <input type="text" name="cvc" maxlength="4" value=""  autocomplete="off" placeholder="CVC" class="cvc required" id="cvc"/>
                            </div> <!-- /input-wrapper -->
                        </div>
                        <div class="flex-wrapper-50">
                            <div class="input-wrapper">
                                <label for="cvc">Expiration (MM/YYYY)*</label>
                                <div class="flex-wrapper-inner">
                                    <input type="text" name="exp_month" maxlength="2" value="" placeholder="MM" class="card-expiry-month required" id="card-expiry-month"/>

                                    <input type="text" name="exp_year" maxlength="4"  value="" placeholder="YYYY" class="card-expiry-year required" id="card-expiry-year"/>
                                </div>
                            </div>
                            <!-- /input-wrapper -->
                        </div>
                    </div>

                    <div class="input-wrapper">
                        <label for="card-number">Coupon Code</label>
                        <input type="text" name="coupon_code" value="" autocomplete="off"  placeholder="Coupon Code"  class="coupon_code " id="coupon_code"/>
                    </div> <!-- /input-wrapper -->

                    <div class="payment-message"></div>


                    <div class="button-wrapper">
                        <input type="submit" value="Buy Credit" tabindex="5" id="payBtn" name="user_art_submit" class="thread-button button blue" />
                    </div> <!-- /button-wrapper -->

                </form>
                <?php  } else { ?>
                    <div class="message-alert"><?php  echo "<p>".$obj->get_site_messages("contest_not_submission_payment_disabled")."</p>"; ?></div>
                <?php } ?>
            </div>


        </section>
    </div>  <!-- /main-container -->

<?php get_footer(); ?>