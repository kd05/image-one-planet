<?php
/**
 * Template Name: Profile Page
 * Template Post Type: Page
 *
 */


get_header();
//the_post();

$user_obj = get_queried_object();
// ####  Get User Data ####
$user_id = $user_obj->ID;
$user_info = $obj->get_user_data($user_id);
extract($user_info);

// ####  Get Profile Galleries ####
$profile_galleries = $obj->get_user_profile_gallery_images($user_id);

?>

    <div class="main-container dark-background" id="user-profile-page">
        <section class="page-content">

            <div class="user-header">
                <h1>
                    <span class="user-name"><?php echo $full_name; ?></span><?php echo $featured == "yes" ? '<span class="featured"><i class="fas fa-star"></i></span>' : ''; ?>
                </h1>
                <h2><?php echo $usertype; ?></h2>
            </div>


            <div class="card-wrapper user-profile-container">

                <div class="flex-wrapper">
                    <div class="headshot-wrapper">

                        <?php
                        $headshot_image = $headshot_id ? current(wp_get_attachment_image_src( $headshot_id, 'medium')) : get_no_profile_pic_found();

                        ?>
                        <img src="<?php echo $headshot_image; ?>" alt="">

                    </div>
                    <div class="profile-details-wrapper">
                        <?php // if ($website_link != "" && $facebook_link != "" && $instagram_link != "") { ?>
                            <div class="profile-links">
                                <p><?php if($website_link != "") { ?><i class="fas fa-globe"></i> <a href="<?php echo $website_link; ?>" target="_blank"><?php echo $website_link; ?></a> <?php } ?></p>
                                <p><?php if($facebook_link != "") { ?><i class="fab fa-facebook-f"></i> <a href="https://www.facebook.com/<?php echo $facebook_link; ?>" target="_blank">https://www.facebook.com/<?php echo $facebook_link; ?></a> <?php } ?></p>
                                <p><?php if($instagram_link != "") { ?><i class="fab fa-instagram"></i> <a href="https://www.instagram.com/<?php echo $instagram_link; ?>" target="_blank">https://www.instagram.com/<?php echo $instagram_link; ?></a> <?php } ?></p>
                            </div>
                        <?php // } ?>
                        <div>
                            <h3>About Me</h3>
                            <p><?php echo $description ? $description : 'This user has not written a description yet.'; ?></p>
                        </div>

                            <div>
                                <h3>Details</h3>
                                <?php if ($birth_date !== "") { ?>
                                <p class="flex-wrapper"><span class="label">Year of Birth</span><span><?php echo $birth_date; ?></span></p>
                                <?php  }  if ($country !== "") {  ?>
                                <p class="flex-wrapper"><span class="label">Country</span><span><?php echo $country; ?></span></p>
                                <?php } if($gallery_name != "") {?>
                                <p class="flex-wrapper"><span class="label">Gallery</span><span><?php echo $gallery_name; ?></span></p>
                                <?php } ?>
                            </div>

                    </div>
                </div> <!-- /flex-wrapper -->

            </div> <!-- /card-wrapper -->


            <div class="card-wrapper profile-gallery-container">

                <?php if(count($profile_galleries) > 0) { ?>

                    <div class="inner-gallery-wrapper wookmark-container">

                        <?php
                        foreach ($profile_galleries as $prof_gallery) {
                            $gallery_id       = $prof_gallery->ID;
                            $gallery_img_url  = get_the_post_thumbnail_url($gallery_id, "imageone-gallery");
                            $gallery_title    = get_the_title($gallery_id);
                            $gallery_content  = substr($prof_gallery->post_content,0,50)."...";
                            $gallery_url      = get_the_permalink($gallery_id);
                            $gallery_price    = get_post_meta($gallery_id,"image-price",true);
                            $gallery_year     = get_post_meta($gallery_id,"image-year",true);
                            $is_featured      = get_post_meta($gallery_id,"image-type",true);

                            // get image height to determine whether an <img> or background-image should be used
                            // this is so the image-overlay does not get cut off on shorter images
                            $gallery_img_meta = wp_get_attachment_metadata(get_post_thumbnail_id($gallery_id));
//                            $gallery_img_size = $gallery_img_meta['sizes']['imageone-gallery']['height'];
                            $gallery_img_size = isset($gallery_img_meta['sizes']['imageone-gallery']['height']) ? $gallery_img_meta['sizes']['imageone-gallery']['height'] : 250;
                            ?>
                            <div class="gallery-image-wrapper">

                                <a href="<?php echo $gallery_url; ?>" class="gal-link">
                                    <?php if ($gallery_img_size > 250) { ?>
                                        <img src="<?php echo $gallery_img_url; ?>" alt="">
                                    <?php } else { ?>
                                        <div class="gal-image" style="background-image: url('<?php echo $gallery_img_url; ?>')"></div>
                                    <?php } ?>
                                </a>

                                <div class="image-overlay">
                                    <?php if (!empty($gallery_price)) { ?> <p class="gal-price">$<?php echo $gallery_price; ?></p> <?php } ?>
                                    <p class="gal-title"><?php echo $gallery_title; ?></p>
                                    <p class="gal-year"><?php echo $gallery_year; ?></p>
                                    <p class="gal-description"><?php echo nl2br($gallery_content); ?></p>

                                    <div class="gal-icons">
                                        <a href="<?php echo $gallery_url; ?>" title="View Gallery Image"><i class="fas fa-eye"></i></a>
                                    </div>

                                </div> <!-- /image-overlay -->

                            </div> <!-- /gallery-image-wrapper -->

                        <?php } ?>

                    </div> <!-- /inner-gallery-wrapper -->

                <?php } else {  ?>

                    <?php echo $obj->get_site_messages("no_profile_gallery"); ?>

                <?php } ?>

            </div>
        </section>
    </div>  <!-- /main-container -->

<?php get_footer(); ?>