<?php
/**
 * Template Name: Profile Gallery Search Page
 * Template Post Type: Page
 *
 */
get_header();

//exit;

$where_tag = (isset($_GET['search-tag']) && $_GET['search-tag'] != "")  ? " AND ( $wpdb->posts.post_title LIKE '%".$_GET['search-tag']."%' OR  $wpdb->posts.post_content LIKE '%".$_GET['search-tag']."%' OR  $wpdb->posts.post_excerpt LIKE '%".$_GET['search-tag']."%')" : " ";

$users_selected = (!isset($_GET['user_type'])) ? array("artist","photographer") : $_GET['user_type'];
$where_users = (count($users_selected) == 2) ?   " ( um.meta_value='artist' OR  um.meta_value='photographer') " : " um.meta_value='".current($users_selected)."' ";

$query = "
            SELECT $wpdb->posts.ID        
            FROM $wpdb->posts            
            INNER JOIN $wpdb->postmeta AS cu ON ($wpdb->posts.ID = cu.post_id AND cu.meta_key='participant_user_id' )          
            INNER JOIN $wpdb->usermeta AS um ON (cu.meta_value = um.user_id AND um.meta_key='usertype' AND $where_users)           
            WHERE $wpdb->posts.post_type = 'user_profile_gallery'
            AND $wpdb->posts.post_status = 'publish'
            ${where_tag}
            GROUP BY $wpdb->posts.ID ORDER BY $wpdb->posts.post_date DESC
        ";
$sql_result = $wpdb->get_results( $query, OBJECT);

//echo '<pre>' . print_r( $sql_result, true ) . '</pre>';
$total = count($sql_result);
//$items_per_page = 15;
$page = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;
$offset = ( $page * PROFILE_GALLERY_PER_PAGE ) - PROFILE_GALLERY_PER_PAGE;
$latestposts = $wpdb->get_results( $query . "  LIMIT ${offset}, ".PROFILE_GALLERY_PER_PAGE );


?>







    <div class="main-container dark-background" id="user-listings-page">
        <section class="page-content">

            <h1>Art Images</h1>

            <div class="card-wrapper">
                <form class="search-user-form form" id="search-user-form" action="<?php echo site_url(); ?>/art-images" >
                    <div class="flex-wrapper">


                        <div class="input-wrapper input-radio-wrapper">
                            <label for="image-year-taken"> <h3>Search</h3></label>
                        </div>
                        <!-- /input-wrapper -->

                        <div class="input-wrapper input-radio-wrapper">
                            <div class="radio-flex">
                                <div class="radio-wrap">
                                    <div class="radio-field">
                                        <input type="checkbox" name="user_type[]"  value="artist" <?php if(in_array("artist",$users_selected)) echo "checked"; ?>>
                                    </div>
                                    <span class="label">Artist</span>
                                </div>
                                <div class="radio-wrap">
                                    <div class="radio-field">
                                        <input type="checkbox" name="user_type[]"  value="photographer" <?php if(in_array("photographer",$users_selected)) echo "checked"; ?>>
                                    </div>
                                    <span class="label">Photographer</span>
                                </div>
                            </div>

                        </div> <!-- /input-wrapper -->

                        <div class="input-wrapper ">
                            <label for="image-year-taken">Tag</label>
                            <input type="text" placeholder="Search Tags" name="search-tag" value="<?php echo $search_tag = isset( $_GET['search-tag'] ) ? $_GET['search-tag']  : ''; ?>">
                        </div> <!-- /input-wrapper -->

                        <div class="button-wrapper">
                            <input type="submit" value="Search" tabindex="5" id="search_user" name="search_user" class="thread-button button blue" />
                        </div> <!-- /button-wrapper -->
                    </div>
                </form>
            </div> <!-- /card-wrapper -->



            <div class="card-wrapper user-listing-container">

                    <?php //if ( have_posts() ) : ?>

                    <div class="wookmark-container">

                        <?php

                        foreach ($latestposts as $latestpost) {

                            $gallery_id       = $latestpost->ID;
                            $gallery_img_url  = get_the_post_thumbnail_url($gallery_id, "imageone-gallery");
                            $gallery_title    = get_the_title($gallery_id);
//                            $gallery_content  = substr(get_the_content($gallery_id),0,50)."...";
                            $gallery_url      = get_the_permalink($gallery_id);
                            $gallery_price    = get_post_meta($gallery_id,"image-price",true);
                            $gallery_year     = get_post_meta($gallery_id,"image-year",true);
                            $is_featured      = get_post_meta($gallery_id,"image-type",true);
                            $is_listing_image = get_post_meta($gallery_id,"listing-image-type",true);

                            // get image height to determine whether an <img> or background-image should be used
                            // this is so the image-overlay does not get cut off on shorter images
                            $gallery_img_meta = wp_get_attachment_metadata(get_post_thumbnail_id($gallery_id));
                            $gallery_img_size = isset($gallery_img_meta['sizes']['imageone-gallery']['height']) ? $gallery_img_meta['sizes']['imageone-gallery']['height'] : 250;

                            ?>

                            <div class="single-user-wrapper" >

                                <a href="<?php echo $gallery_url; ?>" class="user-link">
                                    <?php if ($gallery_img_size > 250) { ?>
                                        <img src="<?php echo $gallery_img_url; ?>" alt="">
                                    <?php } else { ?>
                                        <div class="first-gallery-image" style="background-image: url('<?php echo $gallery_img_url; ?>')"></div>
                                    <?php } ?>
                                </a>

                                <div class="image-overlay">
                                    <?php if (!empty($gallery_price)) { ?> <p class="gal-price">$<?php echo $gallery_price; ?></p> <?php } ?>
                                    <?php if (!empty($gallery_title)) { ?> <p class="gal-title"><?php echo $gallery_title; ?></p> <?php } ?>
                                    <?php if (!empty($gallery_year)) { ?> <p class="gal-year"><?php echo $gallery_year; ?></p> <?php } ?>
                                    <?php if (!empty($gallery_content)) { ?> <p class="gal-description"><?php echo nl2br($gallery_content); ?></p> <?php } ?>
                                    <!-- TODO: only show icons if own profile -->
                                    <div class="gal-icons">
                                        <a href="<?php echo $gallery_url; ?>" title="View <?php echo $gallery_title ?> Profile"><i class="fas fa-eye"></i></a>
                                    </div>
                                </div> <!-- /image-overlay -->
                            </div>

                            <?php
                        }


                        ?>

                    </div><!-- /wookmark-container -->
                        <?php

                            echo paginate_links( array(
                                'base' => add_query_arg( 'cpage', '%#%' ),
                                'format' => '',
                                'type' => 'list',
                                'prev_text' => __( ' <i class="fas fa-caret-left"></i> ', 'imageoneplanet' ),
                                'next_text' => __( ' <i class="fas fa-caret-right"></i> ', 'imageoneplanet' ),
                                'total' => ceil($total / PROFILE_GALLERY_PER_PAGE),
                                'current' => $page
                            ));
                    ?>



            </div> <!-- /card-wrapper -->
        </section>
    </div>
    <!-- /main-container -->

<?php get_footer(); ?>
