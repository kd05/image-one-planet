<?php

$page_allowed = ["participant","jury","administrator"];
$loginCheck->page_access_to_roles($page_allowed);


$user_id = get_current_user_id();
$meta_array = array(
    "payment_user_id" => $user_id,
);
$payments = $obj->get_post_by_multiple_meta("user_payment",$meta_array,-1);
echo '<pre>' . print_r( $payments, true ) . '</pre>';


global $wp;
get_header();
the_post();

?>

    <section class="page-content">
        <h1><?php the_title(); ?></h1>
        <div>

            <?php get_template_part( 'template-parts/user-sidebar-menu/user', 'sidebar' ); ?>


            <div class="message-alert"><?php  echo $obj->message_display_get_method("art_deleted_success"); ?></div>

            <?php  if(count($payments) > 0) { ?>

                <div class="art-listing-wrapper">


                <?php  foreach ($payments as $payment) {
                    $payments_id = $payments->ID;
                    $payment_stripe_id = trim(get_post_meta( $payments_id, 'payment_stripe_id', true ));
                    $payment_stripe_customer = trim(get_post_meta( $payments_id, 'payment_stripe_customer', true ));
                    $payment_stripe_balance_transaction = trim(get_post_meta( $payments_id, 'payment_stripe_balance_transaction', true ));
                    $payment_stripe_currency = trim(get_post_meta( $payments_id, 'payment_stripe_currency', true ));
                    echo $payment_stripe_receipt_url = trim(get_post_meta( $payments_id, 'payment_stripe_receipt_url', true ));
                    ?>
                    <div class="art-single-container">
                        <div class="art-single">
                            <div class="art-img">
                                <img src="<?php echo $art_img; ?>" alt="">
                            </div>
                            <h5><?php echo $title; ?></h5>
                            <p><?php echo nl2br($description); ?></p>

                            <div class="art-actions">
                                <a href="<?php echo site_url(); ?>/edit-art?art_id=<?php echo base64_encode($art_id); ?>"><i class="fa fa-edit"></i></a>
                                &nbsp; | &nbsp;
                                <a href="javascript:void(0);" class="delete-user-art" redirect-success="<?php echo home_url($wp->request); ?>"
                                   delete-art-title="<?php echo $title; ?>" delete-art-id="<?php echo $art_id; ?>"><i class="fa fa-trash"></i></a>
                            </div>
                            <div class="submit-to-contest-btn">
                                <a href="<?php echo $art_link; ?>" class="button blue">Submit to Contest</a>
                            </div>
                        </div>
                    </div>

                <?php } ?>
                </div>
           <?php } else {
                ?>   <div class="message-alert"><p><?php  echo $obj->get_site_messages("no_record"); ?></p></div>   <?php
            }    ?>

        </div>
    </section>

<?php get_footer(); ?>