<?php
/**
 * Template Name: Archive Contest Listing
 * Template Post Type: Page
 */

get_header();
the_post();

$meta_array = array("old_contest" => "older");
$contests = $obj->get_terms_by_multiple_meta("contest_category",$meta_array);
?>

    <section class="page-content">
        <h1><?php the_title(); ?></h1>
        <?php  if(count($contests) > 0) { ?>
                <div class="archive-contest-wrapper">
                    <?php
                    foreach ($contests as $contest) {
                        $contest_id = $contest->term_id;
                        $contest_name = $contest->name;
                        $contest_link = get_term_link($contest_id);
                        $contest_main_image_id = get_term_meta ( $contest_id, 'upload_main_image_id',true);
                        $contest_main_image = ($contest_main_image_id != "") ? current(wp_get_attachment_image_src( $contest_main_image_id, 'large')) : get_image_not_found();
                        ?>
                        <div class="archive-contest"  style="background-image: url('<?php echo $contest_main_image; ?>')" onclick="location.href='<?php echo $contest_link; ?>'">
                            <div class="archive-gradient"></div>
                            <h1><?php echo $contest_name; ?></h1>
                        </div>
                    <?php }  ?>
                </div>
                <?php  } else {  ?>
                    <div class="message-alert"><p><?php  echo $obj->get_site_messages("no_record"); ?></p></div>
                <?php  }  ?>
    </section>

<?php get_footer(); ?>