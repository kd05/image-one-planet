<?php
/**
 * FUNCTIONS
 *
 **/

// Allow custom navigation
function register_custom_menu()
{
    register_nav_menu('header-menu', __('Header Menu'));
    register_nav_menu('footer-menu-1', __('Footer Menu 1'));
    register_nav_menu('footer-menu-2', __('Footer Menu 2'));
    register_nav_menu('footer-menu-3', __('Footer Menu 3'));
    register_nav_menu('footer-menu-4', __('Footer Menu 4'));
    register_nav_menu('footer-social-menu', __('Footer Social Menu'));
}
add_action('init', 'register_custom_menu');


// Hide admin bar when viewing site
add_filter('show_admin_bar', '__return_false');

// Add thumbnail support
add_theme_support( 'post-thumbnails' );







/**
 * Hooks
 */
require get_parent_theme_file_path( '/assets/lib/hooks.php' );



/**
 * Themes
 */
require get_parent_theme_file_path( '/assets/lib/theme.php' );



/**
 * User Fields
 */
require get_parent_theme_file_path( '/assets/lib/user-fields.php' );



/**
 * Post Types
 */
require get_parent_theme_file_path( '/assets/lib/post-types.php' );



/**
 * Register Custom Taxonomy
 */
require get_parent_theme_file_path( '/assets/lib/gp-register-ctax.php' );




/**
 * Register Custom Tag
 */
require get_parent_theme_file_path( '/assets/lib/gp-register-ctag.php' );




/**
 * GP Functions
 */
require get_parent_theme_file_path( '/assets/lib/gp-functions.php' );





/**
 * Settings -> General Fields
 */
require get_parent_theme_file_path( '/assets/lib/gp-settings-fields.php' );


/**
 * Advanced Custom Fields
 */
require get_parent_theme_file_path( '/assets/lib/acf.php' );
require get_parent_theme_file_path( '/assets/lib/acf-contest-page.php' );


/*********************************************
 * Classes
 ********************************************/



/**
 * Page Restriction
 */
require get_parent_theme_file_path( '/assets/lib/classes/page-restriction.php' );



/**
 * Ajax
 */
require get_parent_theme_file_path( '/assets/lib/classes/gp-ajax.php' );



/**
 * Data Operation
 */
require get_parent_theme_file_path( '/assets/lib/classes/data-operation.php' );




/**
 * Email Templates
 */
require get_parent_theme_file_path( '/assets/lib/classes/email-templates.php' );







function search_filter($query) {
    if ( !is_admin() && $query->is_main_query() ) {
        if ($query->is_search) {
            $query->set('post_type', array( 'user_profile_gallery' ) );

//            $meta_query_args = array(
//                array(
//                    'key' => 'your_key',
//                    'value' => $query->query_vars['s'] = '',
//                    'compare' => 'LIKE',
//                ),
//            );
//            $query->set('meta_query', $meta_query_args);

        }
    }
}

add_action('pre_get_posts','search_filter');




/********************************************************************
 * Default
 ********************************************************************/


/**
 * SVG Icons class.
 */
require get_template_directory() . '/classes/class-twentynineteen-svg-icons.php';

/**
 * Custom Comment Walker template.
 */
require get_template_directory() . '/classes/class-twentynineteen-walker-comment.php';

/**
 * Enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * SVG Icons related functions.
 */
require get_template_directory() . '/inc/icon-functions.php';

/**
 * Custom template tags for the theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


?>
