module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                mangle: true,
                compress: {
                    sequences: false
                }
            },
            build: {
                src: [
                    'assets/js/lib/jquery-3.2.1.min.js',
                    'assets/js/lib/*.js',
                    'assets/js/src/*.js',
                ],
                dest: 'assets/js/main.min.js'
            }
        },
        concat: {
            basic: {
                src: [
                    'assets/css/src/mixins.less',
                    'assets/css/src/reset.less',
                    'assets/css/src/general.less',
                    'assets/css/lib/*.css',
                    'assets/css/src/*.less'
                ],
                dest: 'assets/css/staging.less'
            }
        },
        less: {
            options: {
                compress: true,
                banner: '@charset "UTF-8";'
            },
            build: {
                src: 'assets/css/staging.less',
                dest: 'assets/css/style.min.css'
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');

    // Default task(s).
    grunt.registerTask('default', ['uglify', 'concat', 'less']);
    grunt.registerTask('buildJS', ['uglify']);
    grunt.registerTask('buildCSS', ['concat', 'less']);
};