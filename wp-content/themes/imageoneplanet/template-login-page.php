<?php
/**
 * Template Name: Login Page
 * Template Post Type: Page
 *
 */


get_header();
the_post();
global $obj;

?>

    <div class="main-container" id="submit-entry">

        <section class="page-content">
            <h1><?php the_title(); ?></h1>



            <div class="message-alert">
                <?php  echo $obj->message_display_get_method("success_create_account"); ?>

                <?php  echo $obj->message_display_get_method("login_failed"); ?>
            </div>

            <?php  get_template_part( 'template-parts/login/content', 'login' ); ?>


        </section>
    </div>  <!-- /main-container -->

<?php get_footer(); ?>