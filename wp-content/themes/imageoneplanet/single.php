<?php
/**
 * DEFAULT PAGE TEMPLATE
 *
 **/
get_header();
the_post(); ?>

    <section class="page-content">
        <h1><?php the_title(); ?></h1>
        <div>
            <?php the_content(); ?>



            <?php  $url = get_permalink(); ?>
            <div class="social-share">
                <div class="text">Share : </div>
                <div class="share"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/facebook.svg" alt="Share on Facebook"></a></div>
                <div class="share"><a href="https://twitter.com/home?status=<?php echo $url; ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/twitter.svg" alt="Share on Twitter"></a></div>
                <div class="share"><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>&title=&summary=&source="><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/linkedin.svg" alt="Share on LinkedIn"></a></div>
                <div class="share"><a href="https://plus.google.com/share?url=<?php echo $url; ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/google.svg" alt="Share on Google"></a></div>
                <div class="share"><a href="https://pinterest.com/pin/create/button/?url=<?php echo $url; ?>&media=&description="><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/pin.svg" alt="Share on Pinterest"></a></div>
                <div class="share"><a href="mailto:?subject=<?php echo $url; ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/email.svg" alt="Share by Email"></a></div>
            </div>

        </div>
    </section>

<?php get_footer(); ?>