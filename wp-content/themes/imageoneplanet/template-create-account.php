<?php
/**
 * Template Name: Create Account
 * Template Post Type: Page
 *
 */


get_header();
the_post();
global $obj;

?>

    <div class="main-container" id="submit-entry">

        <section class="page-content">
            <h1><?php the_title(); ?></h1>


                <div class="message-alert"></div>

                <?php  if(!is_user_logged_in()) { ?>
                <div class="upload-art-container">
                    <form method='post' action='' id='create-account-form' class="create-account-form">

                        <input type="hidden" id="security-create-account" value="<?php echo wp_create_nonce('security-create-account-nonce'); ?>">

                        <div class="input-wrapper">
                            <label for="username">Username*</label>
                            <input type="text" id="username" name="username"  class="required" >
                        </div> <!-- /input-wrapper -->

                        <div class="input-wrapper">
                            <label for="email">Email Address*</label>
                            <input type="text" id="email" name="email"  class="required chk-email" >
                        </div> <!-- /input-wrapper -->


                        <div class="input-wrapper">
                            <label for="email">Password*</label>
                            <input type="password" id="user_password" name="user_password"  class="required"  value="">
                        </div> <!-- /input-wrapper -->

                        <div class="input-wrapper">
                            <label for="email">Confirm Password*</label>
                            <input type="password" id="confirm_password" name="confirm_password"  class="required"  value="">
                        </div> <!-- /input-wrapper -->

                        <div class="input-wrapper">

                            <div class="radio-wrap">
                                <div class="radio-field">
                                    <input type="checkbox" name="agree-condition" id="agree-condition" value="1">
                                </div>
                                <span class="label">I agree the Arts/Images uploaded under My Profile will be related to show case our beautiful home Earth,
                                preserve wildlife and environment and raise awareness of protecting nature.
                                Image One Planet reserves the right to remove any images that are not appropriate on our website.</span>
                            </div>
                        </div> <!-- /input-wrapper -->


                        <div class="button-wrapper">
                            <input type="submit" value="Create an Account" class="btn-create-account"  />
                        </div> <!-- /button-wrapper -->

                    </form>
                </div>
                <?php

                    }
                    else {
                        echo  "<p>".$obj->get_site_messages("logout_to_create_account")."</p>";
                    }

                ?>


        </section>
    </div>  <!-- /main-container -->

<?php get_footer(); ?>