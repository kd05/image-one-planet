<?php
/**
 * SINGLE User Art Template
 *
 **/

$allowed_page_access = $loginCheck->art_page_access_to_correct_user();
if(!$allowed_page_access) { $loginCheck->page_restriction_redirect(); }

get_header();
the_post();

$art_id = get_the_ID();
$art_image = get_the_post_thumbnail_url( $art_id,"medium" );
$year_taken = get_post_meta($art_id,"image-year-taken",true);

$active_contest_id = $obj->get_active_contest_id();
$allowed_submission = false;
if($active_contest_id && $obj->get_active_contest_mode($active_contest_id) == "submission_mode"){
    $allowed_submission = true;
}



?>


    <div class="main-container" id="submit-entry">
        <section class="page-content">
            <?php get_template_part( 'template-parts/user-sidebar-menu/user', 'sidebar' ); ?>
            <h1><?php the_title(); ?></h1>

            <div>

                <div class="message-instruction" >
                   <p>
                    <?php $does_user_have_credit = $obj->is_user_allowed_for_submission();
                    echo $does_user_have_credit ? sprintf($obj->get_site_messages("credit_left"),$obj->get_total_credit_left()) : $obj->get_site_messages("no_credit"); ?>
                   </p>
                </div>

                <div class="message-alert">
                    <?php  if(!$allowed_submission){ echo "<p>".$obj->get_site_messages("contest_not_submission")."</p>"; } ?>
                </div>


                <div class="submit-to-contest-wrapper">
                    <div class="art-image">
                        <img src="<?php  echo $art_image; ?>" alt="">
                    </div>

                    <div class="art-content">
                        <?php echo the_content(); ?>

                        <p><strong>Year Taken: </strong> <?php echo $year_taken; ?></p>
                    </div>

                    <div>
                        <?php if($allowed_submission) { ?>
                            <form id="contest-submission-form" >
                                <input type="hidden" class="art_id" value="<?php echo $art_id; ?>" >
                                <input type="hidden" id="security-contest-submission" value="<?php echo wp_create_nonce('security-contest-submission-nonce'); ?>">
                                <input type="submit" <?php if(!$does_user_have_credit) echo "disabled"; ?> value="Submit To Contest" class="submit-to-contest blue button">
                            </form>
                        <?php } ?>
                    </div>
                </div>


            </div>

        </section>
    </div>  <!-- /main-container -->


<?php get_footer(); ?>