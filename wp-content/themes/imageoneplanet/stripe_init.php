<?php


// This snippet (and some of the curl code) due to the Facebook SDK.
if (!function_exists('curl_init')) {
    throw new Exception('Stripe needs the CURL PHP extension.');
}
if (!function_exists('json_decode')) {
    throw new Exception('Stripe needs the JSON PHP extension.');
}
if (!function_exists('mb_detect_encoding')) {
    throw new Exception('Stripe needs the Multibyte String PHP extension.');
}


// Stripe singleton
require(dirname(__FILE__) . '/stripe-lib/Stripe.php');

// Utilities
require(dirname(__FILE__) . '/stripe-lib/Util/AutoPagingIterator.php');
require(dirname(__FILE__) . '/stripe-lib/Util/CaseInsensitiveArray.php');
require(dirname(__FILE__) . '/stripe-lib/Util/LoggerInterface.php');
require(dirname(__FILE__) . '/stripe-lib/Util/DefaultLogger.php');
require(dirname(__FILE__) . '/stripe-lib/Util/RandomGenerator.php');
require(dirname(__FILE__) . '/stripe-lib/Util/RequestOptions.php');
require(dirname(__FILE__) . '/stripe-lib/Util/Set.php');
require(dirname(__FILE__) . '/stripe-lib/Util/Util.php');

// HttpClient
require(dirname(__FILE__) . '/stripe-lib/HttpClient/ClientInterface.php');
require(dirname(__FILE__) . '/stripe-lib/HttpClient/CurlClient.php');

// Errors
require(dirname(__FILE__) . '/stripe-lib/Error/Base.php');
require(dirname(__FILE__) . '/stripe-lib/Error/Api.php');
require(dirname(__FILE__) . '/stripe-lib/Error/ApiConnection.php');
require(dirname(__FILE__) . '/stripe-lib/Error/Authentication.php');
require(dirname(__FILE__) . '/stripe-lib/Error/Card.php');
require(dirname(__FILE__) . '/stripe-lib/Error/Idempotency.php');
require(dirname(__FILE__) . '/stripe-lib/Error/InvalidRequest.php');
require(dirname(__FILE__) . '/stripe-lib/Error/Permission.php');
require(dirname(__FILE__) . '/stripe-lib/Error/RateLimit.php');
require(dirname(__FILE__) . '/stripe-lib/Error/SignatureVerification.php');

// OAuth errors
require(dirname(__FILE__) . '/stripe-lib/Error/OAuth/OAuthBase.php');
require(dirname(__FILE__) . '/stripe-lib/Error/OAuth/InvalidClient.php');
require(dirname(__FILE__) . '/stripe-lib/Error/OAuth/InvalidGrant.php');
require(dirname(__FILE__) . '/stripe-lib/Error/OAuth/InvalidRequest.php');
require(dirname(__FILE__) . '/stripe-lib/Error/OAuth/InvalidScope.php');
require(dirname(__FILE__) . '/stripe-lib/Error/OAuth/UnsupportedGrantType.php');
require(dirname(__FILE__) . '/stripe-lib/Error/OAuth/UnsupportedResponseType.php');

// API operations
require(dirname(__FILE__) . '/stripe-lib/ApiOperations/All.php');
require(dirname(__FILE__) . '/stripe-lib/ApiOperations/Create.php');
require(dirname(__FILE__) . '/stripe-lib/ApiOperations/Delete.php');
require(dirname(__FILE__) . '/stripe-lib/ApiOperations/NestedResource.php');
require(dirname(__FILE__) . '/stripe-lib/ApiOperations/Request.php');
require(dirname(__FILE__) . '/stripe-lib/ApiOperations/Retrieve.php');
require(dirname(__FILE__) . '/stripe-lib/ApiOperations/Update.php');

// Plumbing
require(dirname(__FILE__) . '/stripe-lib/ApiResponse.php');
require(dirname(__FILE__) . '/stripe-lib/RequestTelemetry.php');
require(dirname(__FILE__) . '/stripe-lib/StripeObject.php');
require(dirname(__FILE__) . '/stripe-lib/ApiRequestor.php');
require(dirname(__FILE__) . '/stripe-lib/ApiResource.php');
require(dirname(__FILE__) . '/stripe-lib/SingletonApiResource.php');

// Stripe API Resources
require(dirname(__FILE__) . '/stripe-lib/Account.php');
require(dirname(__FILE__) . '/stripe-lib/AccountLink.php');
require(dirname(__FILE__) . '/stripe-lib/AlipayAccount.php');
require(dirname(__FILE__) . '/stripe-lib/ApplePayDomain.php');
require(dirname(__FILE__) . '/stripe-lib/ApplicationFee.php');
require(dirname(__FILE__) . '/stripe-lib/ApplicationFeeRefund.php');
require(dirname(__FILE__) . '/stripe-lib/Balance.php');
require(dirname(__FILE__) . '/stripe-lib/BalanceTransaction.php');
require(dirname(__FILE__) . '/stripe-lib/BankAccount.php');
require(dirname(__FILE__) . '/stripe-lib/BitcoinReceiver.php');
require(dirname(__FILE__) . '/stripe-lib/BitcoinTransaction.php');
require(dirname(__FILE__) . '/stripe-lib/Card.php');
require(dirname(__FILE__) . '/stripe-lib/Charge.php');
require(dirname(__FILE__) . '/stripe-lib/Checkout/Session.php');
require(dirname(__FILE__) . '/stripe-lib/Collection.php');
require(dirname(__FILE__) . '/stripe-lib/CountrySpec.php');
require(dirname(__FILE__) . '/stripe-lib/Coupon.php');
require(dirname(__FILE__) . '/stripe-lib/Customer.php');
require(dirname(__FILE__) . '/stripe-lib/Discount.php');
require(dirname(__FILE__) . '/stripe-lib/Dispute.php');
require(dirname(__FILE__) . '/stripe-lib/EphemeralKey.php');
require(dirname(__FILE__) . '/stripe-lib/Event.php');
require(dirname(__FILE__) . '/stripe-lib/ExchangeRate.php');
require(dirname(__FILE__) . '/stripe-lib/File.php');
require(dirname(__FILE__) . '/stripe-lib/FileLink.php');
require(dirname(__FILE__) . '/stripe-lib/FileUpload.php');
require(dirname(__FILE__) . '/stripe-lib/Invoice.php');
require(dirname(__FILE__) . '/stripe-lib/InvoiceItem.php');
require(dirname(__FILE__) . '/stripe-lib/InvoiceLineItem.php');
require(dirname(__FILE__) . '/stripe-lib/IssuerFraudRecord.php');
require(dirname(__FILE__) . '/stripe-lib/Issuing/Authorization.php');
require(dirname(__FILE__) . '/stripe-lib/Issuing/Card.php');
require(dirname(__FILE__) . '/stripe-lib/Issuing/CardDetails.php');
require(dirname(__FILE__) . '/stripe-lib/Issuing/Cardholder.php');
require(dirname(__FILE__) . '/stripe-lib/Issuing/Dispute.php');
require(dirname(__FILE__) . '/stripe-lib/Issuing/Transaction.php');
require(dirname(__FILE__) . '/stripe-lib/LoginLink.php');
require(dirname(__FILE__) . '/stripe-lib/Order.php');
require(dirname(__FILE__) . '/stripe-lib/OrderItem.php');
require(dirname(__FILE__) . '/stripe-lib/OrderReturn.php');
require(dirname(__FILE__) . '/stripe-lib/PaymentIntent.php');
require(dirname(__FILE__) . '/stripe-lib/Payout.php');
require(dirname(__FILE__) . '/stripe-lib/Person.php');
require(dirname(__FILE__) . '/stripe-lib/Plan.php');
require(dirname(__FILE__) . '/stripe-lib/Product.php');
require(dirname(__FILE__) . '/stripe-lib/Radar/ValueList.php');
require(dirname(__FILE__) . '/stripe-lib/Radar/ValueListItem.php');
require(dirname(__FILE__) . '/stripe-lib/Recipient.php');
require(dirname(__FILE__) . '/stripe-lib/RecipientTransfer.php');
require(dirname(__FILE__) . '/stripe-lib/Refund.php');
require(dirname(__FILE__) . '/stripe-lib/Reporting/ReportRun.php');
require(dirname(__FILE__) . '/stripe-lib/Reporting/ReportType.php');
require(dirname(__FILE__) . '/stripe-lib/Review.php');
require(dirname(__FILE__) . '/stripe-lib/SKU.php');
require(dirname(__FILE__) . '/stripe-lib/Sigma/ScheduledQueryRun.php');
require(dirname(__FILE__) . '/stripe-lib/Source.php');
require(dirname(__FILE__) . '/stripe-lib/SourceTransaction.php');
require(dirname(__FILE__) . '/stripe-lib/Subscription.php');
require(dirname(__FILE__) . '/stripe-lib/SubscriptionItem.php');
require(dirname(__FILE__) . '/stripe-lib/Terminal/ConnectionToken.php');
require(dirname(__FILE__) . '/stripe-lib/Terminal/Location.php');
require(dirname(__FILE__) . '/stripe-lib/Terminal/Reader.php');
require(dirname(__FILE__) . '/stripe-lib/ThreeDSecure.php');
require(dirname(__FILE__) . '/stripe-lib/Token.php');
require(dirname(__FILE__) . '/stripe-lib/Topup.php');
require(dirname(__FILE__) . '/stripe-lib/Transfer.php');
require(dirname(__FILE__) . '/stripe-lib/TransferReversal.php');
require(dirname(__FILE__) . '/stripe-lib/UsageRecord.php');
require(dirname(__FILE__) . '/stripe-lib/UsageRecordSummary.php');

// OAuth
require(dirname(__FILE__) . '/stripe-lib/OAuth.php');

// Webhooks
require(dirname(__FILE__) . '/stripe-lib/Webhook.php');
require(dirname(__FILE__) . '/stripe-lib/WebhookEndpoint.php');
require(dirname(__FILE__) . '/stripe-lib/WebhookSignature.php');
