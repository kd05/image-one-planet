<?php
/**
 * SEARCH TEMPLATE
 *
 **/
get_header();





?>







    <div class="main-container dark-background" id="user-listings-page">
        <section class="page-content">

            <h1>Art Images</h1>

            <div class="card-wrapper">
                <form class="search-user-form form" id="search-user-form" action="<?php echo esc_url( home_url( '/' ) ) ?>" >
                    <div class="flex-wrapper">
                        <div class="input-wrapper ">
                            <label for="image-year-taken"> Search</label>
                            <input type="text" name="s" value="<?php echo get_search_query() ?>">
                        </div> <!-- /input-wrapper -->

                        <div class="button-wrapper">
                            <input type="submit" value="Search" tabindex="5" id="search_user" name="search_user" class="thread-button button blue" />
                        </div> <!-- /button-wrapper -->
                    </div>
                </form>
            </div> <!-- /card-wrapper -->



            <div class="card-wrapper user-listing-container">

                    <?php if ( have_posts() ) : ?>

                    <div class="wookmark-container">

                        <?php
                        // Start the Loop.
                        while ( have_posts() ) :
                            the_post();

                            /*
                             * Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part( 'template-parts/content/content', 'search' );
//                            get_template_part( 'template-parts/post/content', 'search' );

                            // End the loop.
                        endwhile;

                        ?>

                    </div><!-- /wookmark-container -->
                        <?php

                        // Previous/next page navigation.
                        the_posts_pagination( array(
                            'screen_reader_text' => ' ',
                            'type' => 'list',
                            'mid_size'  => 2,
                            'prev_text' => __( ' <i class="fas fa-caret-left"></i> ', 'imageoneplanet' ),
                            'next_text' => __( ' <i class="fas fa-caret-right"></i> ', 'imageoneplanet' ),
                        ) );

                    // If no content, include the "No posts found" template.
                    else :
                        get_template_part( 'template-parts/content/content', 'none' );

                    endif;
                    ?>



            </div> <!-- /card-wrapper -->
        </section>
    </div>
    <!-- /main-container -->

<?php get_footer(); ?>
