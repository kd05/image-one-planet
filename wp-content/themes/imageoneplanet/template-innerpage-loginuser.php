<?php
/**
 * Template Name: Inner Page With Menus (Logged In User)
 * Template Post Type: Page
 *
 */
get_header();
the_post(); ?>

    <section class="page-content">

        <?php if(is_user_logged_in()) { get_template_part( 'template-parts/user-sidebar-menu/user', 'sidebar' ); }  ?>

        <h1><?php the_title(); ?></h1>
        <div>
            <?php the_content(); ?>

        </div>
    </section>

<?php get_footer(); ?>