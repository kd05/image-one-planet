<?php
/**
 * Template Name: Edit Art Page
 * Template Post Type: Page
 *
 */


$page_allowed = ["participant","jury","administrator"];
$loginCheck->page_access_to_roles($page_allowed);


get_header();
the_post();



?>

    <div class="main-container" id="submit-entry">
        <section class="page-content">
            <?php get_template_part( 'template-parts/user-sidebar-menu/user', 'sidebar' ); ?>
            <h1><?php the_title(); ?></h1>
            <div>
                <?php // the_content(); ?>

        <?php  if(isset($_GET['art_id']) && $_GET['art_id'] != "") {

            $art_id = base64_decode($_GET['art_id']);
            $allowed_page_access = $loginCheck->art_page_access_to_correct_user($art_id);
            if ($allowed_page_access) {
                $art_img = current(wp_get_attachment_image_src( get_post_thumbnail_id($art_id), 'medium' ));
                $title = get_the_title($art_id);
                $description = $obj->get_the_content_by_id($art_id);
                $year = get_post_meta($art_id,"image-year-taken", true);
                ?>
                <div class="upload-art-container">
                    <form class="edit-user-art-form form" id="edit-user-art-form" action="" enctype="multipart/form-data">
                        <div class="message-alert"></div>

                        <input type="hidden" id="security-user-art" value="<?php echo wp_create_nonce('security-user-art-nonce'); ?>">
                        <input type="hidden" name="action" id="action" value="edit_user_art"/>
                        <input type="hidden" name="edit_art_id" id="edit_art_id" value="<?php echo $art_id; ?>"/>

                        <!-- images -->
                        <div class="input-wrapper input-image-wrapper">
                            <label for="images">Image</label>

                            <img src="<?php echo $art_img; ?>" alt="">
                        </div> <!-- /input-wrapper -->

                        <!-- Submit button-->
                        <div class="input-wrapper">
                            <label for="image-title">Title of entry</label>
                            <input type="text" name="image-title" id="image-title" class="required" value="<?php echo $title; ?>"
                                   placeholder="Enter a title">
                        </div> <!-- /input-wrapper -->

                        <div class="input-wrapper">
                            <label for="image-description">Description of entry</label>
                            <textarea name="image-description" id="image-description" class="required"
                                      placeholder="Enter a description"><?php echo $description; ?></textarea>
                        </div> <!-- /input-wrapper -->

                        <div class="input-wrapper select-wrapper">
                            <label for="image-year-taken">Year taken</label>
                            <select name="image-year-taken" id="image-year-taken"  class="required" >
                                <option selected disabled>Select a year</option>
                                <?php
                                for ($x = 1920; $x <= date("Y"); $x++) {
                                    $selected = ($x == $year) ? "selected" : "";
                                    echo '<option value="' . $x . '"  ' . $selected . ' >' . $x . '</option>';
                                }
                                ?>
                            </select>


                        </div> <!-- /input-wrapper -->

                        <div class="button-wrapper">
                            <input type="submit" value="Update Art" tabindex="5" id="user_art_edit"
                                   name="user_art_edit" class="thread-button button blue"/>
                        </div> <!-- /button-wrapper -->
                    </form>
                </div>
            <?php }
                    
                    else{  ?>    <div class="message-alert"><p><?php echo $obj->get_site_messages("art_access_denied"); ?></p></div>  <?php  }

                }
            else {   ?>

                <div class="message-alert"><p><?php echo $obj->get_site_messages("art_edit_error"); ?></p></div>

            <?php } ?>
            </div>
        </section>
    </div>  <!-- /main-container -->

<?php get_footer(); ?>