<?php
/**
 * Template Name: View Submissions - For Jury
 * Template Post Type: Page
 */


$page_allowed = ["participant","jury","administrator"];
$loginCheck->page_access_to_roles($page_allowed);
get_header();

$contest_id = $obj->get_active_contest_id();
$contest_detail = get_term_by("id",$contest_id,"contest_category");
$contest_title  = $contest_detail->name;

$submissions = $obj->get_contest_submissions($contest_id);
$total_submissions = count($submissions);


?>

    <div class="main-container" id="submission-jury-page">

        <section class="page-content">

            <?php get_template_part( 'template-parts/user-sidebar-menu/user', 'sidebar' ); ?>

            <h1>Submissions - <?php echo $contest_title; ?></h1>

            <div class="thumb-gallery-wrapper">

                <?php

                if($total_submissions > 0) {
                    foreach ($submissions as $submission_id) {
                        $submission_title = get_the_title($submission_id);
                        $med_img = get_the_post_thumbnail_url($submission_id, "medium");
                        $full_img = get_the_post_thumbnail_url($submission_id, "custom-size-2000");
                        $user_id = get_post_meta($submission_id,"art_user_id",true);
                        $first_name = get_user_meta($user_id, 'first_name', true);
                        $last_name = get_user_meta($user_id, 'last_name', true);
                        $full_name = $first_name." ".$last_name;
                        $submission_social_link = get_post_meta( $submission_id, 'submission_social_link', true );
                    ?>
                        <div class="thumb-wrapper">
                            <a href="<?php echo $full_img; ?>" class="magnific-popup" title="<?php echo $submission_title; ?>" author-data="<?php echo $full_name; ?>" id-data="<?php echo $submission_id; ?>">
                                <div class="thumb-image" style="background-image: url('<?php echo $med_img; ?>')">
                                    <div class="open-lightbox-nopopup"
                                         data-full-img="<?php echo $full_img; ?>"
                                         data-img-title="<?php echo $submission_title; ?>"
                                         data-instagram-view ="<?php echo $submission_social_link ? $submission_social_link : '#'; ?>"
                                         data-artist-name="<?php echo $full_name; ?>">
                                        <i class="fas fa-search"></i>
                                        <span>View</span>
                                    </div>
                                </div>
                            </a>

                            <div class="thumb-details">
                                <p class="artist"><?php echo $full_name; ?></p>
                                <p class="art-title art-id">ID#: <?php echo $submission_id; ?></p>
                                <p class="art-title"><?php echo $submission_title; ?></p>
                            </div>
                        </div>

                    <?php
                   }
                } else { ?>
                    <div class="message-alert"><p><?php  echo $obj->get_site_messages("no_submission"); ?></p></div>
                <?php } ?>


            </div> <!-- /thumb-gallery-wrapper -->

        </section>

    </div>  <!-- /main-container -->

<?php
get_template_part('template-parts/gallery/gallery', 'lightbox');
get_footer();
?>