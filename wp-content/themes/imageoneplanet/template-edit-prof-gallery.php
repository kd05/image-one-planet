<?php
/**
 * Template Name: Edit Profile Gallery Image
 * Template Post Type: Page
 *
 */

$page_allowed = ["participant","jury","administrator"];
$loginCheck->page_access_to_roles($page_allowed);

$status_array = array("For Sale","Not for sale","Sold");
get_header();
the_post();
?>

    <div class="main-container" id="submit-entry">
        <section class="page-content">
            <?php get_template_part( 'template-parts/user-sidebar-menu/user', 'sidebar' ); ?>

        <?php  if(isset($_GET['gallery_id']) && $_GET['gallery_id'] != "") {
            $gallery_id = base64_decode($_GET['gallery_id']);
            $allowed_page_access = $loginCheck->gallery_page_access_to_correct_user($gallery_id);
            if ($allowed_page_access) {
                $gallery_img = current(wp_get_attachment_image_src( get_post_thumbnail_id($gallery_id), 'medium' ));
                $title = get_the_title($gallery_id);
                $description = $obj->get_the_content_by_id($gallery_id);
                $type = get_post_meta($gallery_id,"image-type", true);
                $size = get_post_meta($gallery_id,"image-size", true);
                $frame = get_post_meta($gallery_id,"image-frame", true);
                $listing_image_type = get_post_meta( $gallery_id, 'listing-image-type', true );
                $year = get_post_meta($gallery_id,"image-year", true);

                $img_status = get_post_meta($gallery_id,"image-status", true);
                $price = get_post_meta($gallery_id,"image-price", true);
                $image_tag = get_post_meta($gallery_id,"image-tag", true);
                $image_tag = get_the_excerpt($gallery_id);
            ?>
                <h1>Edit - <?php echo $title; ?></h1>
                <div class="upload-prof-gallery-container">
                        <form class="edit-prof-gallery-form form" id="edit-prof-gallery-form" action="" >
                            <div class="message-alert"></div>

                            <input type="hidden" id="security" value="<?php echo wp_create_nonce('security-prof-gallery-nonce'); ?>">
                            <input type="hidden" name="action" id="action" value="edit_prof_gallery" />
                            <input type="hidden" name="edit_gallery_id" id="edit_art_id" value="<?php echo $gallery_id; ?>"/>


                            <!-- images -->
                            <div class="input-wrapper input-image-wrapper">
                                <label for="images">Image</label>

                                <img src="<?php echo $gallery_img; ?>" alt="">
                            </div> <!-- /input-wrapper -->

                            <!-- Submit button-->
                            <div class="input-wrapper">
                                <label for="image-title">Title*</label>
                                <input type="text" name="image-title" id="image-title" class="required" value="<?php echo $title; ?>" placeholder="Enter a title">
                            </div> <!-- /input-wrapper -->

                            <div class="input-wrapper">
                                <label for="image-description">Description*</label>
                                <textarea  name="image-description" id="image-description"  class="required" placeholder="Enter a description"><?php echo $description; ?></textarea>
                            </div> <!-- /input-wrapper -->


                            <div class="input-wrapper">
                                <label for="image-price">Size</label>
                                <input type="text" name="image-size" id="image-size" value="<?php echo $size; ?>" placeholder="Size">
                                <div class="radio-flex">
                                    <div class="radio-field">
                                        <input type="radio" name="image-frame" id="image-frame" value="Frame Included" <?php  if($frame == "Frame Included") echo "checked"; ?>>
                                        <span>Frame Included</span>
                                    </div>
                                    <div class="radio-field">
                                        <input type="radio" name="image-frame" id="image-frame" value="Frame Excluded" <?php  if($frame == "Frame Excluded") echo "checked"; ?>>
                                        <span>Frame Excluded</span>
                                    </div>
                                </div>
                            </div> <!-- /input-wrapper -->


                            <div class="input-wrapper">
                                <label for="image-type">Select as a Profile Image (Only <?php echo GP_MAX_USER_PROFILE_FEATURED_IMAGES; ?> can be selected as a Profile Images)</label>
                                <p class="tooltip-info">(If you are a featured user, all 50 images will appear in the profile page)</p>
                                <div class="radio-wrap">
                                    <div class="radio-field">
                                        <input type="checkbox" name="image-type" id="image-type" value="featured_image" class="image-type" <?php  if($type == "featured_image") echo "checked"; ?>>
                                    </div>
                                </div>

                            </div> <!-- /input-wrapper -->


                            <div class="input-wrapper">
                                <label for="listing-image-type">Select as a Listing Image (Only <?php echo GP_MAX_USER_PROFILE_LISTING_IMAGES; ?> can be selected as a Listing Image.)</label>
                                <div class="radio-wrap">
                                    <div class="radio-field">
                                        <input type="checkbox" name="listing-image-type" id="listing-image-type" value="listing_image" class="listing-image-type" <?php  if($listing_image_type == "listing_image") echo "checked"; ?>>
                                    </div>
                                </div>
                            </div> <!-- /input-wrapper -->


                            <div class="input-wrapper select-wrapper">
                                <label for="image-year-taken">Year*</label>
                                <select name="image-year-taken" id="image-year-taken" class="required">
                                    <option value="">Select a year</option>
                                    <?php
                                    for ($x = 1920; $x <= date("Y"); $x++) {
                                        $selected = ($x == $year) ? "selected" : "";
                                        echo '<option value="' . $x . '"  ' . $selected . '>' . $x . '</option>';
                                    }
                                    ?>
                                </select>
                            </div> <!-- /input-wrapper -->

                            <div class="input-wrapper select-wrapper">
                                <label for="image-status">Status*</label>
                                <select name="image-status" id="image-status" class="required">
                                    <?php foreach($status_array as $status){

                                        ?>
                                        <option value="<?php echo $status ?>" <?php if($img_status == $status) echo "selected"; ?> ><?php echo $status ?></option>
                                    <?php } ?>
                                </select>
                            </div> <!-- /input-wrapper -->

                            <div class="input-wrapper">
                                <label for="image-price">Price/Value</label>
                                <input type="text" name="image-price" id="image-price" value="<?php echo $price; ?>" placeholder="Price/Value">
                            </div> <!-- /input-wrapper -->


                            <div class="input-wrapper">
                                <label for="image-price">Tags (Comma separated)</label>
                                <input type="text" name="image-tag" id="image-tag" value="<?php echo $image_tag; ?>" placeholder="Tags">
                            </div> <!-- /input-wrapper -->


                            <div class="button-wrapper">
                                <input type="submit" value="Update" tabindex="5" id="edit_prof_gallery" name="edit_prof_gallery" class="thread-button button blue" />
                            </div> <!-- /button-wrapper -->
                        </form>
                    </div>

                <div class="instruction-page-content">
                    <?php  // the_content(); ?>
                </div>

            <?php }
                    else{  ?>
                        <div class="message-alert">
                            <p><?php echo $obj->get_site_messages("prof_gallery_access_denied"); ?></p>
                        </div>  <?php
                    }

            } else {   ?>
            <div class="message-alert"><p><?php echo $obj->get_site_messages("prof_gallery_edit_error"); ?></p></div>
            <?php } ?>


        </section>
    </div>  <!-- /main-container -->

<?php get_footer(); ?>