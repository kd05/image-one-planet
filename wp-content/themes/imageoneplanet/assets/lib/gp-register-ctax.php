<?php
if (!defined('ABSPATH')) {
    exit;
}




add_action( 'init', 'create_taxonomies');
function create_taxonomies() {

    $labels = array(
        'name' => _x( 'Contests', 'taxonomy general name' ),
        'singular_name' => _x( 'Contest', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Contests' ),
        'all_items' => __( 'All Contests' ),
        'parent_item' => __( 'Parent Contest Category' ),
        'parent_item_colon' => __( 'Parent Contest Category:' ),
        'edit_item' => __( 'Edit Contest' ),
        'update_item' => __( 'Update Contest' ),
        'add_new_item' => __( 'Add New Contest' ),
        'new_item_name' => __( 'New Contest Name' ),
        'menu_name' => __( 'Contest' ),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'contest_category', 'hierarchical' => true, 'with_front' => false),
    );

    // Now register the taxonomy
    register_taxonomy('contest_category',array('contest_submission'), $args);






    $labels = array(
        'name' => _x( 'Payment Type', 'taxonomy general name' ),
        'singular_name' => _x( 'Payment Type', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Payment Types' ),
        'all_items' => __( 'All Payment Types' ),
        'edit_item' => __( 'Edit Payment Type' ),
        'update_item' => __( 'Update Payment Type' ),
        'add_new_item' => __( 'Add New Payment Type' ),
        'new_item_name' => __( 'New Payment Type Name' ),
        'menu_name' => __( 'Payment Type' ),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'payment_type', 'hierarchical' => true, 'with_front' => false),
    );

    // Now register the taxonomy
    register_taxonomy('payment_type',array('user_payment'), $args);










    $labels = array(
        'name' => _x( 'Artist Category', 'taxonomy general name' ),
        'singular_name' => _x( 'Artist Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Artist Categories' ),
        'all_items' => __( 'All Artist Categories' ),
        'edit_item' => __( 'Edit Artist Category' ),
        'update_item' => __( 'Update Artist Category' ),
        'add_new_item' => __( 'Add New Artist Category' ),
        'new_item_name' => __( 'New Artist Category Name' ),
        'menu_name' => __( 'Artist Categories' ),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'artist_category', 'hierarchical' => true, 'with_front' => false),
    );

    // Now register the taxonomy
    register_taxonomy('artist_category',array('user_profile_gallery'), $args);






    $labels = array(
        'name' => _x( 'Photographer Category', 'taxonomy general name' ),
        'singular_name' => _x( 'Photographer Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Photographer Categories' ),
        'all_items' => __( 'All Photographer Categories' ),
        'edit_item' => __( 'Edit Photographer Category' ),
        'update_item' => __( 'Update Photographer Category' ),
        'add_new_item' => __( 'Add New Photographer Category' ),
        'new_item_name' => __( 'New Photographer Category Name' ),
        'menu_name' => __( 'Photographer Categories' ),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'photographer_category', 'hierarchical' => true, 'with_front' => false),
    );

    // Now register the taxonomy
    register_taxonomy('photographer_category',array('user_profile_gallery'), $args);



}








// ***********************************************************
//                  META BOXES for User Contest Category
// ***********************************************************


// Add the fields to the "contest_category" taxonomy, using our callback function
add_action( 'contest_category_edit_form_fields', 'contest_category_taxonomy_custom_fields', 10, 2 );

function contest_category_taxonomy_custom_fields($term) {

    $term_id = $term->term_id;

    $contest_status = get_term_meta ( $term_id, 'contest_status',true);
    $contest_mode = get_term_meta ( $term_id, 'contest_mode',true);
    $old_contest = get_term_meta ( $term_id, 'old_contest',true);

    $upload_main_image = "";
    $upload_main_image_id = get_term_meta ( $term_id, 'upload_main_image_id',true);
    if($upload_main_image_id != ""){
        $upload_main_image = current(wp_get_attachment_image_src( $upload_main_image_id, 'thumbnail'));
    }
    if($upload_main_image == ""){ $upload_main_image = get_image_not_found(); }

    $contest_deadline = get_term_meta ( $term_id, 'contest_deadline',true );
    $prize_data_gold = get_term_meta ( $term_id, 'prize_data_gold',true );
    $prize_data_silver = get_term_meta ( $term_id, 'prize_data_silver',true );
    $prize_data_bronze = get_term_meta ( $term_id, 'prize_data_bronze',true );

    $sponsor_image = "";
    $sponsor_image_id = get_term_meta ( $term_id, 'sponsor_image_id',true);
    if($sponsor_image_id != ""){
        $sponsor_image = current(wp_get_attachment_image_src( $sponsor_image_id, 'thumbnail'));
    }
    if($sponsor_image == ""){ $sponsor_image = get_image_not_found(); }

    $sponsor_title = get_term_meta ( $term_id, 'sponsor_title',true);
    $sponsor_description = get_term_meta ( $term_id, 'sponsor_description',true);


    $jury_content_title = get_term_meta ( $term_id, 'jury_content_title',true);
    $jury_content = get_term_meta ( $term_id, 'jury_content',true);

    //  #######  Jury Image 1  #######
    $jury_image_1_image = "";
    $jury_image_1_id = get_term_meta ( $term_id, 'jury_image_1_id',true);
    if($jury_image_1_id != ""){
        $jury_image_1_image = current(wp_get_attachment_image_src( $jury_image_1_id, 'thumbnail'));
    }
    if($jury_image_1_image == ""){ $jury_image_1_image = get_image_not_found(); }
    $jury_title_1 = get_term_meta ( $term_id, 'jury_title_1',true);
    $jury_website_1 = get_term_meta ( $term_id, 'jury_website_1',true);

    //  #######  Jury Image 2  #######
    $jury_image_2_image = "";
    $jury_image_2_id = get_term_meta ( $term_id, 'jury_image_2_id',true);
    if($jury_image_2_id != ""){
        $jury_image_2_image = current(wp_get_attachment_image_src( $jury_image_2_id, 'thumbnail'));
    }
    if($jury_image_2_image == ""){ $jury_image_2_image = get_image_not_found(); }
    $jury_title_2 = get_term_meta ( $term_id, 'jury_title_2',true);
    $jury_website_2 = get_term_meta ( $term_id, 'jury_website_2',true);

    //  #######  Jury Image 3  #######
    $jury_image_3_image = "";
    $jury_image_3_id = get_term_meta ( $term_id, 'jury_image_3_id',true);
    if($jury_image_3_id != ""){
        $jury_image_3_image = current(wp_get_attachment_image_src( $jury_image_3_id, 'thumbnail'));
    }
    if($jury_image_3_image == ""){ $jury_image_3_image = get_image_not_found(); }
    $jury_title_3 = get_term_meta ( $term_id, 'jury_title_3',true);
    $jury_website_3 = get_term_meta ( $term_id, 'jury_website_3',true);

    //  #######  Jury Image 4  #######
    $jury_image_4_image = "";
    $jury_image_4_id = get_term_meta ( $term_id, 'jury_image_4_id',true);
    if($jury_image_4_id != ""){
        $jury_image_4_image = current(wp_get_attachment_image_src( $jury_image_4_id, 'thumbnail'));
    }
    if($jury_image_4_image == ""){ $jury_image_4_image = get_image_not_found(); }
    $jury_title_4 = get_term_meta ( $term_id, 'jury_title_4',true);
    $jury_website_4 = get_term_meta ( $term_id, 'jury_website_4',true);

    //  #######  Jury Image 5  #######
    $jury_image_5_image = "";
    $jury_image_5_id = get_term_meta ( $term_id, 'jury_image_5_id',true);
    if($jury_image_5_id != ""){
        $jury_image_5_image = current(wp_get_attachment_image_src( $jury_image_5_id, 'thumbnail'));
    }
    if($jury_image_5_image == ""){ $jury_image_5_image = get_image_not_found(); }
    $jury_title_5 = get_term_meta ( $term_id, 'jury_title_5',true);
    $jury_website_5 = get_term_meta ( $term_id, 'jury_website_5',true);



    ?>


        <tr class="form-field contest-extra-fields" >
            <th scope="row" valign="top" colspan="2">
                <h1>Extra Fields</h1>
            </th>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="contest_status"><?php _e('Contest Status'); ?></label>
            </th>
            <td>
                <input type="hidden" name="previous_contest_status" value="<?php echo $contest_status; ?>">
                <select name="contest_status" id="contest_status">
                    <option value="inactive" <?php if($contest_status == "inactive") { echo "selected"; } ?> >Inactive</option>
                    <option value="active" <?php if($contest_status == "active") { echo "selected"; } ?>>Active</option>
                </select>
                <span class="description"><?php _e('Is Contest Active'); ?></span>
            </td>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="contest_mode"><?php _e('Contest Mode'); ?></label>
            </th>
            <td>
                <input type="hidden" name="previous_contest_mode" value="<?php echo $contest_mode; ?>">
                <select name="contest_mode" id="contest_mode">
                    <option value="submission_mode" <?php if($contest_mode == "submission_mode") { echo "selected"; } ?>>Submission Mode</option>
                    <option value="publish" <?php if($contest_mode == "publish") { echo "selected"; } ?>>Publish</option>
                    <option value="final" <?php if($contest_mode == "final") { echo "selected"; } ?>>Final</option>
                </select>
                <span class="description"><?php _e('Select Contest Mode'); ?></span>

                <br>
                <input type="checkbox" name="notify_email_mode_change" value="yes" checked="checked">
                <span class="description"><?php _e('Select to send email to all Participants/Jury to notify them about the mode change.<br>
                [NOTE: IT WILL TAKE FEW MINUTES WHILE SAVING AS EMAIL TO EACH USERS TAKES SOMETIME.]'); ?></span>

                <br><br><br>
                <input type="checkbox" name="winner_email" value="yes" checked="checked">
                <span class="description"><?php _e('Select to send email to all Winner Participants to notify them about the mode change to FINAL Mode.<br>
                [NOTE: IT WILL TAKE FEW MINUTES WHILE SAVING AS EMAIL TO EACH USERS TAKES SOMETIME.]'); ?></span>
            </td>
        </tr>


        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="old_contest"><?php _e('Older Contest'); ?></label>
            </th>
            <td>
                <select name="old_contest" id="old_contest">
                    <option value="" >- N/A -</option>
                    <option value="previous" <?php if($old_contest == "previous") { echo "selected"; } ?>>previous</option>
                    <option value="older" <?php if($old_contest == "older") { echo "selected"; } ?>>older</option>
                </select>
            </td>
        </tr>


        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="contest_mode"><?php _e('Main Image'); ?></label>
            </th>
            <td>
                <input class="upload_main_image_url" name="upload_main_image_url" type="text" value=""/>
                <input class="upload_main_image_id" name="upload_main_image_id" type="hidden" value="<?php echo $upload_main_image_id; ?>"/>
                <input  class="upload_image_button" type="button" value="Upload Image"
                       data-upload-image-class="upload_main_image_url"
                       data-upload-image-src="upload_main_image"
                       data-upload-image-id="upload_main_image_id"/>
                <br>
                <div class="contest-img"><img src="<?php echo $upload_main_image; ?>" class="upload_main_image" alt=""></div>
            </td>
        </tr>


        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="contest_deadline"><?php _e('Contest Deadline'); ?></label>
            </th>
            <td>
                <input type="text" name="contest_deadline" id="contest_deadline" value="<?php echo $contest_deadline; ?>" />
            </td>
        </tr>


        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="prize_data_gold"><?php _e('Prize Data(Gold)'); ?></label>
            </th>
            <td>
                <input type="text" name="prize_data_gold" id="prize_data_gold" value="<?php echo $prize_data_gold; ?>" />
            </td>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="prize_data_silver"><?php _e('Prize Data(Silver)'); ?></label>
            </th>
            <td>
                <input type="text" name="prize_data_silver" id="prize_data_silver" value="<?php echo $prize_data_silver; ?>" />
            </td>
        </tr>


        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="prize_data_bronze"><?php _e('Prize Data(Bronze)'); ?></label>
            </th>
            <td>
                <input type="text" name="prize_data_bronze" id="prize_data_bronze" value="<?php echo $prize_data_bronze; ?>" />
            </td>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="sponsor_image"><?php _e('Sponsor Image'); ?></label>
            </th>
            <td>
                <input class="sponsor_image_url" name="sponsor_image_url" type="text" value=""/>
                <input class="sponsor_image_id" name="sponsor_image_id" type="hidden" value="<?php echo $sponsor_image_id; ?>"/>
                <input  class="upload_image_button" type="button" value="Upload Image"
                       data-upload-image-class="sponsor_image_url"
                       data-upload-image-src="sponsor_main_image"
                       data-upload-image-id="sponsor_image_id"/>
                <br>
                <div class="contest-img"><img src="<?php echo $sponsor_image; ?>" class="sponsor_main_image" alt=""></div>
            </td>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="sponsor_title"><?php _e('Sponsor Title'); ?></label>
            </th>
            <td>
                <input type="text" name="sponsor_title" id="sponsor_title" value="<?php echo $sponsor_title; ?>" />
            </td>
        </tr>


        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="sponsor_description"><?php _e('Sponsor Description'); ?></label>
            </th>
            <td>
                <textarea name="sponsor_description" id="sponsor_description" cols="30" rows="10"><?php echo $sponsor_description; ?></textarea>
            </td>
        </tr>

        <!--  ############      Jury Detail     ############-->
        <tr class="form-field contest-extra-fields" >
            <th scope="row" valign="top" colspan="2">
                <hr>
                <h1>Jury Detail</h1>
            </th>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="jury_content_title"><?php _e('Jury Content Title'); ?></label>
            </th>
            <td>
                <input type="text" name="jury_content_title" id="jury_content_title" value="<?php echo $jury_content_title; ?>" />
            </td>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="jury_content"><?php _e('Jury Content'); ?></label>
            </th>
            <td>
                <textarea name="jury_content" id="" cols="30" rows="10"><?php echo $jury_content; ?></textarea>
            </td>
        </tr>

        <!--  ############      Jury 1     ############-->
        <tr class="form-field contest-extra-fields" >
            <th scope="row" valign="top" colspan="2">
                <h1>Jury 1</h1>
            </th>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="contest_deadline"><?php _e('Image'); ?></label>
            </th>

            <td>
                <input class="jury_image_1_url" name="jury_image_1_url" type="text" value=""/>
                <input class="jury_image_1_id" name="jury_image_1_id" type="hidden" value="<?php echo $jury_image_1_id; ?>"/>
                <input  class="upload_image_button" type="button" value="Upload Image"
                       data-upload-image-class="jury_image_1_url"
                       data-upload-image-src="jury_image_1"
                       data-upload-image-id="jury_image_1_id"/>
                <br>
                <div class="contest-img"><img src="<?php echo $jury_image_1_image; ?>" class="jury_image_1" alt=""></div>
            </td>

        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="jury_title_1"><?php _e('Jury Title'); ?></label>
            </th>
            <td>
                <input type="text" name="jury_title_1" id="jury_title_1" value="<?php echo $jury_title_1; ?>" />
            </td>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="jury_website_1"><?php _e('Jury Website'); ?></label>
            </th>
            <td>
                <input type="text" name="jury_website_1" id="jury_website_1" value="<?php echo $jury_website_1; ?>" />
            </td>
        </tr>



        <!--  ############      Jury 2     ############-->
        <tr class="form-field contest-extra-fields" >
            <th scope="row" valign="top" colspan="2">
                <h1>Jury 2</h1>
            </th>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="contest_deadline"><?php _e('Image'); ?></label>
            </th>

            <td>
                <input class="jury_image_2_url" name="jury_image_2_url" type="text" value=""/>
                <input class="jury_image_2_id" name="jury_image_2_id" type="hidden" value="<?php echo $jury_image_2_id; ?>"/>
                <input  class="upload_image_button" type="button" value="Upload Image"
                       data-upload-image-class="jury_image_2_url"
                       data-upload-image-src="jury_image_2"
                       data-upload-image-id="jury_image_2_id"/>
                <br>
                <div class="contest-img"><img src="<?php echo $jury_image_2_image; ?>" class="jury_image_2" alt=""></div>
            </td>

        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="jury_title_2"><?php _e('Jury Title'); ?></label>
            </th>
            <td>
                <input type="text" name="jury_title_2" id="jury_title_2" value="<?php echo $jury_title_2; ?>" />
            </td>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="jury_website_2"><?php _e('Jury Website'); ?></label>
            </th>
            <td>
                <input type="text" name="jury_website_2" id="jury_website_2" value="<?php echo $jury_website_2; ?>" />
            </td>
        </tr>




        <!--  ############      Jury 3     ############-->
        <tr class="form-field contest-extra-fields" >
            <th scope="row" valign="top" colspan="2">
                <h1>Jury 3</h1>
            </th>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="contest_deadline"><?php _e('Image'); ?></label>
            </th>

            <td>
                <input class="jury_image_3_url" name="jury_image_3_url" type="text" value=""/>
                <input class="jury_image_3_id" name="jury_image_3_id" type="hidden" value="<?php echo $jury_image_3_id; ?>"/>
                <input  class="upload_image_button" type="button" value="Upload Image"
                       data-upload-image-class="jury_image_3_url"
                       data-upload-image-src="jury_image_3"
                       data-upload-image-id="jury_image_3_id"/>
                <br>
                <div class="contest-img"><img src="<?php echo $jury_image_3_image; ?>" class="jury_image_3" alt=""></div>
            </td>

        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="jury_title_3"><?php _e('Jury Title'); ?></label>
            </th>
            <td>
                <input type="text" name="jury_title_3" id="jury_title_3" value="<?php echo $jury_title_3; ?>" />
            </td>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="jury_website_3"><?php _e('Jury Website'); ?></label>
            </th>
            <td>
                <input type="text" name="jury_website_3" id="jury_website_3" value="<?php echo $jury_website_3; ?>" />
            </td>
        </tr>





        <!--  ############      Jury 4     ############-->
        <tr class="form-field contest-extra-fields" >
            <th scope="row" valign="top" colspan="2">
                <h1>Jury 4</h1>
            </th>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="contest_deadline"><?php _e('Image'); ?></label>
            </th>

            <td>
                <input class="jury_image_4_url" name="jury_image_4_url" type="text" value=""/>
                <input class="jury_image_4_id" name="jury_image_4_id" type="hidden" value="<?php echo $jury_image_4_id; ?>"/>
                <input  class="upload_image_button" type="button" value="Upload Image"
                       data-upload-image-class="jury_image_4_url"
                       data-upload-image-src="jury_image_4"
                       data-upload-image-id="jury_image_4_id"/>
                <br>
                <div class="contest-img"><img src="<?php echo $jury_image_4_image; ?>" class="jury_image_4" alt=""></div>
            </td>

        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="jury_title_4"><?php _e('Jury Title'); ?></label>
            </th>
            <td>
                <input type="text" name="jury_title_4" id="jury_title_4" value="<?php echo $jury_title_4; ?>" />
            </td>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="jury_website_4"><?php _e('Jury Website'); ?></label>
            </th>
            <td>
                <input type="text" name="jury_website_4" id="jury_website_4" value="<?php echo $jury_website_4; ?>" />
            </td>
        </tr>






        <!--  ############      Jury 5     ############-->

        <tr class="form-field contest-extra-fields" >
            <th scope="row" valign="top" colspan="2">
                <h1>Jury 5</h1>
            </th>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="contest_deadline"><?php _e('Image'); ?></label>
            </th>

            <td>
                <input class="jury_image_5_url" name="jury_image_5_url" type="text" value=""/>
                <input class="jury_image_5_id" name="jury_image_5_id" type="hidden" value="<?php echo $jury_image_5_id; ?>"/>
                <input  class="upload_image_button" type="button" value="Upload Image"
                       data-upload-image-class="jury_image_5_url"
                       data-upload-image-src="jury_image_5"
                       data-upload-image-id="jury_image_5_id"/>
                <br>
                <div class="contest-img"><img src="<?php echo $jury_image_5_image; ?>" class="jury_image_5" alt=""></div>
            </td>

        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="jury_title_5"><?php _e('Jury Title'); ?></label>
            </th>
            <td>
                <input type="text" name="jury_title_5" id="jury_title_5" value="<?php echo $jury_title_5; ?>" />
            </td>
        </tr>

        <tr class="form-field contest-extra-fields">
            <th scope="row" valign="top">
                <label for="jury_website_5"><?php _e('Jury Website'); ?></label>
            </th>
            <td>
                <input type="text" name="jury_website_5" id="jury_website_5" value="<?php echo $jury_website_5; ?>" />
            </td>
        </tr>




    <script>
        jQuery(document).ready( function( $ ) {
             $('#contest_deadline').datepicker({
                 // dateFormat : 'dd/m - yy'
             });
        });
    </script>

    <?php
}






// A callback function to save our extra taxonomy field(s)
function save_contest_category_custom_fields( $term_id ) {


    if(isset($_POST['taxonomy']) && $_POST['taxonomy'] == "contest_category"){

       global $obj;
       global $email_template;

        $term = get_term_by( "id", $term_id, "contest_category");
        $term_name = $term->name;
       
        update_term_meta ( $term_id, 'contest_status', $_POST['contest_status'] );
        update_term_meta ( $term_id, 'old_contest', $_POST['old_contest'] );
        update_term_meta ( $term_id, 'contest_mode', $_POST['contest_mode'] );
        update_term_meta ( $term_id, 'upload_main_image_id', $_POST['upload_main_image_id'] );
        update_term_meta ( $term_id, 'contest_deadline', $_POST['contest_deadline'] );

        update_term_meta ( $term_id, 'prize_data_gold', $_POST['prize_data_gold'] );
        update_term_meta ( $term_id, 'prize_data_silver', $_POST['prize_data_silver'] );
        update_term_meta ( $term_id, 'prize_data_bronze', $_POST['prize_data_bronze'] );

        update_term_meta ( $term_id, 'sponsor_image_id', $_POST['sponsor_image_id'] );
        update_term_meta ( $term_id, 'sponsor_title', $_POST['sponsor_title'] );
        update_term_meta ( $term_id, 'sponsor_description', $_POST['sponsor_description'] );

        update_term_meta ( $term_id, 'jury_content_title', $_POST['jury_content_title'] );
        update_term_meta ( $term_id, 'jury_content', $_POST['jury_content'] );

        update_term_meta ( $term_id, 'jury_image_1_id', $_POST['jury_image_1_id'] );
        update_term_meta ( $term_id, 'jury_title_1', $_POST['jury_title_1'] );
        update_term_meta ( $term_id, 'jury_website_1', $_POST['jury_website_1'] );

        update_term_meta ( $term_id, 'jury_image_2_id', $_POST['jury_image_2_id'] );
        update_term_meta ( $term_id, 'jury_title_2', $_POST['jury_title_2'] );
        update_term_meta ( $term_id, 'jury_website_2', $_POST['jury_website_2'] );

        update_term_meta ( $term_id, 'jury_image_3_id', $_POST['jury_image_3_id'] );
        update_term_meta ( $term_id, 'jury_title_3', $_POST['jury_title_3'] );
        update_term_meta ( $term_id, 'jury_website_3', $_POST['jury_website_3'] );

        update_term_meta ( $term_id, 'jury_image_4_id', $_POST['jury_image_4_id'] );
        update_term_meta ( $term_id, 'jury_title_4', $_POST['jury_title_4'] );
        update_term_meta ( $term_id, 'jury_website_4', $_POST['jury_website_4'] );

        update_term_meta ( $term_id, 'jury_image_5_id', $_POST['jury_image_5_id'] );
        update_term_meta ( $term_id, 'jury_title_5', $_POST['jury_title_5'] );
        update_term_meta ( $term_id, 'jury_website_5', $_POST['jury_website_5'] );


        if($_POST['previous_contest_status'] != $_POST['contest_status']  && $_POST['contest_status'] == "active"){
            $all_contest_terms = get_terms( array('taxonomy' => 'contest_category','hide_empty' => false,) );
            foreach ($all_contest_terms as $single_term){
                echo $single_term_id = $single_term->term_id;
                if($term_id != $single_term_id){
                    update_term_meta ( $single_term_id, 'contest_status', "inactive" );
                }
            }
        }



        if(isset($_POST['notify_email_mode_change']) && $_POST['notify_email_mode_change'] == "yes" && $_POST['previous_contest_mode'] != $_POST['contest_mode'] ){
            $user_query = $obj->get_all_users_excludin_administrator();
            if ( ! empty( $user_query->results ) ) {
                foreach ($user_query->results as $user) {
                    //        Send mail
                    $email_template->user_id = $user->ID;
                    $email_template->email_users_notify_contest_mode_change($term_name,$_POST['contest_mode']);
                }
            }
        }



        if(isset($_POST['winner_email']) && $_POST['winner_email'] == "yes" && $_POST['previous_contest_mode'] != $_POST['contest_mode']  && $_POST['contest_mode'] == "final"){
            $submissions = $obj->get_contest_submissions($term_id);
            global $email_template;
            foreach ($submissions as $submission_id) {
                $tags = get_the_terms( $submission_id,"submission_tag");
                if(is_array($tags) && count($tags) > 0){
                    $tag = current($tags)->slug;
                    $user_id = get_post_meta($submission_id,"art_user_id",true);
                    $email_template->user_id = $user_id;
                    $email_template->email_winner_notification($submission_id,$tag);
                }
            }
        }


    }
    
}

add_action( 'edited_contest_category', 'save_contest_category_custom_fields', 10, 2 );




/**
 * Add Column to Contest Taxonomy List Admin.
 */


function custom_contest_category_column_header( $columns ){
    $columns['contest_status'] = 'Contest Status';
    $columns['contest_mode'] = 'Contest Mode';
    $columns['contest_older'] = 'Contest Older';
    return $columns;
}
add_filter( "manage_edit-contest_category_columns", 'custom_contest_category_column_header', 10);


function custom_contest_category_column_content( $value, $column_name, $term_id ){

    if ($column_name === 'contest_status') {
        $contest_status = get_term_meta ( $term_id, 'contest_status',true);
        if($contest_status == "active"){
            echo "<span class='contest-column-active'>$contest_status</span>";
        }
        else{
            echo "<span class='contest-column-inactive'>Inactive</span>";
        }
    }

    if ($column_name === 'contest_mode') {
        echo $contest_mode = get_term_meta ( $term_id, 'contest_mode',true);
    }

    if ($column_name === 'contest_older') {
        echo get_term_meta ( $term_id, 'old_contest',true);
    }

}
add_action( "manage_contest_category_custom_column", 'custom_contest_category_column_content', 10, 3);










// ****************************************************************
//                  META BOXES for User Payment
// ****************************************************************


add_action( 'payment_type_add_form_fields', 'payment_type_taxonomy_custom_fields', 10, 2 );
add_action( 'payment_type_edit_form_fields', 'payment_type_taxonomy_custom_fields', 10, 2 );

function payment_type_taxonomy_custom_fields($term)
{
    $term_id = $term->term_id;

    $payment_type = get_term_meta ( $term_id, 'payment_type',true);
    $payment_price = get_term_meta ( $term_id, 'payment_price',true);
    $upgrade_membership_time = get_term_meta ( $term_id, 'upgrade_membership_time',true);
    $credit_package = get_term_meta ( $term_id, 'credit_package',true);

    ?>

    <tr class="form-field payment-type-extra-fields" >
        <th scope="row" valign="top" colspan="2">
            <h1>Extra Fields</h1>
        </th>
    </tr>


    <tr class="form-field payment-type-extra-fields">
        <th scope="row" valign="top">
            <label for="payment_type"><?php _e('Payment Type'); ?></label>
        </th>
        <td>
            <select name="payment_type" id="payment_type">
                <option value="" >-- Select Payment Type --</option>
                <option value="credit" <?php if($payment_type == "credit") { echo "selected"; } ?> >Credit</option>
                <option value="upgrade_membership" <?php if($payment_type == "upgrade_membership") { echo "selected"; } ?>>Upgrade Membership</option>
            </select>

            <br><span class="description"><?php _e('Select the type of Payment'); ?></span>
            <br><br>
        </td>
    </tr>


    <tr class="form-field payment-type-extra-fields">
        <th scope="row" valign="top">
            <label for="payment_price"><?php _e('Price'); ?></label>
        </th>
        <td>
            <input type="number" name="payment_price" id="payment_price"  step="any" min="0.1" value="<?php echo $payment_price; ?>" />
            <br><br>
        </td>
    </tr>



    <tr class="form-field payment-type-extra-fields">
        <th scope="row" valign="top">
            <label for="upgrade_membership_time"><?php _e('Membership Time'); ?></label>
        </th>
        <td>
            <select name="upgrade_membership_time" id="upgrade_membership_time">
                <option value="" >-- Select Membership Type --</option>
                <option value="month" <?php if($upgrade_membership_time == "month") { echo "selected"; } ?> >Month</option>
                <option value="year" <?php if($upgrade_membership_time == "year") { echo "selected"; } ?>>Year</option>
            </select>

            <br><span class="description"><?php _e('Select the type of Membership'); ?></span>
            <br><br>
        </td>
    </tr>



    <tr class="form-field payment-type-extra-fields">
        <th scope="row" valign="top">
            <label for="credit_package"><?php _e('Credit Package'); ?></label>
        </th>
        <td>
            <input type="number" name="credit_package" id="credit_package" value="<?php echo $credit_package; ?>" />
            <br><span class="description"><?php _e('The number of Arts user can upload for a particular contest.'); ?></span>
            <br><br>
        </td>
    </tr>


    <?php
}



// A callback function to save our extra taxonomy field(s)
function save_payment_type_custom_fields( $term_id )
{
    if (isset($_POST['taxonomy']) && $_POST['taxonomy'] == "payment_type") {
        update_term_meta ( $term_id, 'payment_type', $_POST['payment_type'] );
        update_term_meta ( $term_id, 'payment_price', $_POST['payment_price'] );
        update_term_meta ( $term_id, 'upgrade_membership_time', $_POST['upgrade_membership_time'] );
        update_term_meta ( $term_id, 'credit_package', $_POST['credit_package'] );
    }
}
add_action( 'edited_payment_type', 'save_payment_type_custom_fields', 10, 2 );
add_action( 'create_payment_type', 'save_payment_type_custom_fields', 10, 2 );





/**
 * Add Column to Payment Type List Admin.
 */


function custom_payment_type_column_header( $columns ){
    $columns['payment_price'] = 'Price';
    return $columns;
}
add_filter( "manage_edit-payment_type_columns", 'custom_payment_type_column_header', 10);


function custom_payment_type_column_content( $value, $column_name, $term_id ){
    if ($column_name === 'payment_price') {
        $payment_price = get_term_meta ( $term_id, 'payment_price',true);
       echo GP_CURRENCY_SYMBOL.$payment_price;
    }
}
add_action( "manage_payment_type_custom_column", 'custom_payment_type_column_content', 10, 3);




