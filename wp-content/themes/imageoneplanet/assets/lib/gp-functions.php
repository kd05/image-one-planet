<?php
if (!defined('ABSPATH')) {
    exit;
}






/**
 * Returns the query posts (custom/page/post)
 * @param $options
 * @return array|bool
 */
function gp_get_posts( $options ) {
    // Default
    $options = array_merge(
        array(
            'post_type'      => 'post',
            'posts_per_page' => 5,
            'orderby'        => 'menu_order',
            'order'          => 'ASC'
        ), $options
    );

    // Args
    $args = array(
        'post_type'      => $options['post_type'],
        'posts_per_page' => $options['posts_per_page'],
        'orderby'        => $options['orderby'],
        'order'          => $options['order'],
    );

    // Query
    $query = new WP_Query( $args );

    // Results
    return $query;
}






function gp_excerptize($content, $num = 30){
    $number=$num;
    //$content = apply_filters('the_content', $content);
    //echo "<pre style='display: none;'>".print_r($content, true)."</pre>";
    $content = strip_tags($content, '<br>');
    $content = str_replace('&nbsp;', '', $content);

    $contentArray = explode(' ', $content,$number+1);
    //echo '<pre>'.print_r($contentArray, true).'</pre>';
    $contentString='';
    foreach($contentArray as $key=>$value){
        if($key>=$number){
            $contentString.='...';
            break;
        }
        $contentString.=trim($value);
        if($key<$number-1){
            $contentString.=' ';
        }
    }
    return $contentString;
}







// ***********************************************
// ****************  Get Taxonomy  ***************
// ***********************************************
function gp_get_post_by_meta($post_type, $meta_key, $meta_value, $total_number) {


    $prod_cat_terms = get_posts( array(
        'fields'    => 'ids',
        'post_type' => $post_type,
        'posts_per_page' => $total_number,
        'orderby' => 'title',
        'order' => 'ASC',
        'meta_query' => array(
            array(
                'key'       => $meta_key,
                'value'     => $meta_value,
                'compare'   => '='
            )
        )
    ) );

    return $prod_cat_terms;
}



function gp_get_post_by_multiple_meta($post_type, $meta_array, $total_number) {

    $meta_query_array = array();
    foreach ($meta_array as $meta_key => $meta_val){
        $temp = array(
                'key'       => $meta_key,
                'value'     => $meta_val,
                'compare'   => '='
            );

        array_push($meta_query_array,$temp);
    }

    $prod_cat_terms = get_posts( array(
        'fields'    => 'ids',
        'post_type' => $post_type,
        'posts_per_page' => $total_number,
        'orderby' => 'title',
        'order' => 'ASC',
        'meta_query' => array(
            $meta_query_array
        )
    ) );
    return $prod_cat_terms;
}




function get_product_image($prod_id,$image_size = "large"){
    $prod_image = wp_get_attachment_image_src( get_post_thumbnail_id( $prod_id ), $image_size );
    if(is_array($prod_image)){
        return $prod_image = current($prod_image);
    }
    else{
        $prod_image = get_image_not_found();
        return $prod_image;
    }
}



function get_image_not_found(){
    return get_template_directory_uri()."/assets/images/no-image.jpg";
}



function get_no_profile_pic_found(){
    return get_template_directory_uri()."/assets/images/no-profile-image.jpg";
}





?>
