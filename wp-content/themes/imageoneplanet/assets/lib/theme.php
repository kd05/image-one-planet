<?php



add_action('admin_enqueue_scripts', 'add_my_js');
function add_my_js(){
    $theme_dir = get_template_directory_uri();

    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');


    wp_enqueue_script('gp_validate', $theme_dir.'/assets/js/admin/jquery.validate.min.js', array('jquery'));
    wp_enqueue_script('gp_script_js', $theme_dir.'/assets/js/admin/validate-script.js');

    wp_enqueue_style("gp_admin_css", $theme_dir.'/assets/css/admin/admin.css');
    wp_enqueue_style( 'jquery-ui-style', $theme_dir.'/assets/css/admin/jquery-ui.css', true);
}








/**
 * Enqueues scripts and styles.
 */
function gp_enqueue_scripts() {

    $theme_dir = get_template_directory_uri();
    wp_enqueue_script( 'jquery-ui-datepicker' );
    wp_enqueue_style("gp_date_picker_style", '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');


    wp_enqueue_style("magnific_popup_style", $theme_dir.'/assets/css/lib/magnific-popup.css');
    wp_enqueue_script('magnific_popup_script', $theme_dir.'/assets/js/lib/jquery.magnific-popup.min.js', array('jquery'));


    wp_enqueue_script('gp_main_script', $theme_dir.'/assets/js/main.min.js', array('jquery'));

    $js_urls = array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'site_url' => site_url(),
    );
    wp_register_script('local_variables',"");
    wp_localize_script('local_variables', 'website_urls', $js_urls );
    wp_enqueue_script('local_variables');

}
add_action( 'wp_enqueue_scripts', 'gp_enqueue_scripts' );




