<?php
if (!defined('ABSPATH')) {
    exit;
}



add_action('init', 'gp_register_custom_post_type');
function gp_register_custom_post_type() {


//  ######  User Profile Gallery  ######
    $labels = array(
        'name'               => _x( 'User Profile Gallery', 'imageoneplanet' ),
        'singular_name'      => _x( 'User Profile Gallery', 'imageoneplanet' ),
        'add_new'            => _x( 'Add New', 'imageoneplanet' ),
        'add_new_item'       => __( 'Add New User Profile Gallery' ),
        'edit_item'          => __( 'Edit User Profile Gallery' ),
        'new_item'           => __( 'New User Profile Gallery' ),
        'all_items'          => __( 'All User Profile Galleries' ),
        'view_item'          => __( 'View User Profile Gallery' ),
        'search_items'       => __( 'Search User Profile Galleries' ),
        'not_found'          => __( 'No User Profile Galleries found' ),
        'not_found_in_trash' => __( 'No User Profile Galleries found in the Trash' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'User Profile Gallery'
    );

    $args = array(
        'labels'        => $labels,
        'description'   => 'User Profile Gallery',
        'menu_icon'   => 'dashicons-images-alt2',
        'exclude_from_search' => true,
        'show_ui' => true,
        'hierarchical' => false,
        'public'        => true,
        'menu_position' => 23,
        'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ,'page-attributes'),
        'rewrite' => array('slug' => 'user_profile_gallery', 'with_front'  => false),
        'has_archive'   => true,
        'register_meta_box_cb' => 'gp_add_user_profile_gallery_metaboxes',
    );
    register_post_type( 'user_profile_gallery', $args );




//   ######   User Art  ######
    $labels = array(
        'name'               => _x( 'User Art', 'imageoneplanet' ),
        'singular_name'      => _x( 'User Art', 'imageoneplanet' ),
        'add_new'            => _x( 'Add New', 'imageoneplanet' ),
        'add_new_item'       => __( 'Add New User Art' ),
        'edit_item'          => __( 'Edit User Art' ),
        'new_item'           => __( 'New User Art' ),
        'all_items'          => __( 'All User Profile Arts' ),
        'view_item'          => __( 'View User Art' ),
        'search_items'       => __( 'Search User Profile Arts' ),
        'not_found'          => __( 'No User Profile Arts found' ),
        'not_found_in_trash' => __( 'No User Profile Arts found in the Trash' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'User Arts'
    );

    $args = array(
        'labels'        => $labels,
        'description'   => 'User Art',
        'menu_icon'   => 'dashicons-admin-customizer',
        'exclude_from_search' => true,
        'show_ui' => true,
        'hierarchical' => false,
        'public'        => true,
        'menu_position' => 23,
        'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ,'page-attributes'),
        'rewrite' => array('slug' => 'user_art', 'with_front'  => false),
        'has_archive'   => true,
        'register_meta_box_cb' => 'gp_add_user_art_metaboxes',
    );
    register_post_type( 'user_art', $args );




    //   ######   Contest Submission  ######
    $labels = array(
        'name'               => _x( 'Contest Submission', 'imageoneplanet' ),
        'singular_name'      => _x( 'Contest Submission', 'imageoneplanet' ),
        'add_new'            => _x( 'Add New', 'imageoneplanet' ),
        'add_new_item'       => __( 'Add New Contest Submission' ),
        'edit_item'          => __( 'Edit Contest Submission' ),
        'new_item'           => __( 'New Contest Submission' ),
        'all_items'          => __( 'All Contest Submissions' ),
        'view_item'          => __( 'View Contest Submission' ),
        'search_items'       => __( 'Search Contest Submissions' ),
        'not_found'          => __( 'No Contest Submissions found' ),
        'not_found_in_trash' => __( 'No Contest Submissions found in the Trash' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'Contest Submissions'
    );

    $args = array(
        'labels'        => $labels,
        'description'   => 'Contest Submissions',
        'menu_icon'   => 'dashicons-upload',
        'exclude_from_search' => true,
        'show_ui' => true,
        'hierarchical' => false,
        'public'        => true,
        'menu_position' => 23,
        'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ,'page-attributes'),
        'rewrite' => array('slug' => 'contest_submission', 'with_front'  => false),
        'has_archive'   => true,
        'register_meta_box_cb' => 'gp_add_contest_submission_metaboxes',
    );
    register_post_type( 'contest_submission', $args );








//   ######   Payment  ######
    $labels = array(
        'name'               => _x( 'User Payment', 'imageoneplanet' ),
        'singular_name'      => _x( 'User Payment', 'imageoneplanet' ),
        'add_new'            => _x( 'Add New', 'imageoneplanet' ),
        'add_new_item'       => __( 'Add New User Payment' ),
        'edit_item'          => __( 'Edit User Payment' ),
        'new_item'           => __( 'New User Payment' ),
        'all_items'          => __( 'All User Payments' ),
        'view_item'          => __( 'View User Payment' ),
        'search_items'       => __( 'Search User Payments' ),
        'not_found'          => __( 'No User Payments found' ),
        'not_found_in_trash' => __( 'No User Payments found in the Trash' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'User Payments'
    );

    $args = array(
        'labels'        => $labels,
        'description'   => 'User Payment',
        'menu_icon'   => 'dashicons-cart',
        'exclude_from_search' => true,
        'show_ui' => true,
        'hierarchical' => false,
        'public'        => true,
        'menu_position' => 23,
        'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ,'page-attributes'),
        'rewrite' => array('slug' => 'user_payment', 'with_front'  => false),
        'has_archive'   => true,
        'register_meta_box_cb' => 'gp_add_user_payment_metaboxes',
    );
    register_post_type( 'user_payment', $args );






    //   ######   Coupon Codes  ######
    $labels = array(
        'name'               => _x( 'Coupon Code', 'imageoneplanet' ),
        'singular_name'      => _x( 'Coupon Code', 'imageoneplanet' ),
        'add_new'            => _x( 'Add New', 'imageoneplanet' ),
        'add_new_item'       => __( 'Add New Coupon Code' ),
        'edit_item'          => __( 'Edit Coupon Code' ),
        'new_item'           => __( 'New Coupon Code' ),
        'all_items'          => __( 'All Coupon Codes' ),
        'view_item'          => __( 'View Coupon Code' ),
        'search_items'       => __( 'Search Coupon Codes' ),
        'not_found'          => __( 'No Coupon Codes found' ),
        'not_found_in_trash' => __( 'No Coupon Codes found in the Trash' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'Coupon Codes'
    );

    $args = array(
        'labels'        => $labels,
        'description'   => 'Coupon Code',
        'menu_icon'   => 'dashicons-index-card',
        'exclude_from_search' => true,
        'show_ui' => true,
        'hierarchical' => false,
        'public'        => true,
        'menu_position' => 23,
        'supports'      => array( 'title', 'thumbnail', 'excerpt', 'comments' ,'page-attributes'),
        'rewrite' => array('slug' => 'gp_coupon_code', 'with_front'  => false),
        'has_archive'   => true,
        'register_meta_box_cb' => 'gp_add_coupon_code_metaboxes',
    );
    register_post_type( 'gp_coupon_code', $args );




}










// ***********************************************************
//                  META BOXES for User Profile Gallery
// ***********************************************************

/**
 * User Profile Gallery Meta Box
 */
function gp_add_user_profile_gallery_metaboxes( $post ) {

    add_meta_box(
        'user-profile-gallery',
        'User Profile Gallery Fields',
        'gp_user_profile_gallery_meta_box',
        'user_profile_gallery',
        'normal',
        'low'
    );

}



/**
 * Create the metabox data
 */
function gp_user_profile_gallery_meta_box(){

    global $post;

    // Nonce field to validate form request came from current site
    wp_nonce_field( basename( __FILE__ ), 'user_profile_gallery_fields' );

    $status_array = array("For Sale","Not for sale","Sold");
    $image_type_array = array("regular_image","featured_image");
    $listing_image_array = array("regular_image","listing_image");

    $post_id = $post->ID;
    $user_image_type = get_post_meta( $post_id, 'image-type', true );
    $listing_image_type = get_post_meta( $post_id, 'listing-image-type', true );
    $size = get_post_meta($post_id,"image-size", true);
    $frame = get_post_meta($post_id,"image-frame", true);
    $user_image_year = get_post_meta( $post_id, 'image-year', true );
    $user_image_status = get_post_meta( $post_id, 'image-status', true );
    $user_image_price = get_post_meta( $post_id, 'image-price', true );
    $participant_user_id = trim(get_post_meta( $post_id, 'participant_user_id', true ));
    $user_info = get_userdata($participant_user_id);
    $user_name = $user_info->first_name." ".$user_info->last_name;

    ?>

    <table border="0" class="table-page-user-profile-gallery">

        <tr>
            <th>Image Type</th>
            <td>
                <select name="image-type" id="image-type">
                    <?php foreach($image_type_array as $image_type){ ?>
                        <option value="<?php echo $image_type ?>" <?php if($user_image_type == $image_type) { echo "selected"; } ?>><?php echo $image_type ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>


        <tr>
            <th>Listing Image Type</th>
            <td>
                <select name="listing-image-type" id="listing-image-type">
                    <?php foreach($listing_image_array as $listing_image){ ?>
                        <option value="<?php echo $listing_image ?>" <?php if($listing_image_type == $listing_image) { echo "selected"; } ?>><?php echo $listing_image ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>


        <tr>
            <th>Year</th>
            <td><input type="text" name="image-year" id="image-year" value="<?php echo $user_image_year; ?>" ></td>
        </tr>

        <tr>
            <th>Size</th>
            <td><input type="text" name="image-size" id="image-size" value="<?php echo $size; ?>" ></td>
        </tr>

        <tr>
            <th>Frame</th>
            <td>
                <div class="radio-field">
                    <input type="radio" name="image-frame" id="image-frame" value="Frame Included" <?php  if($frame == "Frame Included") echo "checked"; ?>>
                    <span>Frame Included</span>
                </div>
                <div class="radio-field">
                    <input type="radio" name="image-frame" id="image-frame" value="Frame Excluded" <?php  if($frame == "Frame Excluded") echo "checked"; ?>>
                    <span>Frame Excluded</span>
                </div>
            </td>
        </tr>

        <tr>
            <th>Status</th>
            <td>
                <select name="image-status" id="image-status">
                    <?php foreach($status_array as $status){ ?>
                        <option value="<?php echo $status ?>"  <?php if($user_image_status == $status) { echo "selected"; } ?>><?php echo $status ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>

        <tr>
            <th>Price/Value</th>
            <td><input type="text" name="image-price" id="image-price" value="<?php echo $user_image_price; ?>" ></td>
        </tr>

        <tr>
            <th>ID</th>
            <td><input type="text"  value="<?php echo $post_id; ?>" disabled></td>
        </tr>


        <tr>
            <th>User ID </th>
            <td>
                <input type="text" name="participant_user_id" id="participant_user_id" value="<?php echo $participant_user_id; ?>" >
                <a href="<?php echo admin_url()."/user-edit.php?user_id=".$participant_user_id; ?>">
                    <b style="color: #0088ff"><?php echo $user_name; ?></b>
                </a>
            </td>
        </tr>

    </table>

<?php

}


/**
 * Save the metabox data
 */
function gp_save_user_profile_gallery_meta( $post_id, $post ) {

    // Return if the user doesn't have edit permissions.
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
    }

    // Verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times.
    if ( !isset( $_POST['image-year'] ) || ! wp_verify_nonce( $_POST['user_profile_gallery_fields'], basename(__FILE__) ) ) {
        return $post_id;
    }

    update_post_meta( $post_id, "image-type", esc_attr($_POST['image-type']) );
    update_post_meta( $post_id, "listing-image-type", esc_attr($_POST['listing-image-type']) );
    update_post_meta( $post_id, "image-year", esc_attr($_POST['image-year']) );
    update_post_meta( $post_id, "image-size", esc_attr($_POST['image-size']) );
    update_post_meta( $post_id, "image-frame", esc_attr($_POST['image-frame']) );
    update_post_meta( $post_id, "image-status", esc_attr($_POST['image-status']) );
    update_post_meta( $post_id, "image-price", esc_attr($_POST['image-price']) );
    update_post_meta( $post_id, "participant_user_id", esc_attr($_POST['participant_user_id']) );

}
add_action( 'save_post', 'gp_save_user_profile_gallery_meta', 1, 2 );



/**
 * Show featured image in custom post type list
 */
add_filter('manage_user_profile_gallery_posts_columns', 'add_user_profile_gallery_posts_columns');

add_action('manage_user_profile_gallery_posts_custom_column', 'show_user_profile_gallery_posts_columns', 10, 2);


function add_user_profile_gallery_posts_columns($defaults) {
    $defaults['featured_image'] = 'Featured Image';
    $defaults['user_id'] = 'User';
    return $defaults;
}


function show_user_profile_gallery_posts_columns($column_name, $post_id) {
    if ($column_name == 'featured_image') {
        echo get_the_post_thumbnail($post_id, 'thumbnail');
    }

    if ($column_name == 'user_id') {
        global $obj;
        $user_id =  get_post_meta($post_id,"participant_user_id",true);
        $user_data = $obj->get_user_data($user_id);
        echo "<h4>User Id : $user_id , ".$user_data['full_name']."</h4>";
    }
}





// ***********************************************************
//                  META BOXES for User Art
// ***********************************************************


/**
 * User Profile Gallery Meta Box
 */
function gp_add_user_art_metaboxes( $post ) {

    add_meta_box(
        'user-art',
        'User Art Fields',
        'gp_user_art_meta_box',
        'user_art',
        'normal',
        'low'
    );

}






/**
 * Create the metabox data
 */
function gp_user_art_meta_box()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'user_art_fields');

    $post_id = $post->ID;
    $user_image_year_taken = get_post_meta( $post_id, 'image-year-taken', true );
    $art_user_id = trim(get_post_meta( $post_id, 'art_user_id', true ));
    $user_info = get_userdata($art_user_id);
    $user_name = $user_info->first_name." ".$user_info->last_name;

?>
        <table border="0" class="table-page-user-art">
            <tr>
                <th>Year</th>
                <td><input type="text" name="image-year-taken" id="image-year-taken" value="<?php echo $user_image_year_taken; ?>" ></td>
            </tr>

            <tr>
                <th>User ID <br><span style="color: #ff0000">(Only Change if Admin adds the image on behalf of user)</span></th>
                <td>
                    <input type="text" name="art_user_id" id="art_user_id" value="<?php echo $art_user_id; ?>" >
                    <a href="<?php echo admin_url()."/user-edit.php?user_id=".$art_user_id; ?>">
                        <b style="color: #0088ff"><?php echo $user_name; ?></b>
                    </a>
                </td>
            </tr>

        </table>
<?php
}



/**
 * Save the metabox data
 */
function gp_save_user_art_meta( $post_id, $post ) {

    // Return if the user doesn't have edit permissions.
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
    }

    // Verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times.
    if ( !isset( $_POST['image-year-taken'] ) || ! wp_verify_nonce( $_POST['user_art_fields'], basename(__FILE__) ) ) {
        return $post_id;
    }

    update_post_meta( $post_id, "image-year-taken", esc_attr($_POST['image-year-taken']) );
    update_post_meta( $post_id, "art_user_id", esc_attr($_POST['art_user_id']) );

}
add_action( 'save_post', 'gp_save_user_art_meta', 1, 2 );




/**
 * Show featured image in custom post type list
 */
add_filter('manage_user_art_posts_columns', 'add_featured_image_column');

add_action('manage_user_art_posts_custom_column', 'show_featured_image_column', 10, 2);







// ***********************************************************
//                  META BOXES for Contest Submissions
// ***********************************************************


/**
 *  Contest Submissions Meta Box
 */
function gp_add_contest_submission_metaboxes( $post ) {

    add_meta_box(
        'contest-submission',
        'Contest Submissions Fields',
        'gp_contest_submission_meta_box',
        'contest_submission',
        'normal',
        'low'
    );

}




/**
 * Create the metabox data
 */
function gp_contest_submission_meta_box()
{
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'contest_submission_fields');

    $post_id = $post->ID;
    $user_image_year_taken = get_post_meta( $post_id, 'image-year-taken', true );
    $submission_social_link = get_post_meta( $post_id, 'submission_social_link', true );

    $art_user_id = trim(get_post_meta( $post_id, 'art_user_id', true ));
    $user_info = get_userdata($art_user_id);
    $user_name = $user_info->first_name." ".$user_info->last_name;

    ?>
    <table border="0" class="table-page-submission">
        <tr>
            <th>Submission Social Media Link</th>
            <td><input type="text" class="full-width-text" name="submission_social_link" id="submission_social_link" value="<?php echo $submission_social_link; ?>" ></td>
        </tr>

        <tr>
            <th colspan="2"><br><hr/><br></th>
        </tr>

        <tr>
            <th>Year</th>
            <td><input type="text" name="image-year-taken" id="image-year-taken" value="<?php echo $user_image_year_taken; ?>" ></td>
        </tr>

        <tr>
            <th>User ID </th>
            <td>
                <input type="text" name="art_user_id" id="art_user_id" value="<?php echo $art_user_id; ?>" >
                <a href="<?php echo admin_url()."/user-edit.php?user_id=".$art_user_id; ?>">
                    <b style="color: #0088ff"><?php echo $user_name; ?></b>
                </a>
            </td>
        </tr>



    </table>
    <?php
}



/**
 * Save the metabox data
 */
function gp_save_contest_submission_meta( $post_id, $post ) {

    // Return if the user doesn't have edit permissions.
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
    }

    // Verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times.
    if ( !isset( $_POST['image-year-taken'] ) || ! wp_verify_nonce( $_POST['contest_submission_fields'], basename(__FILE__) ) ) {
        return $post_id;
    }

    update_post_meta( $post_id, "image-year-taken", esc_attr($_POST['image-year-taken']) );
    update_post_meta( $post_id, "art_user_id", esc_attr($_POST['art_user_id']) );
    update_post_meta( $post_id, "submission_social_link", esc_attr($_POST['submission_social_link']) );

}
add_action( 'save_post', 'gp_save_contest_submission_meta', 1, 2 );





/**
 * Show featured image in custom post type list
 */
//add_filter('manage_contest_submission_posts_columns', 'add_featured_image_column');
//
//add_action('manage_contest_submission_posts_custom_column', 'show_featured_image_column', 10, 2);






/**
 * Show featured image in custom post type list
 */
add_filter('manage_contest_submission_posts_columns', 'add_contest_submission_posts_columns');

add_action('manage_contest_submission_posts_custom_column', 'show_contest_submission_posts_columns', 10, 2);


function add_contest_submission_posts_columns($defaults) {
    $defaults['featured_image'] = 'Featured Image';
    $defaults['user_id'] = 'User';
    return $defaults;
}


function show_contest_submission_posts_columns($column_name, $post_id) {
    if ($column_name == 'featured_image') {
        $full_size_img =  get_the_post_thumbnail_url($post_id,"full");
        echo get_the_post_thumbnail($post_id, 'thumbnail');
        echo "<br /><a href='$full_size_img' download>Download</a>";
    }

    if ($column_name == 'user_id') {
        global $obj;
        $user_id =  get_post_meta($post_id,"art_user_id",true);
        $user_data = $obj->get_user_data($user_id);
        echo "<h4>User Id : $user_id , ".$user_data['full_name']."</h4>";
    }
}





// ******************************************************************
//                  META BOXES for User Payment
// ******************************************************************

/**
 * User Payment Meta Box
 */
function gp_add_user_payment_metaboxes( $post ) {

    add_meta_box(
        'user-payment',
        'User Payments Fields',
        'gp_user_payment_meta_box',
        'user_payment',
        'normal',
        'low'
    );

}



/**
 * Create the metabox data
 */
function gp_user_payment_meta_box()
{
    global $post;
    global $obj;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'user_payment_fields');

    $post_id = $post->ID;
    $payment_user_id = trim(get_post_meta( $post_id, 'payment_user_id', true ));
    if($payment_user_id){
        $user_info = get_userdata($payment_user_id);
        $user_name = $user_info->first_name." ".$user_info->last_name;
    }else{
        $user_info = "";
        $user_name = "";
    }

    $payment_contest_id = get_post_meta( $post_id, 'payment_contest_id', true );
    if($payment_contest_id){ $term_array = get_term($payment_contest_id); $contest_name = $term_array->name;  } else{ $contest_name = "";  }

    $payment_type = trim(get_post_meta( $post_id, 'payment_type', true ));
    $payment_price = trim(get_post_meta( $post_id, 'payment_price', true ));
    $payment_tax = trim(get_post_meta( $post_id, 'payment_tax', true ));
    $upgrade_membership_time = trim(get_post_meta( $post_id, 'upgrade_membership_time', true ));

    $membership_end_date = trim(get_post_meta( $post_id, 'membership_end_date', true ));
    if($membership_end_date) { $membership_end_date = date("Y-M-d", strtotime($membership_end_date) ); }
    else{ $membership_end_date = ""; }


    $credit_package = trim(get_post_meta( $post_id, 'credit_package', true ));

    $payment_stripe_id = trim(get_post_meta( $post_id, 'payment_stripe_id', true ));
    $payment_stripe_customer = trim(get_post_meta( $post_id, 'payment_stripe_customer', true ));
    $payment_stripe_balance_transaction = trim(get_post_meta( $post_id, 'payment_stripe_balance_transaction', true ));
    $payment_stripe_currency = trim(get_post_meta( $post_id, 'payment_stripe_currency', true ));
    $payment_stripe_receipt_url = trim(get_post_meta( $post_id, 'payment_stripe_receipt_url', true ));
    $payment_description = trim(get_post_meta( $post_id, 'payment_description', true ));

//    $total_price = $obj->get_total_price_by_tax($payment_price,$payment_tax);

    $payment_total_price = trim(get_post_meta( $post_id, 'payment_total_price', true ));
    $payment_coupon_applied = trim(get_post_meta( $post_id, 'payment_coupon_applied', true ));
    $payment_coupon_code = trim(get_post_meta( $post_id, 'payment_coupon_code', true ));
    $payment_coupon_percentage = trim(get_post_meta( $post_id, 'payment_coupon_percentage', true ));
    $payment_coupon_discount = trim(get_post_meta( $post_id, 'payment_coupon_discount', true ));
    $payment_final_price = trim(get_post_meta( $post_id, 'payment_final_price', true ));
?>

    <table border="0" class="table-page-user-payment">

        <tr>
            <th>Contest Id</th>
            <td><input type="text" name="payment_contest_id" id="payment_contest_id" value="<?php echo $payment_contest_id; ?>" ><?php  echo $contest_name; ?>
            </td>
        </tr>

        <tr>
            <th>User ID </th>
            <td>
                <input type="text" name="payment_user_id" id="payment_user_id" value="<?php echo $payment_user_id; ?>" >
                <a href="<?php echo admin_url()."/user-edit.php?user_id=".$payment_user_id; ?>">
                    <b style="color: #0088ff"><?php echo $user_name; ?></b>
                </a>
            </td>
        </tr>

        <tr>
            <th>Payment Type</th>
            <td><input type="text" name="payment_type" id="payment_type" value="<?php echo $payment_type; ?>" ></td>
        </tr>

        <tr>
            <th>Membership Type</th>
            <td><input type="text" name="upgrade_membership_time" id="upgrade_membership_time" value="<?php echo $upgrade_membership_time; ?>" ></td>
        </tr>

        <tr>
            <th>Membership End Date</th>
            <td><b><?php echo $membership_end_date;  ?></b></td>
        </tr>

        <tr>
            <th>Credit Package</th>
            <td><input type="number" name="credit_package" id="credit_package" value="<?php echo $credit_package; ?>" ></td>
        </tr>

        <tr>
            <th>Price</th>
            <td><?php echo GP_CURRENCY_SYMBOL; ?><input type="text" name="payment_price" id="payment_price" value="<?php echo $payment_price; ?>" ></td>
        </tr>

        <tr>
            <th>Tax</th>
            <td><input type="text" name="payment_tax" id="payment_tax" value="<?php echo $payment_tax; ?>" >%</td>
        </tr>

        <tr>
            <th>Total Price</th>
            <td><?php echo GP_CURRENCY_SYMBOL; ?><span><b><?php echo $payment_total_price;  ?></b></span></td>
        </tr>


        <tr>
            <th>Coupon Applied?</th>
            <td><b><?php echo $payment_coupon_applied;  ?></b></span></td>
        </tr>

        <tr>
            <th>Coupon Code</th>
            <td><b><?php echo $payment_coupon_code;  ?></b></span></td>
        </tr>

        <tr>
            <th>Coupon Percentage</th>
            <td><b><?php echo $payment_coupon_percentage;  ?>%</b></span></td>
        </tr>

        <tr>
            <th>Coupon Discount</th>
            <td><?php echo GP_CURRENCY_SYMBOL; ?><span><b><?php echo $payment_coupon_discount;  ?></b></span></td>
        </tr>

        <tr>
            <th>Final Price</th>
            <td><?php echo GP_CURRENCY_SYMBOL; ?><span><b><?php echo $payment_final_price;  ?></b></span></td>
        </tr>




        <tr>
            <th colspan="2"><h1>Stripe Data</h1></th>
        </tr>

        <tr>
            <th>Stripe ID</th>
            <td><b><?php echo $payment_stripe_id;  ?></b></td>
        </tr>

        <tr>
            <th>Stripe Customer</th>
            <td><b><?php echo $payment_stripe_customer;  ?></b></td>
        </tr>

        <tr>
            <th>Balance Transaction</th>
            <td><b><?php echo $payment_stripe_balance_transaction;  ?></b></td>
        </tr>

        <tr>
            <th>Currency</th>
            <td><b><?php echo $payment_stripe_currency;  ?></b></td>
        </tr>

        <tr>
            <th>Stripe Receipt</th>
            <td><b><a href="<?php echo $payment_stripe_receipt_url;  ?>" target="_blank">Receipt</a></b></td>
        </tr>

        <tr>
            <th>Stripe Description</th>
            <td><b><?php echo $payment_description;  ?></b></td>
        </tr>

    </table>

<?php
}






/**
 * Save the metabox data
 * *****Not Used**** ( We are not saving this data from Admin anymore)
 */
function gp_save_user_payment_meta( $post_id, $post ) {

    // Return if the user doesn't have edit permissions.
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
    }

    // because save_post can be triggered at other times.
    if ( !isset( $_POST['payment_type'] ) || ! wp_verify_nonce( $_POST['user_payment_fields'], basename(__FILE__) ) ) {
        return $post_id;
    }

    update_post_meta( $post_id, "payment_user_id", trim($_POST['payment_user_id']) );
    update_post_meta( $post_id, "payment_contest_id", trim($_POST['payment_contest_id']) );
    update_post_meta( $post_id, "payment_type", trim($_POST['payment_type']) );
    update_post_meta( $post_id, "payment_price", trim($_POST['payment_price']) );
    update_post_meta( $post_id, "payment_tax", trim($_POST['payment_tax']) );
    update_post_meta( $post_id, "upgrade_membership_time", trim($_POST['upgrade_membership_time']) );
    update_post_meta( $post_id, "credit_package", trim($_POST['credit_package']) );

}
//add_action( 'save_post', 'gp_save_user_payment_meta', 1, 2 );






/**
 * Add Column to User Payment Post type  List Admin.
 */
add_filter('manage_user_payment_posts_columns', 'add_user_payment_column');

add_action('manage_user_payment_posts_custom_column', 'show_user_payment_column_values', 10, 2);

function add_user_payment_column($defaults) {
    $defaults['payment_type'] = 'Payment Type';
    $defaults['payment_price'] = 'Price';
    $defaults['payment_tax'] = 'Tax';
    $defaults['payment_user_id'] = 'User Id';

    return $defaults;
}


function show_user_payment_column_values($column_name, $post_id) {

    if ($column_name == 'payment_type') {
        $payment_type = get_post_meta( $post_id, "payment_type", true);
        if($payment_type == "credit") {  echo "Credit"; }
        else if($payment_type == "upgrade_membership"){ echo "Membership Upgrade"; }
    }

    if ($column_name == 'payment_price') {
        echo GP_CURRENCY_SYMBOL.get_post_meta( $post_id, "payment_price", true);
    }
    if ($column_name == 'payment_tax') {
        echo get_post_meta( $post_id, "payment_tax", true)."%";
    }

    if ($column_name == 'payment_user_id') {
        echo get_post_meta( $post_id, "payment_user_id", true);
    }
}






// ***********************************************************************
//                  Show featured image in custom post type list
// ***********************************************************************

function add_featured_image_column($defaults) {
    $defaults['featured_image'] = 'Featured Image';
    return $defaults;
}


function show_featured_image_column($column_name, $post_id) {
    if ($column_name == 'featured_image') {
        $full_size_img =  get_the_post_thumbnail_url($post_id,"full");
        echo get_the_post_thumbnail($post_id, 'thumbnail');
        echo "<br /><a href='$full_size_img' download>Download</a>";
    }
}






















// ***********************************************************
//                  META BOXES for Contest Submissions
// ***********************************************************


/**
 *  Contest Submissions Meta Box
 */
function gp_add_coupon_code_metaboxes( $post ) {

    add_meta_box(
        'coupon-code-id',
        'Coupon Code Fields',
        'gp_coupon_code_meta_box',
        'gp_coupon_code',
        'normal',
        'low'
    );

}


function gp_coupon_code_meta_box(){
    global $post;
    // Nonce field to validate form request came from current site
    wp_nonce_field(basename(__FILE__), 'coupon_code_nonce_fields');

    $post_id = $post->ID;
    $coupon_percentage = get_post_meta( $post_id, 'coupon-percentage', true );
    $coupon_end_date = trim(get_post_meta( $post_id, 'coupon-end-date', true ));
    $coupon_type = trim(get_post_meta( $post_id, 'coupon-type', true ));

    ?>
    <table border="0" class="table-page-user-art">
        <tr>
            <th>Coupon Type</th>
            <td>
                <select name="coupon-type" id="coupon-type">
                    <option value="membership-coupon" <?php if($coupon_type == "membership-coupon") { echo "selected"; } ?>>Membership Coupon</option>
                    <option value="credit-coupon" <?php if($coupon_type == "credit-coupon") { echo "selected"; } ?>>Credit Coupon</option>
                </select>
            </td>
        </tr>

        <tr>
            <th>Percentage(%) Off</th>
            <td><input type="text" name="coupon-percentage" id="coupon-percentage" value="<?php echo $coupon_percentage; ?>" ></td>
        </tr>

        <tr>
            <th>End Date</th>
            <td><input type="text" name="coupon-end-date" id="coupon-end-date" value="<?php echo $coupon_end_date; ?>" ></td>
        </tr>

    </table>
    <?php
}






/**
 * Save the metabox data
 */
function gp_save_coupon_code_meta( $post_id, $post ) {

    // Return if the user doesn't have edit permissions.
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
    }

    // Verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times.
    if ( !isset( $_POST['coupon-percentage'] ) || ! wp_verify_nonce( $_POST['coupon_code_nonce_fields'], basename(__FILE__) ) ) {
        return $post_id;
    }

    update_post_meta( $post_id, "coupon-percentage", esc_attr($_POST['coupon-percentage']) );
    update_post_meta( $post_id, "coupon-end-date", esc_attr($_POST['coupon-end-date']) );
    update_post_meta( $post_id, "coupon-type", esc_attr($_POST['coupon-type']) );

}
add_action( 'save_post', 'gp_save_coupon_code_meta', 1, 2 );



// ##########   If Coupon Code is duplicate change Title & save it as a draft   ##############
add_filter( 'wp_insert_post_data' , 'filter_post_data' , '99', 2 );
function filter_post_data( $data , $postarr ) {

//    echo '<pre>' . print_r( $data, true ) . '</pre>';
//    echo '<pre>' . print_r( $postarr, true ) . '</pre>';

    if($data['post_type'] == "gp_coupon_code" && $data['post_status'] != "auto-draft"){
        global $wpdb;
        $query =  "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $data['post_title'] . "' AND ID <> ".$postarr['ID'];
        $find_postid = $wpdb->get_var( $query);

        // Check if Coupon Code already exist
        if($find_postid != "") {
            $data['post_title'] .= '_copy';
            $data['post_status'] = "draft";
        }

        if(strlen($data['post_title']) <= 4){
            $data['post_title'] .= ' (Must Be Atlest 5 Character)';
            $data['post_status'] = "draft";
        }

        //        Save Coupon code as a draft if Percentage or End date is not Entered
        if((isset($postarr['coupon-percentage']) &&  trim($postarr['coupon-percentage']) == "" ) ||  (isset($postarr['coupon-percentage']) &&  trim($postarr['coupon-end-date']) == "")){
            $data['post_status'] = "draft";
        }

    }
    return $data;
//    exit;

}







/**
 * Disabling "User Payment" Post type Delete Action
 * Removes the "Trash" link on the individual "User Payment" Post Type
 * edit page.
 */

//add_filter( 'post_row_actions', 'remove_row_actions_post', 10, 2 );
//function remove_row_actions_post( $actions, $post ) {
//    if( $post->post_type === 'user_payment' ) {
//        unset( $actions['clone'] );
//        unset( $actions['trash'] );
//    }
//    return $actions;
//}
//
//add_action('wp_trash_post', 'restrict_post_deletion');
//function restrict_post_deletion($post_id) {
//    if( get_post_type($post_id) === 'user_payment' ) {
//        wp_die('The Payment Post you were trying to delete is protected.');
//    }
//}