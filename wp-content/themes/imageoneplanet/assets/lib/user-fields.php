<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) {

    $user_id = $user->ID;
    $headshot_id = esc_attr( get_the_author_meta( 'headshot', $user_id) );
    $headshot_image = "";
    if($headshot_id != ""){
        $headshot_image = current(wp_get_attachment_image_src( $headshot_id, 'thumbnail'));
    }
    if($headshot_image == ""){ $headshot_image = get_image_not_found(); }
    ?>
    <h3><?php _e("Extra profile information", "blank"); ?></h3>

    <table class="form-table">


        <tr class="form-field contest-extra-fields">
            <th><label for="headshot"><?php _e("Head Shot"); ?></label></th>

            <td>
                <input class="headshot_image_url" name="headshot_image_url" type="text" value=""/>
                <input class="headshot_image_id" name="headshot_image_id" type="hidden" value="<?php echo $headshot_id; ?>"/>
                <input  class="upload_image_button" type="button" value="Upload Image"
                        data-upload-image-class="headshot_image_url"
                        data-upload-image-src="headshot_image"
                        data-upload-image-id="headshot_image_id"/>
                <br />
                <span class="description">Please upload your image for your profile.</span><br />
                <div class="headshot-img"><img src="<?php echo $headshot_image; ?>" class="headshot_image" alt=""></div>
            </td>

        </tr>


        <tr>
            <th><label for="usertype"><?php _e("User Type"); ?></label></th>
            <td>
                Artist <input type="radio" name="usertype" value="artist" <?php  if(esc_attr( get_the_author_meta( 'usertype', $user->ID ) ) == "artist") { echo "checked"; } ?> class="required"> <br />
                Photographer <input type="radio" name="usertype" value="photographer" <?php  if(esc_attr( get_the_author_meta( 'usertype', $user->ID ) ) == "photographer") { echo "checked"; } ?> > <br />
            </td>
        </tr>

        <tr>
            <th><label for="country"><?php _e("Country"); ?></label></th>
            <td>
                <input type="text" name="country" id="country" value="<?php echo esc_attr( get_the_author_meta( 'country', $user->ID ) ); ?>"  class="regular-text required" /><br />
                <span class="description"><?php _e("Please enter your Country."); ?></span>
            </td>
        </tr>


        <tr>
            <th><label for="gallery_name"><?php _e("Gallery Name"); ?></label></th>
            <td>
                <input type="text" name="gallery_name" id="gallery_name" value="<?php echo esc_attr( get_the_author_meta( 'gallery_name', $user->ID ) ); ?>"  class="regular-text required" /><br />

            </td>
        </tr>

        <tr>
            <th><label for="website-link"><?php _e("Website Link"); ?></label></th>
            <td>
                <input type="text" name="website-link" id="website-link" value="<?php echo esc_attr( get_the_author_meta( 'website-link', $user->ID ) ); ?>"  class="regular-text " /><br />
                <span class="description"><?php _e("Please enter your Website Link."); ?></span>
            </td>
        </tr>


        <tr>
            <th><label for="facebook-link"><?php _e("Facebook Link"); ?></label></th>
            <td>
                <input type="text" name="facebook-link" id="facebook-link" value="<?php echo esc_attr( get_the_author_meta( 'facebook-link', $user->ID ) ); ?>"  class="regular-text " /><br />
                <span class="description"><?php _e("Please enter your Facebook Link."); ?></span>
            </td>
        </tr>

        <tr>
            <th><label for="instagram-link"><?php _e("Instagram Link"); ?></label></th>
            <td>
                <input type="text" name="instagram-link" id="instagram-link" value="<?php echo esc_attr( get_the_author_meta( 'instagram-link', $user->ID ) ); ?>"  class="regular-text " /><br />
                <span class="description"><?php _e("Please enter your Instagram Link."); ?></span>
            </td>
        </tr>



        <tr class="form-field contest-extra-fields">
            <th><label for="birth-date"><?php _e("Year of Birth"); ?></label></th>
            <td>
                <input type="text" name="birth-date" id="birth-date" value="<?php echo esc_attr( get_the_author_meta( 'birth-date', $user->ID ) ); ?>" />
            </td>
        </tr>


        <tr>
            <th><label for="featured-artist"><?php _e("Featured"); ?></label></th>
            <td>
                Yes <input type="radio" name="featured"  value="yes" <?php  if(esc_attr( get_the_author_meta( 'featured', $user->ID ) ) == "yes") { echo "checked"; } ?> class="required"> <br />
                No <input type="radio" name="featured"   value="no" <?php  if(esc_attr( get_the_author_meta( 'featured', $user->ID ) ) == "no") { echo "checked"; } ?> > <br />
            </td>
        </tr>


        <tr>
            <th><label for="artist-category"><?php _e("Artist Category"); ?></label></th>
            <td>
                <?php
                $get_artist_cats = get_the_author_meta( 'artist-category', $user->ID );
                $explode_artist_cats = array_filter(explode(",",$get_artist_cats));
                $artist_categories = $terms = get_terms( 'artist_category', array('hide_empty' => false,) );
                foreach ($artist_categories as $artist_cat) {
                    $a_cat_id = $artist_cat->term_id;
                    $a_cat_name = $artist_cat->name;
                    ?>
                    <label for="">
                        <input type="checkbox" name="artist-category[]" value="<?php echo $a_cat_id; ?>" <?php if(in_array($a_cat_id, $explode_artist_cats)){ echo "checked"; } ?> ><?php echo $a_cat_name; ?>
                    </label>
                    <br>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <th><label for="photographer-category"><?php _e("Photographer Category"); ?></label></th>
            <td>
                <?php
                    $get_photographer_cats = get_the_author_meta( 'photographer-category', $user->ID );
                    $explode_photographer_cats = array_filter(explode(",",$get_photographer_cats));
                    $photographer_categories = $terms = get_terms( 'photographer_category', array('hide_empty' => false,) );
                    foreach ($photographer_categories as $photographer_cat) {
                        $p_cat_id = $photographer_cat->term_id;
                        $p_cat_name = $photographer_cat->name;
                 ?>
                    <label for="">
                        <input type="checkbox" name="photographer-category[]" value="<?php echo $p_cat_id; ?>" <?php if(in_array($p_cat_id, $explode_photographer_cats)){ echo "checked"; } ?> ><?php echo $p_cat_name; ?>
                    </label>
                    <br>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <th><h1 ><?php _e("User Images"); ?></h1></th>
            <td>
            </td>
        </tr>

        <tr>
            <?php
            $regular_meta_array = array("participant_user_id" => $user->ID);
            $regular_images =  gp_get_post_by_multiple_meta("user_profile_gallery",$regular_meta_array,-1);
            ?>
            <th><label ><?php _e("Gallery Images"); ?></label></th>
            <td>
                <?php
                foreach ($regular_images as $regular_image){
                    $thumbnail_img = current(wp_get_attachment_image_src( get_post_thumbnail_id($regular_image), 'thumbnail' ));
                    ?>
                    <img src="<?php echo $thumbnail_img; ?>" alt="" style="margin: 5px; max-width: 75px" >
                <?php } ?>
            </td>
        </tr>




        <tr>
            <th><label for="cron-ignore"><?php _e("Ignore Cron(If Selected yes, user account wont be automatically degraded)"); ?></label></th>
            <td>
                <select name="cron-ignore" id="cron-ignore">
                    <option value="no" <?php  if(esc_attr( get_the_author_meta( 'cron-ignore', $user->ID ) ) == "no") { echo "selected"; } ?>>No</option>
                    <option value="yes" <?php  if(esc_attr( get_the_author_meta( 'cron-ignore', $user->ID ) ) == "yes") { echo "selected"; } ?>>Yes</option>
                </select>
            </td>
        </tr>

        

<!--        <tr>-->
<!--            --><?php
//            $regular_meta_array = array("participant_user_id" => $user->ID, "image-type" => "featured_image");
//            $regular_images =  gp_get_post_by_multiple_meta("user_profile_gallery",$regular_meta_array,-1);
//            ?>
<!--            <th><label >--><?php //_e("Featured Images"); ?><!--</label></th>-->
<!--            <td>-->
<!--                --><?php
//                foreach ($regular_images as $regular_image){
//                    $thumbnail_img = current(wp_get_attachment_image_src( get_post_thumbnail_id($regular_image), 'thumbnail' ));
//                    ?>
<!--                    <img src="--><?php //echo $thumbnail_img; ?><!--" alt="" style="margin: 5px; max-width: 75px" >-->
<!--                --><?php //} ?>
<!--            </td>-->
<!--        </tr>-->

    </table>


    <script>
        jQuery(document).ready( function( $ ) {
//            $('#birth-date').datepicker({
//                // dateFormat : 'dd/m - yy'
//            });
        });
    </script>
<?php }











add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) {
        return false;
    }

    if(isset($_POST['usertype'])){
        update_user_meta( $user_id, 'usertype', $_POST['usertype'] );
    }

    update_user_meta( $user_id, 'country', $_POST['country'] );
    update_user_meta( $user_id, 'gallery_name', $_POST['gallery_name'] );
    update_user_meta( $user_id, 'website-link', $_POST['website-link'] );
    update_user_meta( $user_id, 'facebook-link', $_POST['facebook-link'] );
    update_user_meta( $user_id, 'instagram-link', $_POST['instagram-link'] );
    update_user_meta( $user_id, 'birth-date', $_POST['birth-date'] );

    
    
    if(isset($_POST['featured'])){
        update_user_meta( $user_id, 'featured', $_POST['featured'] );
    }

    $implode_artist_category = "";
    if(isset($_POST['artist-category'])){
        $implode_artist_category = ",".implode(",",$_POST['artist-category']).",";
    }
    update_user_meta( $user_id, 'artist-category', $implode_artist_category );

    $implode_photographer_category = "";
    if(isset($_POST['photographer-category'])){
        $implode_photographer_category = ",".implode(",",$_POST['photographer-category']).",";
    }
    update_user_meta( $user_id, 'photographer-category', $implode_photographer_category );


    update_user_meta( $user_id, 'cron-ignore', $_POST['cron-ignore'] );


    update_user_meta( $user_id, 'headshot', $_POST['headshot_image_id'] );

    global $email_template;
    $email_template->user_id = $user_id;
    $email_template->email_success_profile_updated();
}








/**
 * Add Column to Users  - Usertype / Featured
 */

function new_modify_user_table( $column ) {
    $column['usertype'] = 'User Type';
    $column['is_featured'] = 'Is Featured?';
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );

function new_modify_user_table_row( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'usertype' :
            return get_the_author_meta( 'usertype', $user_id );
            break;
        case 'is_featured' :
            $is_featured = get_the_author_meta( 'featured', $user_id );
            if($is_featured == "yes") {   return "<span class='user-is-featured user-featured'>Yes</span>" ;  }
            else{ return "<span class='user-is-featured'>No</span>" ;  }
            break;
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );




