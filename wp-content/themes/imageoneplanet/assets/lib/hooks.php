<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



//************************************************************
//  Disable Auto Update
//************************************************************
add_filter( 'auto_update_plugin', '__return_false' );




//************************************************************
//  Add New Roles / Remove Default roles
//************************************************************
function gp_new_role() {
    add_role('participant','Participant');
    add_role('jury','Jury');

    global $wp_roles;
    $wp_roles->remove_role("subscriber");
    $wp_roles->remove_role("contributor");
    $wp_roles->remove_role("author");
    $wp_roles->remove_role("editor");
}
add_action('admin_init', 'gp_new_role');




//************************************************************
//  Add New Image Size
//************************************************************
add_image_size( 'custom-size-2000', 2000, 2000,false );

// for wookmark gallery layouts
add_image_size('imageone-gallery', 425, 9999, false);






//****************************************************************************************************
//          Login Failed redirect back to Login Frontend - Rather than redirect to Admin
//****************************************************************************************************
add_action( 'wp_login_failed', 'my_front_end_login_fail' );

function my_front_end_login_fail( $username ) {
    $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
    if (strpos($referrer, '?login=failed') !== false) {
        $referrer = substr($referrer, 0, strpos($referrer, "?"));
    }

    // if there's a valid referrer, and it's not the default log-in screen
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
        wp_redirect( $referrer . '?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
        exit;
    }
}





add_action( 'login_form_middle', 'add_lost_password_link' );
function add_lost_password_link() {
    return '<p class="lost-password"><a href="'.site_url().'/lost-password">Lost Password?</a></p>';
}



