<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}





//General Setting User Category Data
add_filter('admin_init', 'my_general_settings_register_fields');
function my_general_settings_register_fields()

{
    //   Payment Tax
    register_setting('general', 'payment_tax_setting', 'esc_attr');
    add_settings_field('payment_tax_setting', '<label for="payment_tax_setting">'.__('Tax' , 'payment_tax_setting' ).'</label>' , 'payment_tax_settings_fields_html', 'general');



//    //    Stripe Publishable Key Settings
//    register_setting('general', 'stripe_publishable_key', 'esc_attr');
//    add_settings_field('stripe_publishable_key', '<label for="stripe_publishable_key">'.__('Stripe Publishable key' , 'stripe_publishable_key' ).'</label>' , 'stripe_publishable_key_settings_fields_html', 'general');
//
//
//    //    Stripe Secret Key Settings
//    register_setting('general', 'stripe_secret_key', 'esc_attr');
//    add_settings_field('stripe_secret_key', '<label for="stripe_secret_key">'.__('Stripe Secret key' , 'stripe_secret_key' ).'</label>' , 'stripe_secret_key_settings_fields_html', 'general');

}


function payment_tax_settings_fields_html()
{
    $payment_tax_setting = get_option( 'payment_tax_setting', '' );
    echo '<input class="payment_tax_setting" type="number" id="payment_tax_setting" name="payment_tax_setting" value="' . $payment_tax_setting . '" />';
}


function stripe_publishable_key_settings_fields_html()
{
    $stripe_publishable_key = get_option( 'stripe_publishable_key', '' );
    echo '<input class="stripe_publishable_key regular-text" type="text" id="stripe_publishable_key" name="stripe_publishable_key" value="' . $stripe_publishable_key . '" />';
}


function stripe_secret_key_settings_fields_html()
{
    $stripe_secret_key = get_option( 'stripe_secret_key', '' );
    echo '<input class="stripe_secret_key regular-text" type="text" id="stripe_secret_key" name="stripe_secret_key" value="' . $stripe_secret_key . '" />';
}
