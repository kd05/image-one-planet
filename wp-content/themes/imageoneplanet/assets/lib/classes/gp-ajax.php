<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



class GP_Ajax{

    /**
     * GP_Ajax constructor.
     */
    public function __construct() {

        // Initialize all ajax requests

        add_action( 'wp_ajax_add_user_art', array( $this, 'add_user_art' ) );

        add_action( 'wp_ajax_edit_user_art', array( $this, 'edit_user_art' ) );

        add_action( 'wp_ajax_edit_profile', array( $this, 'edit_profile' ) );

        add_action( 'wp_ajax_delete_user_art', array( $this, 'delete_user_art' ) );

        add_action( 'wp_ajax_submit_art_to_contest', array( $this, 'submit_art_to_contest' ) );

        add_action( 'wp_ajax_nopriv_create_account', array( $this, 'create_account' ) );

        add_action( 'wp_ajax_buy_credit_or_upgrade_membership', array( $this, 'buy_credit_or_upgrade_membership' ) );


        add_action( 'wp_ajax_gallery_interest', array( $this, 'gallery_interest' ) );
        add_action( 'wp_ajax_nopriv_gallery_interest', array( $this, 'gallery_interest' ) );


        add_action( 'wp_ajax_delete_user_prof_gallery', array( $this, 'delete_user_prof_gallery' ) );


        add_action( 'wp_ajax_add_prof_gallery', array( $this, 'add_prof_gallery' ) );

        add_action( 'wp_ajax_edit_prof_gallery', array( $this, 'edit_prof_gallery' ) );

        add_action( 'wp_ajax_update_featured_image', array( $this, 'update_featured_image' ) );

        add_action( 'wp_ajax_update_listing_gallery_image', array( $this, 'update_listing_gallery_image' ) );

        add_action( 'wp_ajax_contact_form', array( $this, 'contact_form' ) );


        add_action( 'wp_ajax_home_page_featured_user', array( $this, 'home_page_featured_user' ) );
        add_action( 'wp_ajax_nopriv_home_page_featured_user', array( $this, 'home_page_featured_user' ) );


        add_action( 'wp_ajax_show_coupon_code_detail', array( $this, 'show_coupon_code_detail' ) );
        add_action( 'wp_ajax_nopriv_show_coupon_code_detail', array( $this, 'show_coupon_code_detail' ) );

    }



    /*********************************************************************
     * Home Page Show Featured User (Refresh) every X seconds in javascript
     ********************************************************************/

    function home_page_featured_user(){
        global $obj;

        $feat_artist_args = array('meta_query'=>
            array( 'relation' => 'AND',
                array('key' => 'usertype','value' => 'artist', 'compare' => "=",),
                array('key' => 'hide_on_home','value' => 'no', 'compare' => "=",),
                array('key' => 'featured','value' => 'yes','compare' => "=",)));
        $feat_artist_users = get_users( $feat_artist_args );
        echo $obj->home_page_featured_user_section($feat_artist_users,"Artist");

        $feat_photographer_args = array('meta_query'=>
            array( 'relation' => 'AND',
                array('key' => 'usertype','value' => 'photographer', 'compare' => "=",),
                array('key' => 'hide_on_home','value' => 'no', 'compare' => "=",),
                array('key' => 'featured','value' => 'yes','compare' => "=",)));
        $feat_photographer_users = get_users( $feat_photographer_args );
        echo $obj->home_page_featured_user_section($feat_photographer_users,"Photographer");

        exit;
    }



    /*****************************************************
     * Create an Account
     ****************************************************/
    function create_account()
    {
        check_ajax_referer('security-create-account-nonce', 'security');
        global $obj;
        global $email_template;

        $params = $_POST['data'];
        parse_str($params, $data);
        $username = sanitize_text_field($data['username']);
        $user_email = sanitize_email($data['email']);
        $user_password = sanitize_text_field($data['user_password']);


        if(is_user_logged_in()){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("logout_to_create_account") );
            echo json_encode($response);
            exit;
        }

        if (email_exists($user_email)){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("email_already_exist") );
            echo json_encode($response);
            exit;
        }

        $userdata = array(
            'user_login' => $username,
            'user_pass' => $user_password,
            'user_email' => $user_email,
        );
        $user_id = wp_insert_user($userdata);

        if(is_wp_error($user_id)) {
            $response = array("success" => 0, "msg" => $user_id->get_error_message());
            echo json_encode($response);
            exit;
        }else {
            update_user_meta($user_id,"featured","no");
            update_user_meta( $user_id, 'hide_on_home', "no" );
            $response = array("success" => 1, "msg" => $obj->get_site_messages("success_create_account"));
            echo json_encode($response);
            $email_template->user_id = $user_id;
            $email_template->email_create_account();
            exit;
        }

    }





    /*****************************************************
     * Profile Gallery  Interest/Inquiry Form
     ****************************************************/
    function gallery_interest()
    {
        check_ajax_referer('security-user-gallery-form-nonce', 'security');
        global $obj;
        global $email_template;

        $params = $_POST['data'];
        parse_str($params, $data);

        $gallery_id = $data['gallery-id'];
        $user_id = get_post_meta($gallery_id,"participant_user_id", true);


        $email_template->user_id = $user_id;
        $email_template->email_gallery_image_inquiry($gallery_id,$data);
        exit;

    }


    /**
     * Add Profile gallery Images
     */
    function add_prof_gallery(){

        check_ajax_referer('security-prof-gallery-nonce', 'security');
        global $obj;

        $user_id = get_current_user_id();
        $title = sanitize_text_field($_POST['title']);
        $description = sanitize_textarea_field($_POST['description']);
        $size = sanitize_text_field($_POST['image-size']);
        $frame = sanitize_text_field($_POST['image-frame']);
        $image_type = ( sanitize_text_field($_POST['image-type']) == "featured_image") ? "featured_image" : "regular_image";
        $listing_image_type = ( sanitize_text_field($_POST['listing-image-type']) == "listing_image") ? "listing_image" : "regular_image";
        $year = sanitize_text_field($_POST['year']);
        $status = sanitize_text_field($_POST['status']);
        $price = sanitize_text_field($_POST['price']);
        $image_tag = trim(sanitize_text_field($_POST['tag'])) == "" ? $title : trim(sanitize_text_field($_POST['tag']));
        //   $image_tag = " ";

        if (!function_exists('wp_handle_upload')) {
            require_once(ABSPATH . 'wp-admin/includes/file.php');
            require_once(ABSPATH . 'wp-admin/includes/image.php' );
        }

        $meta_array = array("participant_user_id" => $user_id);
        $profile_galleries = $obj->get_post_by_multiple_meta("user_profile_gallery",$meta_array);
        $total_gallery_images = count($profile_galleries);

        if($total_gallery_images >= GP_MAX_USER_PROFILE_IMAGES) {
            echo "prof_gallery_upload_limit_exceeds";
            die();
        }

        if(isset($_FILES['file']) && $_FILES['file']['error'] == 0)
        {
            //  **********        Check Invalid File Type    ********
            $response = $obj->check_file_type($_FILES['file']['type']);
            if($response['error'] == 1){
                $response =  "file_type_error";
                echo $response;
                die();
            }

            //  **********        Check File Size     ********
            $response = $obj->check_file_size($_FILES['file']['size']);
            if($response['error'] == 1){
                $response =  "file_size_error";
                echo $response;
                die();
            }

            //  **********        Check If Maximum featured images exceeds     ********
            if($image_type == "featured_image" && $obj->get_total_count_featured_images($user_id) >= GP_MAX_USER_PROFILE_FEATURED_IMAGES){
                $response =  "prof_gallery_featured_exceeds";
                echo $response;
                die();
            }

            //  **********        Check If Maximum Listing images exceeds     ********
            if($listing_image_type == "listing_image" && $obj->get_total_count_listing_gallery_images($user_id) >= GP_MAX_USER_PROFILE_LISTING_IMAGES){
                $response =  "prof_gallery_listing_exceeds";
                echo $response;
                die();
            }

            $post = array( //our wp_insert_post args
                'post_title'    => $title,
                'post_content'  => $description,
                'post_status'   => 'publish',
                'post_type' => 'user_profile_gallery',
                'post_excerpt' => $image_tag
            );
            $post_id = wp_insert_post($post);


            update_post_meta( $post_id, "image-size", $size);
            update_post_meta( $post_id, "image-frame", $frame);
            update_post_meta( $post_id, "listing-image-type", $listing_image_type);
            update_post_meta( $post_id, "image-type", $image_type);
            update_post_meta( $post_id, "image-year", $year );
            update_post_meta( $post_id, "image-status", $status );
            update_post_meta( $post_id, "image-price", $price);
//            update_post_meta( $post_id, "image-tag", $image_tag);
            update_post_meta( $post_id, "participant_user_id", $user_id );
            $obj->upload_attachment_file($_FILES['file'],$post_id);
            echo "success";
            die();
        }
        else{
            echo "file_error";
            die();
        }
    }



    /**
     * Edit Profile gallery Images
     */
    function edit_prof_gallery(){

        check_ajax_referer('security-prof-gallery-nonce', 'security');

        global $obj, $loginCheck;
        $params = $_POST['data'];
        parse_str($params, $data);
        $user_id = get_current_user_id();
        $gallery_id = $data['edit_gallery_id'];
        $title = sanitize_text_field($data['image-title']);
        $description = sanitize_textarea_field($data['image-description']);
        $size = sanitize_text_field($data['image-size']);
        $frame = sanitize_text_field($data['image-frame']);
        $type =  (isset($data['image-type']) && $data['image-type'] == "featured_image") ? "featured_image" : "regular_image";
        $listing_image_type = (isset($data['listing-image-type']) && $data['listing-image-type'] == "listing_image") ? "listing_image" : "regular_image";
        $year = sanitize_text_field($data['image-year-taken']);
        $img_status = sanitize_text_field($data['image-status']);
        $price = sanitize_text_field($data['image-price']);
//        $tag = sanitize_text_field($data['image-tag']);
        $tag = trim(sanitize_text_field($data['image-tag'])) == "" ? $title : trim(sanitize_text_field($data['image-tag']));



        $allowed_page_access = $loginCheck->gallery_page_access_to_correct_user($gallery_id);
        //   Checking -  Is Currect User editing the saved Art
        if(!$allowed_page_access) {
            $response = array("success" => 0, "msg" => $obj->get_site_messages("prof_gallery_access_denied"));
            echo json_encode($response);
            exit;
        }

        $get_img_type = get_post_meta($gallery_id, "image-type",true);
        if($get_img_type == "featured_image" && $type == "featured_image" && $obj->get_total_count_featured_images($user_id) > GP_MAX_USER_PROFILE_FEATURED_IMAGES){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("prof_gallery_featured_exceeds"));
            echo json_encode($response);
            exit;
        }

        if($get_img_type == "regular_image" && $type == "featured_image" && $obj->get_total_count_featured_images($user_id) >= GP_MAX_USER_PROFILE_FEATURED_IMAGES){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("prof_gallery_featured_exceeds"));
            echo json_encode($response);
            exit;
        }


        $get_listing_img_type = get_post_meta($gallery_id, "listing-image-type",true);
        if($get_listing_img_type == "listing_image" && $listing_image_type == "listing_image" && $obj->get_total_count_listing_gallery_images($user_id) > GP_MAX_USER_PROFILE_LISTING_IMAGES){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("prof_gallery_listing_exceeds"));
            echo json_encode($response);
            exit;
        }
        if($get_listing_img_type != "listing_image" && $listing_image_type == "listing_image" && $obj->get_total_count_listing_gallery_images($user_id) >= GP_MAX_USER_PROFILE_LISTING_IMAGES){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("prof_gallery_listing_exceeds"));
            echo json_encode($response);
            exit;
        }


        $post = array( //our wp_insert_post args
            'ID'           => $gallery_id,
            'post_title'    => $title,
            'post_content'  => $description,
            'post_type' => 'user_profile_gallery',
            'post_excerpt' => $tag
        );
        wp_update_post($post);

        update_post_meta( $gallery_id, "image-size", $size);
        update_post_meta( $gallery_id, "image-frame", $frame);
        update_post_meta( $gallery_id, "image-type", $type);
        update_post_meta( $gallery_id, "listing-image-type", $listing_image_type);
        update_post_meta( $gallery_id, "image-year", $year );
        update_post_meta( $gallery_id, "image-status", $img_status );
        update_post_meta( $gallery_id, "image-price", $price);
//        update_post_meta( $gallery_id, "image-tag", $tag);

        $response = array("success" => 1, "msg" => $obj->get_site_messages("prof_gallery_edit_success"));
        echo json_encode($response);
        die();
    }


    /**
     * Delete Profile Gallery -
     */
    function delete_user_prof_gallery(){
        global $obj;
        $current_user_id = get_current_user_id();
        if(isset($_POST['delete_id'])){
            $delete_id = $_POST['delete_id'];
            $user_id = get_post_meta($delete_id,"participant_user_id", true);
            if($current_user_id != $user_id){
                $response = array(
                    "success" => 0,
                    "msg"  => $obj->get_site_messages("profile_gallery_invalid_user")
                );
                echo json_encode($response);
                exit;
            }

            if(wp_delete_post($delete_id)){
                $response = array(
                    "success" => 1
                );
                echo json_encode($response);
                exit;
            }
        }
        exit;
    }



    /**
     * Update Profile gallery Featured Image Status
     */
    function update_featured_image(){
        global $obj, $loginCheck;
        $gallery_id = $_POST['gallery_id'];
        $user_id = get_current_user_id();
        $type = get_post_meta($gallery_id,"image-type", true);
        $change_to = ($type == "featured_image") ? "regular_image" : "featured_image";

        $allowed_page_access = $loginCheck->gallery_page_access_to_correct_user($gallery_id);
        //   Checking -  Is Currect User editing the saved Art
        if(!$allowed_page_access) {
            $response = array("success" => 0, "msg" => $obj->get_site_messages("prof_gallery_access_denied"));
            echo json_encode($response);
            exit;
        }

        if($change_to == "featured_image" && $obj->get_total_count_featured_images($user_id) >= GP_MAX_USER_PROFILE_FEATURED_IMAGES){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("prof_gallery_featured_exceeds"));
            echo json_encode($response);
            exit;
        }

        update_post_meta($gallery_id,"image-type",$change_to);
        $response = ($change_to == "featured_image") ?
                        array("success" => 1, "msg" => $obj->get_site_messages("prof_gallery_change_to_featured"), "icon" => "fas") :
                        array("success" => 1, "msg" => $obj->get_site_messages("prof_gallery_change_to_regular"), "icon" => "far");
        echo json_encode($response);
        exit;
    }


    /**
     * Update Profile gallery Listing Image Status
     */
    function update_listing_gallery_image(){
        global $obj, $loginCheck;
        $gallery_id = $_POST['gallery_id'];
        $user_id = get_current_user_id();
        $type = get_post_meta($gallery_id,"listing-image-type", true);
        $change_to = ($type == "listing_image") ? "regular_image" : "listing_image";

        $allowed_page_access = $loginCheck->gallery_page_access_to_correct_user($gallery_id);
        //   Checking -  Is Currect User editing the saved Art
        if(!$allowed_page_access) {
            $response = array("success" => 0, "msg" => $obj->get_site_messages("prof_gallery_access_denied"));
            echo json_encode($response);
            exit;
        }

        if($change_to == "listing_image" && $obj->get_total_count_listing_gallery_images($user_id) >= GP_MAX_USER_PROFILE_LISTING_IMAGES){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("prof_gallery_listing_exceeds"));
            echo json_encode($response);
            exit;
        }

        update_post_meta($gallery_id,"listing-image-type",$change_to);
        $response = ($change_to == "listing_image") ?
            array("success" => 1, "msg" => $obj->get_site_messages("prof_gallery_change_to_listing"), "icon" => "fas") :
            array("success" => 1, "msg" => $obj->get_site_messages("prof_gallery_change_to_regular_listing"), "icon" => "far");
        echo json_encode($response);
        exit;
    }

    /*****************************************************
     * Edit Profile
     ****************************************************/



    /**
     * Edit Profile
     */

    function edit_profile(){

        check_ajax_referer('security-edit-profile-nonce', 'security');
        global $obj;
        global $email_template;

        $user_id = get_current_user_id();
        $headshot_id = (isset($_POST['headshot_id']) && $_POST['headshot_id'] != "")? $_POST['headshot_id'] : "";

        if (!function_exists('wp_handle_upload')) {
            require_once(ABSPATH . 'wp-admin/includes/file.php');
            require_once(ABSPATH . 'wp-admin/includes/image.php' );
        }

        //        If Profile HeadShot is not set THAN Check for the file
        if($headshot_id == "" && !isset($_FILES['file'])){
            $response =  "select_file";
            echo $response;
            die();
        }
        elseif( isset($_FILES['file']) && $_FILES['file']['error'] == 0) {
            //  **********        Check Invalid File Type    ********
            $response = $obj->check_file_type($_FILES['file']['type']);
            if($response['error'] == 1){
                //                $response =  json_encode($response);
                $response =  "file_type_error";
                echo $response;
                die();
            }
            //  **********        Check File Size     ********
            $response = $obj->check_file_size($_FILES['file']['size']);
            if($response['error'] == 1){
                //                $response =  json_encode($response);
                $response =  "file_size_error";
                echo $response;
                die();
            }
            $headshot_id = $obj->upload_attachment_file($_FILES['file']);
        }


        $first_name = sanitize_text_field($_POST['first-name']);
        $last_name = sanitize_text_field($_POST['last-name']);
        $usertype = isset($_POST['usertype']) ? sanitize_text_field($_POST['usertype']) : "";
        $artist_category = isset($_POST['artist-category']) ? ",".implode(",",$_POST['artist-category'])."," : "";
        $photographer_category = isset($_POST['photographer-category']) ? ",".implode(",",$_POST['photographer-category'])."," : "";
        $description = sanitize_textarea_field($_POST['image-description']);
        $email_address = sanitize_text_field($_POST['email-address']);
        $country = sanitize_text_field($_POST['country']);
        $gallery_name = sanitize_text_field($_POST['gallery_name']);
        $website_link = sanitize_text_field($_POST['website-link']);
        $facebook_link = sanitize_text_field($_POST['facebook-link']);
        $instagram_link = sanitize_text_field($_POST['instagram-link']);
        $birth_date = sanitize_text_field($_POST['birth-date']);

        update_user_meta( $user_id, 'first_name', $first_name);
        update_user_meta( $user_id, 'last_name', $last_name);
        update_user_meta( $user_id, 'usertype', $usertype);
        update_user_meta( $user_id, 'artist-category', $artist_category );
        update_user_meta( $user_id, 'photographer-category', $photographer_category );
        update_user_meta( $user_id, 'description', $description);
        update_user_meta( $user_id, 'headshot', $headshot_id);
        update_user_meta( $user_id, 'country', $country );
        update_user_meta( $user_id, 'gallery_name', $gallery_name );
        update_user_meta( $user_id, 'website-link', $website_link );
        update_user_meta( $user_id, 'facebook-link', $facebook_link );
        update_user_meta( $user_id, 'instagram-link', $instagram_link);
        update_user_meta( $user_id, 'birth-date', $birth_date);

        //        Send mail
        $email_template->user_id = $user_id;
        $email_template->email_success_profile_updated();

        $response =  "success";
        echo $response;
        die();
    }



    /*****************************************************
     * User Art
     ****************************************************/



    /**
     * Add User Art
     */
    function add_user_art(){

        check_ajax_referer('security-user-art-nonce', 'security');
        global $obj;

        if (!function_exists('wp_handle_upload')) {
            require_once(ABSPATH . 'wp-admin/includes/file.php');
            require_once(ABSPATH . 'wp-admin/includes/image.php' );
        }

        $user_arts = $obj->get_user_arts();
        $total_arts = count($user_arts);
        if($total_arts >= GP_MAX_USER_ARTS) {
            echo "art_upload_limit_exceeds";
            die();
        }

        if(isset($_FILES['file']) && $_FILES['file']['error'] == 0)
        {
            //  **********        Check Invalid File Type    ********
            $response = $obj->check_file_type($_FILES['file']['type']);
            if($response['error'] == 1){
                //                $response =  json_encode($response);
                $response =  "file_type_error";
                echo $response;
                die();
            }

            //  **********        Check File Size     ********
            $response = $obj->check_file_size($_FILES['file']['size']);
            if($response['error'] == 1){
                //                $response =  json_encode($response);
                $response =  "file_size_error";
                echo $response;
                die();
            }

            $title = sanitize_text_field($_POST['title']);
            $description = sanitize_textarea_field($_POST['description']);
            $year = sanitize_text_field($_POST['year']);
            $post = array( //our wp_insert_post args
                'post_title'    => $title,
                'post_content'  => $description,
                'post_status'   => 'publish',
                'post_type' => 'user_art'
            );
            $post_id = wp_insert_post($post);

            update_post_meta( $post_id, "image-year-taken", wp_strip_all_tags($year));
            update_post_meta( $post_id, "art_user_id", get_current_user_id() );
            $obj->upload_attachment_file($_FILES['file'],$post_id);
            echo "success";
            die();
        }
        else{
            echo "file_error";
            die();
        }
    }



    /**
     * Edit User Art
     */
    function edit_user_art(){

        check_ajax_referer('security-user-art-nonce', 'security');

        global $obj, $loginCheck;
        $params = $_POST['data'];
        parse_str($params, $data);
        $art_id = $data['edit_art_id'];
        $art_title = sanitize_text_field($data['image-title']);
        $art_description = sanitize_textarea_field($data['image-description']);
        $art_year = sanitize_text_field($data['image-year-taken']);

        $allowed_page_access = $loginCheck->art_page_access_to_correct_user($art_id);
        //   Checking -  Is Currect User editing the saved Art
        if(!$allowed_page_access) {
            $response = array("success" => 0, "msg" => $obj->get_site_messages("art_access_denied"));
            echo json_encode($response);
            exit;
        }

        $post = array( //our wp_insert_post args
            'ID'           => $art_id,
            'post_title'    => $art_title,
            'post_content'  => $art_description,
            'post_type' => 'user_art'
        );
        wp_update_post($post);
        update_post_meta( $art_id, "image-year-taken", wp_strip_all_tags($art_year));
        $response = array("success" => 1, "msg" => $obj->get_site_messages("art_edit_success"));
        echo json_encode($response);
        die();
    }




    /**
     * Delete User Art
     */
    function delete_user_art(){
        global $obj;
       $current_user_id = get_current_user_id();

        if(isset($_POST['delete_id'])){
            $delete_id = $_POST['delete_id'];
            $user_id = get_post_meta($delete_id,"art_user_id", true);

            if($current_user_id != $user_id){
                $response = array(
                    "success" => 0,
                    "msg"  => $obj->get_site_messages("art_invalid_user")
                );
                echo json_encode($response);
                exit;
            }

            if(wp_delete_post($delete_id)){
                $response = array(
                    "success" => 1
                );
                echo json_encode($response);
                exit;
            }
        }
        exit;
    }



    /*****************************************************
     * Submit Art to Contest
     ****************************************************/

    /**
     * Submit Art
     */

    function submit_art_to_contest()
    {

        check_ajax_referer('security-contest-submission-nonce', 'security');
        global $obj;
        global $loginCheck;
        global $email_template;

        $art_id = $_POST['art_id'];
        $art_title = get_the_title($art_id);
        $art_content =  $obj->get_the_content_by_id($art_id);
        $art_image_year_taken = get_post_meta( $art_id, 'image-year-taken', true );
        $art_user_id = get_current_user_id();
        $art_attachment_id = get_post_thumbnail_id($art_id);


        // ********* Check if User is allowed to Submit this Art
        // ********* (This Condition will only take place if user is smart enough to change the art ID hidden field)
        $allowed_page_access = $loginCheck->art_page_access_to_correct_user($art_id);
        if(!$allowed_page_access){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("submission_user_not_allowed") );
            echo json_encode($response);
            exit;
        }


        //  ********* Check if Contest is active and in Submission mode
        $active_contest_id = $obj->get_active_contest_id();
        $allowed_submission = false;
        if($active_contest_id && $obj->get_active_contest_mode($active_contest_id) == "submission_mode"){
            $allowed_submission = true;
        }
        if(!$allowed_submission){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("contest_not_submission") );
            echo json_encode($response);
            exit;
        }



        //  ********* Check if User has credit to Submit Art to the Contest
        $does_user_have_credit = $obj->is_user_allowed_for_submission();
        if(!$does_user_have_credit){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("no_credit") );
            echo json_encode($response);
            exit;
        }


        //  ***********   Submit Art to the Contest  ***********

        $insert_array = array( //our wp_insert_post args
            'post_title'    => $art_title,
            'post_content'  => $art_content,
            'post_status'   => 'publish',
            'post_type' => 'contest_submission',
        );
        $post_id = wp_insert_post($insert_array);

        //  *********      Set Featured Image
        set_post_thumbnail( $post_id,$art_attachment_id);

        //  *********     Set Contest Category . i.e., Link this post with current Contest
        wp_set_post_terms($post_id,$active_contest_id,"contest_category");
//        wp_set_post_terms($post_id,9,"contest_category");

        //  *********     Set Postmeta values year and Link User to the Submission
        update_post_meta( $post_id, "image-year-taken", $art_image_year_taken );
        update_post_meta( $post_id, "art_user_id",$art_user_id );

        $response = array("success" => 1, "msg" => $obj->get_site_messages("submission_success") );
        echo json_encode($response);


        //        Send mail
        $email_template->user_id = $art_user_id;
        $email_template->email_submit_art_to_contest();
        die();
    }





    /*****************************************************************************
     *      Stripe Payment (Buy Credit & Upgrade membership both Form)
     ****************************************************************************/

    function show_coupon_code_detail()
    {
        global $obj;
        global $wpdb;
        $payment_type_id = trim($_POST['package_id']);
        $coupon_code =  trim($_POST['coupon_code']);
        $pay_type_price = get_term_meta ( $payment_type_id, 'payment_price',true);
        $payment_type = get_term_meta ( $payment_type_id, 'payment_type',true);

        $tax = get_option( 'payment_tax_setting', '' );
        $total_price = $obj->get_total_price_by_tax($pay_type_price,$tax);

        if(empty($payment_type_id) && empty($coupon_code)){
            $response = array("success" => 2, "msg" => "" );
            echo json_encode($response);
            exit;
        }
        elseif (!empty($payment_type_id) && empty($coupon_code)){
            $response = array("success" => 1, "msg" => "Final Total (Incl. All Tax) : ".GP_CURRENCY_SYMBOL.$total_price );
            echo json_encode($response);
            exit;
        }
        elseif (empty($payment_type_id) && !empty($coupon_code)){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("select_package_for_coupon") );
            echo json_encode($response);
            exit;
        }
        elseif (!empty($payment_type_id) && !empty($coupon_code)){
//            $get_coupon = get_page_by_title($coupon_code, OBJECT, "gp_coupon_code");

            if($payment_type == "credit"){
                $cou_type = "credit-coupon";
            } else{
                $cou_type = "membership-coupon";
            }

            $coupon_query = "
            SELECT $wpdb->posts.ID        
            FROM $wpdb->posts            
            INNER JOIN $wpdb->postmeta AS cu ON ($wpdb->posts.ID = cu.post_id AND cu.meta_key='coupon-type' AND  cu.meta_value='".$cou_type."' )         
            WHERE $wpdb->posts.post_type = 'gp_coupon_code'
            AND $wpdb->posts.post_status = 'publish' 
            AND  $wpdb->posts.post_title = '".$coupon_code."' LIMIT 1";
            $get_coupon = $wpdb->get_results( $coupon_query, OBJECT);



            if(count($get_coupon) > 0){
                //  echo '<pre>' . print_r( $get_coupon, true ) . '</pre>';
                $coupon_id = current($get_coupon)->ID;
                $today_date = date("Y-m-d");
                $coupon_percentage = trim(get_post_meta( $coupon_id, 'coupon-percentage', true ));
                $coupon_end_date = $obj->convert_date_format(trim(get_post_meta( $coupon_id, 'coupon-end-date', true )));
                if($today_date > $coupon_end_date){
                    $response = array("success" => 0, "msg" => $obj->get_site_messages("coupon_expired") );
                    echo json_encode($response);
                    exit;
                } else {

                    $deduction_price = $obj->get_coupon_off_price($total_price,$coupon_percentage);
                    $final_amount = $obj->get_deducted_coupon_amount($total_price,$deduction_price);
                    $message = $obj->get_site_messages("coupon_success_with_detail");
                    $response = array("success" => 1,
                        "msg" => sprintf($message, $total_price, $coupon_percentage, $final_amount));
                    echo json_encode($response);
                    exit;
                }
            }else{
                $response = array("success" => 0,"msg" => $obj->get_site_messages("coupon_invalid"));
                echo json_encode($response);
            }
            exit;
        }
        exit;
    }


    function buy_credit_or_upgrade_membership(){

        check_ajax_referer('security-stripe-pay-nonce', 'security');
        global $obj;
        global $wpdb;
        global $email_template;

        $params = $_POST['data'];
        parse_str($params, $data);
        $token  = $data['stripeToken'];
        $payment_type  = $data['payment_type'];
        $user_id = get_current_user_id();
        $current_user = wp_get_current_user();
        $user_email = esc_html( $current_user->user_email );

        $coupon_code =  $data['coupon_code'];
        $payment_type_id = $data['package-id'];
        $payment_type_term = get_term_by( "id", $payment_type_id, "payment_type");
        $pay_type_price = get_term_meta ( $payment_type_id, 'payment_price',true);
        $upgrade_membership_time = get_term_meta ( $payment_type_id, 'upgrade_membership_time',true);
        $pay_credit_package = get_term_meta ( $payment_type_id, 'credit_package',true);
        $is_payment_type_correct = (is_object($payment_type_term) && $pay_type_price > 0) ? true : false;

        $contest_id = $obj->get_active_contest_id();
        //Set Product Values
        $itemName = $payment_type_term->name;
        $tax = get_option( 'payment_tax_setting', '' );
        $total_price = $obj->get_total_price_by_tax($pay_type_price,$tax);
        $total_price_stripe = $obj->convert_to_stripe_price($total_price);

        //      Default:   For Coupons
        $actual_price = $total_price;
        $deduction_price = "0";
        $coupon_applied = "No";
        $coupon_percentage = 0;


        //      Check Token
        if(empty($token)){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("error_token_processing_payment") );
            echo json_encode($response);
            exit;
        }

        //      Is Price properly Set & Payment Type Id is Valid(i.e, User didn't alter the id)
        if(!$is_payment_type_correct){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("error_wrong_package_selected") );
            echo json_encode($response);
            exit;
        }


        //      If payment type is NOT (credit or upgrade_membership)
        if($payment_type != "credit" && $payment_type != "upgrade_membership"){
            $response = array("success" => 0, "msg" => $obj->get_site_messages("error_wrong_payment_type") );
            echo json_encode($response);
            exit;
        }




        //        Check if User entered coupon Code
        if(!empty($coupon_code)){
            $get_coupon = get_page_by_title($coupon_code, OBJECT, "gp_coupon_code");

            if($payment_type == "credit"){
                $cou_type = "credit-coupon";
            } else{
                $cou_type = "membership-coupon";
            }

            $coupon_query = "
            SELECT $wpdb->posts.ID        
            FROM $wpdb->posts            
            INNER JOIN $wpdb->postmeta AS cu ON ($wpdb->posts.ID = cu.post_id AND cu.meta_key='coupon-type' AND  cu.meta_value='".$cou_type."' )         
            WHERE $wpdb->posts.post_type = 'gp_coupon_code'
            AND $wpdb->posts.post_status = 'publish' 
            AND  $wpdb->posts.post_title = '".$coupon_code."' LIMIT 1";
            $get_coupon = $wpdb->get_results( $coupon_query, OBJECT);

            if(count($get_coupon) > 0){
                $coupon_applied = "Yes";
                $coupon_id = current($get_coupon)->ID;
                $today_date = date("Y-m-d");
                $coupon_percentage = trim(get_post_meta( $coupon_id, 'coupon-percentage', true ));
                $coupon_end_date = $obj->convert_date_format(trim(get_post_meta( $coupon_id, 'coupon-end-date', true )));
                if($today_date > $coupon_end_date){
                    $response = array("success" => 0, "msg" => $obj->get_site_messages("coupon_expired") );
                    echo json_encode($response);
                    exit;
                } else {
                    $deduction_price = $obj->get_coupon_off_price($total_price,$coupon_percentage);
                    $total_price = $obj->get_deducted_coupon_amount($actual_price,$deduction_price);
                    $total_price_stripe = $obj->convert_to_stripe_price($total_price);
                }
            } else {
                $response = array("success" => 0,"msg" => $obj->get_site_messages("coupon_invalid"));
                echo json_encode($response);
                exit;
            }
        }



        //      Check If Contest is in Submission Mode - Else User can't buy credit
        if($payment_type == "credit"){
            $allowed_buy_credit = false;
            if($contest_id && $obj->get_active_contest_mode($contest_id) == "submission_mode"){
                $allowed_buy_credit = true;
            }
            if(!$allowed_buy_credit){
                $response = array("success" => 0, "msg" => $obj->get_site_messages("contest_not_submission_payment_disabled") );
                echo json_encode($response);
                exit;
            }
        }


        //      Check If Member is already Featured Member
        if($payment_type == "upgrade_membership"){
            $is_featured =  $obj->is_user_featured();
            if($is_featured) {
                $response = array("success" => 0, "msg" => $obj->get_site_messages("already_featured_member") );
                echo json_encode($response);
                exit;
            }
        }


        require_once(get_template_directory() . '/stripe_init.php' );

        //  set api key
        $stripe = array(
            "secret_key"      => STRIPE_SECRET_KEY,
            "publishable_key" => STRIPE_PUBLISHABLE_KEY
        );

        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        //  add customer to stripe
        $customer = \Stripe\Customer::create(array(
            'email' => $user_email,
            'source'  => $token
        ));

        //  charge a credit or a debit card
        $charge = \Stripe\Charge::create(array(
            'customer' => $customer->id,
            'amount'   => $total_price_stripe,
            'currency' => GP_CURRENCY,
            'description' => $itemName,
            'metadata' => array(
                'price' => $pay_type_price,
                'tax'   => $tax,
                "total_price" => $actual_price,
                "coupon_applied" => $coupon_applied,
                "coupon_code" => $coupon_code,
                "coupon_percentage" => $coupon_percentage,
                "coupon_discount" => $deduction_price,
                "final_price" => $total_price,
            ),

        ));

        //retrieve charge details
        $chargeJson = $charge->jsonSerialize();

        if($chargeJson['amount_refunded'] == 0 &&
            empty($chargeJson['failure_code']) &&
            $chargeJson['paid'] == 1 &&
            $chargeJson['captured'] == 1 &&
            $chargeJson['status'] == "succeeded")
        {

            $insert_title = $itemName." ".$chargeJson['id'];
            $stripe_payment_id = $chargeJson['id'];
            $stripe_customer = $chargeJson['customer'];
            $stripe_balance_transaction = $chargeJson['balance_transaction'];
            $stripe_receipt_url  = $chargeJson['receipt_url'];

            $insert_args = array(
                'post_title' => $insert_title,
                'post_content' => "",
                'post_type' => 'user_payment',
                'post_status' => 'publish',
            );
            $last_insert_id = wp_insert_post($insert_args);

            //   Assigning "payment_type" taxonomy -- (But we will not use this reference anywhere to the site)
            wp_set_post_terms($last_insert_id,$payment_type_id,"payment_type");

            update_post_meta($last_insert_id,"payment_user_id",$user_id);
            update_post_meta($last_insert_id,"payment_type",$payment_type);
            update_post_meta($last_insert_id,"payment_price",$pay_type_price);
            update_post_meta($last_insert_id,"payment_tax",$tax);
            //            Added Extra
            update_post_meta($last_insert_id,"payment_stripe_id",$stripe_payment_id);
            update_post_meta($last_insert_id,"payment_stripe_customer",$stripe_customer);
            update_post_meta($last_insert_id,"payment_stripe_balance_transaction",$stripe_balance_transaction);
            update_post_meta($last_insert_id,"payment_stripe_currency",GP_CURRENCY);
            update_post_meta($last_insert_id,"payment_stripe_receipt_url",$stripe_receipt_url);
            update_post_meta($last_insert_id,"payment_description",$itemName);
            //            Coupons
            update_post_meta($last_insert_id,"payment_total_price",$actual_price);
            update_post_meta($last_insert_id,"payment_coupon_applied",$coupon_applied);
            update_post_meta($last_insert_id,"payment_coupon_code",$coupon_code);
            update_post_meta($last_insert_id,"payment_coupon_percentage",$coupon_percentage);
            update_post_meta($last_insert_id,"payment_coupon_discount",$deduction_price);
            update_post_meta($last_insert_id,"payment_final_price",$total_price);

            if($payment_type == "credit"){
                update_post_meta($last_insert_id,"payment_contest_id",$contest_id);
                update_post_meta($last_insert_id,"credit_package",$pay_credit_package);
                $response = array("success" => 1, "msg" => $obj->get_site_messages("credit_payment_success") );

                echo json_encode($response);

                //        Send mail
                $email_template->user_id = $user_id;
                $email_template->email_success_credit_payment($stripe_payment_id,$itemName,$total_price);
                exit;
            }
            elseif($payment_type == "upgrade_membership"){
                if($upgrade_membership_time == "month"){
                    $membership_end_date = date('Y-m-d', strtotime('+1 months'));
                }
                else{
                    $membership_end_date = date('Y-m-d', strtotime('+1 years'));
                }
                update_post_meta($last_insert_id,"membership_end_date",$membership_end_date);
                update_post_meta($last_insert_id,"upgrade_membership_time",$upgrade_membership_time);
                //  Update Feature to user
                update_user_meta($user_id,"featured","yes");
                $response = array("success" => 1, "msg" => $obj->get_site_messages("upgrade_membership_payment_success") );
                echo json_encode($response);

                //        Send mail
                $email_template->user_id = $user_id;
                $email_template->email_success_upgrade_membership_payment($stripe_payment_id,$itemName,$total_price);
                exit;
            }
        }
        else{
            $response = array("success" => 0, "msg" => $obj->get_site_messages("transaction_fail") );
            echo json_encode($response);
            exit;
        }
        exit;
    }




    function contact_form()
    {
        check_ajax_referer('security-contact-nonce', 'security');
        global $email_template;
        $params = $_POST['data'];
        parse_str($params, $data);
        $email_template->email_contact_us($data);
        exit;
    }



}

// Initialize
global $gp_ajax;
$gp_ajax = new GP_Ajax();