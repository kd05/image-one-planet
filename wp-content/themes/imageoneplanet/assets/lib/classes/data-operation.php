<?php


class Data_Operation{


    public $user_role = "participant";


    function insert_post($data){
        $post_id = wp_insert_post($data);
        if($post_id){
            return true;
        }
    }






//   ***************************************************************
//   ***     User Arts
//   ***************************************************************

    /**
     * Get User Arts
     */
    function get_user_arts($user_id = ""){
        if($user_id == "") { $user_id = get_current_user_id(); }
        $meta_array = array("art_user_id" => $user_id);
        return  $this->get_post_by_multiple_meta("user_art", $meta_array,-1);
    }






//   ***************************************************************
//   ***     File Uploads
//   ***************************************************************

    /**
     * Check Upload File Type
     */
    function check_file_type($image_type){
        $allowed_types = array('image/jpeg' ,'image/jpg','image/png' ,'image/pjpeg','image/gif' );
        if(!in_array($image_type,$allowed_types)){
            return $response = array(
                "error" => "1",
                "message" => "Invalid File Type"
            );
        }
        else{
            return $response = array(
                "error" => "0",
                "message" => ""
            );
        }
    }


    /**
     * Check Upload File Size
     */
    function check_file_size($image_size){
        if($image_size > GP_LIMIT_FILE_UPLOAD){
            return $response = array(
                "error" => "1",
                "message" => $this->get_site_messages("file_size_error")
            );
        }
        else{
            return $response = array(
                "error" => "0",
                "message" => ""
            );
        }

    }




//   ***************************************************************
//   ***     Contest Functions
//   ***************************************************************



    /**
     * Check Active Contest ID
     */
    function get_active_contest_id(){
        $taxonomy = "contest_category";
        $meta_array = array("contest_status" => "active");
        $terms = $this->get_terms_by_multiple_meta($taxonomy, $meta_array);
        if(count($terms) > 0){
            return current($terms)->term_id;
        }
        else{
            return false;
        }
    }


    /**
     * Check Active Contest Mode
     */
    function get_active_contest_mode($contest_id = ""){
        if($contest_id == ""){
            $contest_id = $this->get_active_contest_id();
        }
        return  $contest_mode = trim(get_term_meta ( $contest_id, 'contest_mode',true));
    }




//   ***************************************************************
//   ***     Submission Functions
//   ***************************************************************


    function get_contest_submissions($contest_id = ""){
        if($contest_id == ""){
            $contest_id = $this->get_active_contest_id();
        }
        $tax_query =  array(
            array(
                'taxonomy' => 'contest_category',
                'field' => 'term_id',
                'terms' => $contest_id
            )
        );

        return  $this->get_post_by_multiple_meta("contest_submission", array(),-1,$tax_query, "ids");

    }

    /**
     * Get User Submission
     */
    function get_user_submissions($contest_id = "", $user_id = ""){
        if($contest_id == ""){
            $contest_id = $this->get_active_contest_id();
        }
        if($user_id == ""){
            $user_id = get_current_user_id();
        }

        $tax_query =  array(
            array(
                'taxonomy' => 'contest_category',
                'field' => 'term_id',
                'terms' => $contest_id
            )
        );

        $meta_array = array("art_user_id" => $user_id);
        return  $this->get_post_by_multiple_meta("contest_submission", $meta_array,-1,$tax_query);
    }


    /**
     * Get User Total Submissions
     */
    function get_user_total_submission_current_contest($contest_id = ""){
       $subs = $this->get_user_submissions();
       return $total_subs = count($subs);
    }


    /**
     * Get User Total Payments
     */
    function get_user_total_payments($contest_id = "", $user_id = "",$payment_type = "credit",$fields = ""){
        if($contest_id == ""){
            $contest_id = $this->get_active_contest_id();
        }
        if($user_id == ""){
            $user_id = get_current_user_id();
        }

        $meta_array = array("payment_user_id" => $user_id,"payment_contest_id" => $contest_id, "payment_type" => $payment_type);
        return  $this->get_post_by_multiple_meta("user_payment", $meta_array,-1,"",$fields);
    }


    /**
     * Get Total Credits
     */
    function calculate_total_credits_purchased($contest_id = "", $user_id = ""){
          $payments = $this->get_user_total_payments($contest_id,$user_id,"credit","ids");
          $total_credit_purchased = 0;
          if(is_array($payments) && count($payments) > 0){
              foreach ($payments as $payment_id){
                 $credit = get_post_meta($payment_id, "credit_package", true);
                 $total_credit_purchased = $total_credit_purchased +    $credit;
              }
          }
          return $total_credit_purchased;
    }


    /**
     * Is User have any credit Left for submission (Return True/False)
     */
    function is_user_allowed_for_submission(){
       $total_credit = $this->calculate_total_credits_purchased();
       $total_submission =  $this->get_user_total_submission_current_contest();
       return ($total_credit > $total_submission) ? true : false;
    }


    /**
     * Return Total Credit Left
     */
    function get_total_credit_left(){
        $total_credit = $this->calculate_total_credits_purchased();
        $total_submission =  $this->get_user_total_submission_current_contest();
        return ($total_credit - $total_submission);
    }



//   ***************************************************************
//   ***     User Functions
//   ***************************************************************

    function is_user_featured($user_id = ""){
        if($user_id == ""){  $user_id = get_current_user_id(); }
        return get_user_meta($user_id,"featured",true) == "yes" ? true : false;
    }


    function get_user_featured_membership_payments($user_id = ""){
        if($user_id == ""){  $user_id = get_current_user_id(); }
        $meta_array = array(
            "payment_user_id" => $user_id,
            "payment_type" => "upgrade_membership",
        );
        return $this->get_post_by_multiple_meta("user_payment",$meta_array,-1);
    }


    function get_user_featured_membership_end_date($user_id = ""){
        if($user_id == ""){  $user_id = get_current_user_id(); }
        $membership_payments = $this->get_user_featured_membership_payments($user_id);
        if(is_array($membership_payments) && count($membership_payments) > 0){
            $final_membership_end_date = "";
            foreach ($membership_payments as $payment){
                $payment_id = $payment->ID;
                $membership_end_date = trim(get_post_meta( $payment_id, 'membership_end_date', true ));
                if($membership_end_date > $final_membership_end_date){
                    $final_membership_end_date = $membership_end_date;
                }
            }
            return $final_membership_end_date;
        }
        else return false;
    }


    function get_user_data($user_id){

        $user_info = get_userdata($user_id);
        $email_address =  $user_info->user_email;

        $headshot_id = get_user_meta( $user_id, 'headshot', true);
        $first_name = get_user_meta($user_id, 'first_name', true);
        $last_name = get_user_meta($user_id, 'last_name', true);
        $full_name = $first_name." ".$last_name;
        $featured = get_user_meta($user_id, 'featured', true);
        $usertype = ucfirst(get_user_meta($user_id, 'usertype', true));
        $description = nl2br(get_user_meta($user_id, 'description', true));
        $country = get_user_meta($user_id, 'country', true);
        $gallery_name = get_user_meta($user_id, 'gallery_name', true);
        $website_link = get_user_meta($user_id, 'website-link', true);
        $facebook_link = get_user_meta($user_id, 'facebook-link', true);
        $instagram_link = get_user_meta($user_id, 'instagram-link', true);
        $birth_date = get_user_meta($user_id, 'birth-date', true);

        return array(
            "email_address" => $email_address,
            "headshot_id"  => $headshot_id,
            "first_name"  => $first_name,
            "last_name"  => $last_name,
            "full_name"  => $full_name,
            "featured"  => $featured,
            "usertype"  => $usertype,
            "description"  => $description,
            "country"  => $country,
            "gallery_name" => $gallery_name,
            "website_link"  => $website_link,
            "facebook_link"  => $facebook_link,
            "instagram_link"  => $instagram_link,
            "birth_date"  => $birth_date,
        );
    }



//   ***************************************************************
//   ***     Users Profile  Listing Functions
//   ***************************************************************

    function count_total_users(){
        $count_args  = array(
            'role'      => $this->user_role,
            'number'    => -1
        );
        $user_count_query = new WP_User_Query($count_args);
        $user_count = $user_count_query->get_results();

        // count the number of users found in the query
        return $total_users = $user_count ? count($user_count) : 0;
    }



//   ***************************************************************
//   ***     Payments Functions
//   ***************************************************************







//   ***************************************************************
//   ***     Profile galleries Prev / Next Pages Links
//   ***************************************************************



    function get_user_profile_gallery_images($user_id){
        $featured = get_user_meta($user_id, 'featured', true);
        if($featured == "yes") {
            $meta_array = array("participant_user_id" => $user_id);
            $profile_galleries = $this->get_post_by_multiple_meta("user_profile_gallery", $meta_array);
        }
        else{
            $meta_array = array("participant_user_id" => $user_id, "image-type" => "featured_image");
            $profile_galleries = $this->get_post_by_multiple_meta("user_profile_gallery", $meta_array,5);
        }
        return $profile_galleries;
    }


    function get_total_count_featured_images($user_id){
        $meta_array = array("participant_user_id" => $user_id, "image-type" => "featured_image");
        $profile_galleries = $this->get_post_by_multiple_meta("user_profile_gallery", $meta_array);
        return count($profile_galleries);
    }


    function get_total_count_listing_gallery_images($user_id){
        $meta_array = array("participant_user_id" => $user_id, "listing-image-type" => "listing_image");
        $profile_galleries = $this->get_post_by_multiple_meta("user_profile_gallery", $meta_array);
        return count($profile_galleries);
    }


    function get_previous_profile_galleries_link($galleries_ids,$current_key){
        $total = count($galleries_ids);
        $prev_id = ($current_key == 1) ? $galleries_ids[$total] : $galleries_ids[$current_key - 1];
        return get_the_permalink($prev_id);
    }


    function get_next_profile_galleries_link($galleries_ids,$current_key){
        $total = count($galleries_ids);
        $next_id = ($current_key == $total) ? current($galleries_ids) : $galleries_ids[$current_key + 1];
        return get_the_permalink($next_id);
    }





//   ***************************************************************
//   ***     Home Page Featured User Section
//   ***************************************************************

    function home_page_featured_user_section($feat_artist_users,$type = "Artist"){
        if(count($feat_artist_users) > 0) {
            $featured_artist_id = $feat_artist_users[array_rand($feat_artist_users)]->ID;
            $featured_artist_headshot_id = get_user_meta($featured_artist_id, 'headshot', true);
            $featured_artist_headshot_img = (($featured_artist_headshot_id == "") || !isset($featured_artist_headshot_id)) ? get_no_profile_pic_found() : current(wp_get_attachment_image_src($featured_artist_headshot_id, 'thumbnail'));;

            $user_description = get_user_meta($featured_artist_id, 'description', true);
            $description_length = 350;
            $description = strlen($user_description) > $description_length ?
                substr(nl2br($user_description),0,$description_length). " <a href='".get_author_posts_url($featured_artist_id)."'>.. Read More</a>"
                            : nl2br($user_description);

            $meta_array = array("participant_user_id" => $featured_artist_id, "listing-image-type" => "listing_image");
            $profile_galleries = $this->get_post_by_multiple_meta("user_profile_gallery", $meta_array);
            if (is_array($profile_galleries) && count($profile_galleries) > 0) {
                $prof_gallery = current($profile_galleries);
                $gallery_id = $prof_gallery->ID;
                $feat_img = get_the_post_thumbnail_url($gallery_id, "custom-size-2000");
            } else {
                $feat_img = get_image_not_found();
            }

            $temp_section = '
                    <section class="featured-artist-section ">
                        <div class="inner-container">
                            <div class="featured-image ">
                                <div class="feat-image-inner has-border">
                                    <img src="'.$feat_img.'" alt="Featured Artist Work">
                                </div>                                
                            </div> 
                            
                            <div class="text-wrapper">
                                <h4>Featured '.$type.'</h4>
                                <div class="artist-title">
                                    <div class="artist-image-wrapper">
                                        <div class="artist-image" style="background-image: url('.$featured_artist_headshot_img.')"></div>
                                    </div>
                                    
                                    <h1>'.get_user_meta($featured_artist_id, 'first_name', true).'</h1>
                                </div> 
                                
                                <p>'.$description.'</p>
                                <a href="'.get_author_posts_url($featured_artist_id).'" class="button green">View Profile</a>
                            </div> 
                         </div>
                     </section>
            ';
            return $temp_section;

        }
        else{
            return ;
        }
    }

//   ***************************************************************
//   ***     Common Functions
//   ***************************************************************

    /**
     * Get Content By ID
     */
    function get_the_content_by_id($post_id) {
        $page_data = get_post($post_id);
        if ($page_data) {
            return $page_data->post_content;
        }
        else return false;
    }


    /**
     * Get Posts By Multiple Meta Fields
     */
    function get_post_by_multiple_meta($post_type, $meta_array, $total_number = "-1",$tax_query = "",$fields = "") {

        $meta_query_array = array();
        foreach ($meta_array as $meta_key => $meta_val){
            $temp = array(
                'key'       => $meta_key,
                'value'     => $meta_val,
                'compare'   => '='
            );
            array_push($meta_query_array,$temp);
        }

        $args = array(
            //   'fields'    => 'ids',
            'post_type' => $post_type,
            'posts_per_page' => $total_number,
            'orderby' => 'title',
            'order' => 'ASC',
            'meta_query' => array(
                $meta_query_array
            ),
        ) ;

        if($tax_query != ""){ $args['tax_query'] = $tax_query;  }
        if($fields != ""){ $args['fields'] = $fields;  }
//        echo '<pre>' . print_r( $args, true ) . '</pre>';
        $posts = get_posts($args);
        return $posts;
    }



    /**
     * Check if Active Contest in submission mode
     */
    function get_terms_by_multiple_meta($taxonomy, $meta_array) {

        $meta_query_array = array();
        foreach ($meta_array as $meta_key => $meta_val){
            $temp = array(
                'key'       => $meta_key,
                'value'     => $meta_val,
                'compare'   => '='
            );
            array_push($meta_query_array,$temp);
        }

        $terms = get_terms( array(
            //   'fields'    => 'ids',
            'taxonomy' => $taxonomy,
            'hide_empty' => false,
            'meta_query' => array(
                $meta_query_array
            )
        ) );
        return $terms;
    }



    /**
     * Get User Profile listing - By multiple meta
     */
    function get_users_by_multiple_meta($paged,$meta_array,$order_by){
        $offset = ($paged ==1 ) ? 0 : ($paged - 1)* USER_PER_PAGE;
        $args = array(
            'meta_key'  => 'featured',
            'orderby' => $order_by,
            // "role" => $role,
            'role__not_in' => 'administrator',
            'number'    => USER_PER_PAGE,
            'offset'    => $offset ,// skip the number of users that we have per page
            'meta_query' => $meta_array
        );
        $user_query = new WP_User_Query($args);

        return $user_query;

    }





    /**
     * Get User Profile listing - By multiple meta
     */
    function get_all_users_excludin_administrator(){
        $args = array(
            'role__not_in' => 'administrator'
        );
        return $user_query = new WP_User_Query($args);
    }


    function get_all_featured_users(){
        $args  = array(
            'role__not_in' => 'administrator',
            'meta_key' => 'featured',
            'meta_value' => 'yes',
            'meta_compare' => '=' // exact match only
        );
        return $user_query = new WP_User_Query($args);
    }



    /**
     * Get User Profile listing Pagination
     */
    function get_user_profile_listing_pagination($paged,$total_pages){
        return paginate_links(array(
//                    'base' => get_pagenum_link(1) . '%_%',
            'format' => '?paged=%#%',
            'current' => $paged,
            'total' => $total_pages,
            'prev_text' => '<i class="fas fa-caret-left"></i>',
            'next_text' => '<i class="fas fa-caret-right"></i>',
            'type'     => 'list',
        ));
    }





    /**
     * Whole site Alert/Success/Error Messages
     */
    function get_site_messages($type=""){


        $messages = array(

            //        Upload File Message
            "select_file" => "Please Select File.",
            "file_error" => "Something wrong with the uploaded file.",
            "file_type_error" => "Invalid File.",
            "file_size_error" => "File should be less than ".GP_LIMIT_FILE_UPLOAD_MB,

            //        User Arts Message
            "art_upload_success" => "Art Successfully Uploaded.",
            "art_upload_limit_exceeds" => "Sorry, You have already uploaded maximum (".GP_MAX_USER_ARTS.") number of Arts.",
            "art_deleted_success" => "Art Successfully deleted.",
            "upload_art_to_submit_in_contest" => "You haven't uploaded any arts yet. Upload Art and than Submit to the Contest.",

            "art_edit_error" => "Something went wrong.",
            "art_edit_success" => "Art Successfully Updated.",
            "art_access_denied" => "Sorry you are not allowed to access this Art",
            "art_invalid_user" => "Something went wrong. You cannot delete this Art",

            //        Contest Message
            "contest_not_submission" => "Sorry, You cannot submit your art at this time.",

            //        Profile Gallery
            "profile_gallery_inquiry_success" => "Your Inquiry is successfully submitted.",
            "profile_gallery_inquiry_error" => "Something went wrong. Please Contact Us.",
            "profile_gallery_invalid_user" => "Something went wrong. You cannot delete this profile gallery",
            "delete_prof_gallery_success"   => "Profile Gallery Image successfully deleted.",
            "prof_gallery_upload_limit_exceeds" => "Sorry, You have already uploaded maximum (".GP_MAX_USER_PROFILE_IMAGES.") number of Profile Gallery Images.",
            "prof_gallery_edit_error" => "Something went wrong.",
            "prof_gallery_access_denied" => "Sorry you are not allowed to access this Profile Gallery Image.",
            "prof_gallery_edit_success" => "Gallery Image information successfully updated.",
            "prof_gallery_featured_exceeds" => "You cannot select more than (".GP_MAX_USER_PROFILE_FEATURED_IMAGES.") as a featured Image",
            "prof_gallery_change_to_featured" => "Image updated to Featured Image",
            "prof_gallery_change_to_regular" => "Image is no more featured Image",

            "prof_gallery_listing_exceeds" => "You can only select ".GP_MAX_USER_PROFILE_LISTING_IMAGES." as a Listing Image",
            "prof_gallery_change_to_listing" => "Image is selected as a Listing Image.",
            "prof_gallery_change_to_regular_listing" => "Image is no more Listing Image",
            "no_profile_gallery" => "User haven't uploaded any Arts yet.",

            //        Submission Message
            "submission_user_not_allowed" => "You are not allowed to submit this Art.",
            "submission_success" => "Your Art is successfully submitted to the contest.",
            "credit_left" => "You still have (%s) credit left.",
            "no_credit" => "Sorry, You have no credits to submit your Art.",
            "no_submission" => "No Submissions found",


            //       Create Account / User Messages
            "agree_fail" => "Please agree the condition to create an Account",
            "logout_to_create_account" => "You are logged In. Please Logout to create a new Account.",
            "login_failed" => "Username or password is invalid.",
            "success_create_account" => "Account successfully created. Please Log In to complete your profile.",
            "email_already_exist" => "The email address you have entered is already registered.",
            "error_create_account" => "Something went wrong while creating an Account. Please Contact Us.",


            //            Profile
            "edit_profile_success" => "Profile has been successfully updated.",
            "no_listing_image_selected" => "Your Profile won't appear in the Artists / Photographers page. Please upload the Gallery image and select it as a 'Listing Image' to make your profile public.",

            //            Coupon Code Messages
            "select_package_for_coupon" => "Please select package to apply coupon code.",
            "coupon_expired" => "The Coupon is expired.",
            "coupon_invalid" => "Invalid Coupon Code",
            "coupon_success" => "Coupon is Successfully Applied.",
            "coupon_success_with_detail" => "Coupon is Successfully Applied. <br>
                                            Total Amount :" . GP_CURRENCY_SYMBOL . "%.2f - Coupon Discount (%d &#37;) <br>
                                            Final Total : " . GP_CURRENCY_SYMBOL . "%.2f",

            //        Buy Credit / Upgrade Membership -
            "error_wrong_package_selected" => "Something wrong with the package you selected. Please contact Us.",
            "error_token_processing_payment" => "(Token: Error) Something wrong with the package you selected. Please contact Us.",
            "error_processing_payment" => "Something went wrong while creating processing payment. Please Contact Us.",
            "transaction_fail" => "Transaction has been failed.",
            "no_payment_type_found" => "No such payment type found.",
            "error_wrong_payment_type" => "Sorry, Something went wrong (Altered Payment Type).",
            "contest_not_submission_payment_disabled" => "Sorry, You cannot buy credit at this time.",
            "credit_payment_success" => "Your payment has been processed successfully and credit is added to your account.",
            "upgrade_membership_payment_success" => "Your payment has been processed successfully and your membership is upgraded as a featured member.",
            "already_featured_member" => "You are already a featured member.",

            //            User Profile Messages
            "no_user_profile" => "No Record Found",

            // Common
            "no_record" => "No Records Found",
            "success_contact" => "Your Inquiry is Successfully Submitted",

            // Instruction
            "inst_upload_art" => "Maximum File allowed to upload is ".GP_LIMIT_FILE_UPLOAD_MB.". Please compress your file by searching 'Online Image compression' in Google.",

        );

        if($type == ""){
            return $messages;
        }
        else{
            return $messages[$type];
        }

    }


    /**
     * Get Method Any Alert/Success/Error Messages on Page Load
     */
    function message_display_get_method($type){

        //        Delete Art Success Message
        if($type == "art_deleted_success"){
            if(isset($_GET['delete_success']) && $_GET['delete_success'] == 1){
                return "<p class='success'>".$this->get_site_messages($type)."</p>";
            }
            else{  return "";  }
        }


        //       Login Failed Message
        if($type == "login_failed"){
            if(isset($_GET['login']) && $_GET['login'] == "failed"){
                return "<p >".$this->get_site_messages($type)."</p>";
            }
            else{  return "";  }
        }


        //       Account Success Created
        if($type == "success_create_account"){
            if(isset($_GET['account-create']) && $_GET['account-create'] == "success"){
                return "<p class='success'>".$this->get_site_messages($type)."</p>";
            }
            else{  return "";  }
        }



        //     Edit Profile Success
        if($type == "edit_profile_success"){
            if(isset($_GET['edit-profile']) && $_GET['edit-profile'] == "success"){
                return "<p class='success'>".$this->get_site_messages($type)."</p>";
            }
            else{  return "";  }
        }


        //     Delete Profile Gallery Success
        if($type == "delete_prof_gallery_success"){
            if(isset($_GET['delete_success']) && $_GET['delete_success'] == "1"){
                return "<p class='success'>".$this->get_site_messages($type)."</p>";
            }
            else{  return "";  }
        }

    }


    function get_coupon_off_price($price,$percentage){
        return round(($price * ($percentage / 100)),2);
    }

    function get_deducted_coupon_amount($price,$deduction_price){
        return round(($price - $deduction_price),2);
    }


    /**
     * Get Price with Tax
     */
    function get_total_price_by_tax($price,$tax = 0){
        return round(($price + (($price * $tax) / 100)),2);
    }


    /**
     * Get Stripe Price
     */
    function convert_to_stripe_price($price){
        return ($price * 100);
    }


    /**
     * Get Stripe Reverse
     */
    function reverse_convert_to_stripe_price($price){
        return ($price / 100);
    }



    /**
     * Convert date to Y-m-d format
     */
    function convert_date_format($date = ""){
        if($date == ""){ $date = date("Y-m-d");  }
        return date("Y-m-d", strtotime($date));
    }



    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }



    /**
     * Upload Attachment and set Image as a Featured Image if post Id is passed
     */
    function upload_attachment_file($uploaded_file,$post_id = ""){



        $uploaddir = wp_upload_dir();
        $file = $uploaded_file["name"];
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $attachment_title = pathinfo($file, PATHINFO_FILENAME);
        $updated_file_name = $this->generateRandomString(7).time().".".$ext;
        $uploadfile = $uploaddir['path'] . '/' . basename( $updated_file_name );

        move_uploaded_file( $uploaded_file["tmp_name"] , $uploadfile );
        $filename = basename( $uploadfile );
        $wp_filetype = wp_check_filetype(basename($filename), null );

        // Insert post as a attachment
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', $attachment_title),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attach_id = wp_insert_attachment( $attachment, $uploadfile,$post_id);
        // Generate Meta Data for the attachment/Media Post type
        $attach_data = wp_generate_attachment_metadata( $attach_id, $uploadfile );
        wp_update_attachment_metadata( $attach_id, $attach_data );


        //        Set Featured Image
        if($post_id != "") { set_post_thumbnail( $post_id, $attach_id ); }

        return $attach_id;
    }




}


// Initialize
global $obj;
$obj = new Data_Operation();