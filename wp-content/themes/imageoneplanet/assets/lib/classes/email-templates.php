<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



Class Email_Templates
{
    public $user_id;
    public $admin_email;
    public $from_email;
    public $subject;
    public $email_header;
    public $header;
    public $body;
    public $footer;


    public function __construct()
    {

        $this->header = '<table  border="1"  cellpadding="0" cellspacing="0"  width="800">
                            <tr>
                                <td align="left"  bgcolor="#091115" style="padding: 40px 20px">
                                    <img src="' . get_template_directory_uri() . '/assets/images/imageoneplanet-logo.png." >
                                 </td>
                            </tr>';

        $this->body = '';

        $this->footer = '<tr>
                            <td align="center" style="padding: 20px 0 20px 0;">
                                <span>&copy; Image One Planet ' . date("Y") . '</span>
                            </td>
                        </tr>
                    </table>';


        $this->admin_email = get_option("admin_email");
        $this->from_email = "noreply@imageoneplanet.com";
        $this->email_header[] = "From: Image One Planet  <$this->from_email>";
        $this->email_header[] = "Return-Path: <$this->admin_email>";
        $this->email_header[] = 'MIME-Version: 1.0' . "\r\n";
        $this->email_header[] = 'Content-Type: text/html; charset=UTF-8';

    }


    //    Mail When user succesfully submits the Art to the contest
    public function email_create_account()
    {
        $this->email_header[] = "CC: <$this->admin_email>";
        $this->subject = "Welcome to Image One Planet";
        $user_info = get_userdata($this->user_id);
        $user_email = $user_info->user_email;
        $name = $user_info->last_name . " " . $user_info->first_name;

        $this->body = '<td bgcolor="#ffffff" style="padding: 70px 30px ;">
                         <table  cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                               <td>
                                    <h3> Hello, ' . $name . '</h3>
                                    
                                    <p>Thank you for registering with Image One Planet. Please complete your profile by login to the site. </p>
                                    <p>Email us if you have any questions @ <a href="mailto:' . $this->admin_email . '">' . $this->admin_email . '</a>.</p> 
                               </td>                              
                              </tr>
                         </table>
                        </td>';

        $email_html = $this->header .
            $this->body .
            $this->footer;

        wp_mail($user_email, $this->subject, $email_html, $this->email_header);
    }


    //    Mail When user succesfully submits the Art to the contest
    public function email_gallery_image_inquiry($gallery_id, $data)
    {

        $this->admin_email = get_option("admin_email");
        $this->email_header[] = "";
        $this->email_header[] = "From: Image One Planet  <" . $data['pg-email'] . ">";
        $this->email_header[] = "CC: " . $this->admin_email;
        $this->email_header[] = "Return-Path: <" . $data['pg-email'] . ">";
        $this->email_header[] = 'MIME-Version: 1.0' . "\r\n";
        $this->email_header[] = 'Content-Type: text/html; charset=UTF-8';


        $gallery_title = get_the_title($gallery_id);
        $this->subject = "Image One Planet - Inquiry for your Profile Gallery Image($gallery_title)";
        $user_info = get_userdata($this->user_id);
        $user_email = $user_info->user_email;
        $name = $user_info->last_name . " " . $user_info->first_name;

        $this->body = '<td bgcolor="#ffffff" style="padding: 70px 30px ;">
                         <table  cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                               <td>
                                    <h3> Hello ' . $name . ',</h3>
                                    
                                    <p>Congratulations, You have received a new Inquiry for you Profile Gallery Image (' . $gallery_title . ')</p><br />
                                    <p>Id# : ' . $gallery_id . ' </p><br />
                                    <p>Information of the User is below:</p> 
                                    <p><strong>Name</strong> : ' . $data['pg-name'] . '</p> 
                                    <p><strong>Email</strong> : ' . $data['pg-email'] . '</p> 
                                    <p><strong>Message</strong> : ' . nl2br($data['pg-message']) . '</p> 
                               </td>                              
                              </tr>
                         </table>
                        </td>';

        $email_html = $this->header .
            $this->body .
            $this->footer;

        wp_mail($user_email, $this->subject, $email_html, $this->email_header);
    }


    //    Edit Profile Success Email
    public function email_success_profile_updated()
    {

        $this->subject = "Image One Planet - Your Profile Is Successfully Updated";
        $user_info = get_userdata($this->user_id);
        $user_email = $user_info->user_email;
        $name = $user_info->last_name . " " . $user_info->first_name;

        $this->body = '<td bgcolor="#ffffff" style="padding: 70px 30px ;">
                         <table  cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                               <td>
                                    <h3> Hello, ' . $name . '</h3>
                                    
                                    <p>Your Profile has been successfully updated.</p>
                               </td>                              
                              </tr>
                         </table>
                        </td>';

        $email_html = $this->header .
            $this->body .
            $this->footer;

        wp_mail($user_email, $this->subject, $email_html, $this->email_header);
    }


    //    Cron Job Email When User Featured membership is expired
    public function email_user_featured_member_expired()
    {

        $this->subject = "Image One Planet - Your Featured Membership Is Expired";
        $user_info = get_userdata($this->user_id);
        $user_email = $user_info->user_email;
        $name = $user_info->last_name . " " . $user_info->first_name;
//        echo '<pre>' . print_r( $user_info, true ) . '</pre>';

        $this->body = '<td bgcolor="#ffffff" style="padding: 70px 30px ;">
                         <table  cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                               <td>
                                    <h3> Hello, ' . $name . '</h3>
                                    
                                    <p>Your Subscription has been expired.</p>
                               </td>                              
                              </tr>
                         </table>
                        </td>';

        $email_html = $this->header .
            $this->body .
            $this->footer;

        wp_mail($user_email, $this->subject, $email_html, $this->email_header);
    }


    //    Mail When user succesfully submits the Art to the contest
    public function email_submit_art_to_contest()
    {
        $this->subject = "Image One Planet - Thank You For Submitting Your Art.";
        $user_info = get_userdata($this->user_id);
        $user_email = $user_info->user_email;
        $name = $user_info->last_name . " " . $user_info->first_name;


        $this->body = '<td bgcolor="#ffffff" style="padding: 70px 30px ;">
                         <table  cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                               <td>
                                    <h3> Hello, ' . $name . '</h3>
                                    
                                    <p>Your have successfully uploaded your art to the contest.</p>
                               </td>                              
                              </tr>
                         </table>
                        </td>';

        $email_html = $this->header .
            $this->body .
            $this->footer;

        wp_mail($user_email, $this->subject, $email_html, $this->email_header);
    }


    //    Success Payments
    public function email_success_credit_payment($stripe_payment_id, $itemName, $total_price)
    {
        $this->subject = "Thank you for purchasing the credit";
        $user_info = get_userdata($this->user_id);
        $user_email = $user_info->user_email;
        $name = $user_info->last_name . " " . $user_info->first_name;

        global $obj;
//        $total_price = $obj->get_total_price_by_tax($price, $tax);

        $this->body = '<td bgcolor="#ffffff" style="padding: 70px 30px ;">
                         <table  cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                               <td>
                                    <h3> Dear ' . $name . ',</h3>
                                    
                                    <p>Payment #  : ' . $stripe_payment_id . ' </p>
                                    <p>Thank you for purchasing the credit - ' . $itemName . ' </p>
                                    <p>We have received your payment for ' . GP_CURRENCY_SYMBOL . $total_price . '. The payment has been authorized and approved..</p> 
                               </td>                              
                              </tr>
                         </table>
                        </td>';

        $email_html = $this->header .
            $this->body .
            $this->footer;

        wp_mail($user_email, $this->subject, $email_html, $this->email_header);
    }


    public function email_success_upgrade_membership_payment($stripe_payment_id, $itemName, $total_price)
    {
        $this->subject = "Thank you for upgrading your membership";
        $user_info = get_userdata($this->user_id);
        $user_email = $user_info->user_email;
        $name = $user_info->last_name . " " . $user_info->first_name;

        global $obj;
//        $total_price = $obj->get_total_price_by_tax($price, $tax);

        $this->body = '<td bgcolor="#ffffff" style="padding: 70px 30px ;">
                         <table  cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                               <td>
                                    <h3> Dear ' . $name . ',</h3>
                                    
                                    <p>Payment # : ' . $stripe_payment_id . ' </p>
                                    <p>Thank you for purchasing membership uprade package - ' . $itemName . ' </p>
                                    <p>We have received your payment for ' . GP_CURRENCY_SYMBOL . $total_price . '. The payment has been authorized and approved.</p> 
                               </td>                              
                              </tr>
                         </table>
                        </td>';

        $email_html = $this->header .
            $this->body .
            $this->footer;

        wp_mail($user_email, $this->subject, $email_html, $this->email_header);
    }


    //   Contest Mode Status Change Notify all users
    public function email_users_notify_contest_mode_change($term_name, $mode)
    {
        $this->subject = "Image One Planet - Contest Status is changed";
        $user_info = get_userdata($this->user_id);
        $user_email = $user_info->user_email;
        $name = $user_info->last_name . " " . $user_info->first_name;

        global $obj;
        $email_content = nl2br($obj->get_the_content_by_id(428));

        $this->body = '<td bgcolor="#ffffff" style="padding: 70px 30px ;">
                         <table  cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                               <td>
                                    <h3> Hello, ' . $name . '</h3>
                                    
                                    <p>' . $email_content . '</p>
                               </td>                              
                              </tr>
                         </table>
                        </td>';

        $email_html = $this->header .
            $this->body .
            $this->footer;

        wp_mail($user_email, $this->subject, $email_html, $this->email_header);
    }


    //    Notify Winner to all users When Mode is updated to Final Mode
    public function email_winner_notification($submission_id, $tag)
    {
        $this->subject = "Image One Planet - Congratulations you have been selected as a Winner";
        $user_info = get_userdata($this->user_id);
        $user_email = $user_info->user_email;
        $name = $user_info->last_name . " " . $user_info->first_name;
        $submission_title = get_the_title($submission_id);


        if ($tag == "top-100") {
            $winner_body = "Congratulations, Your submission($submission_title) is selected as a Top 100.";
        } elseif ($tag == "top-20") {
            $winner_body = "Congratulations, Your submission($submission_title) is selected as a Top 20.";
        } elseif ($tag == "most-popular") {
            $winner_body = "Congratulations, Your submission($submission_title) is selected as a Most Popular.";
        } elseif ($tag == "winner-bronze") {
            $winner_body = "Congratulations, Your submission($submission_title) is selected as a 3rd - Winner.";
        } elseif ($tag == "winner-silver") {
            $winner_body = "Congratulations, Your submission($submission_title) is selected as a 2nd - Winner.";
        } elseif ($tag == "winner-gold") {
            $winner_body = "Congratulations, Your submission($submission_title) is selected as a 1st - Winner";
        }


        $this->body = '<td bgcolor="#ffffff" style="padding: 70px 30px ;">
                         <table  cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                               <td>
                                    <h3> Hello, ' . $name . '</h3>
                                    
                                    <p>' . $winner_body . '</p>
                               </td>                              
                              </tr>
                         </table>
                        </td>';

        $email_html = $this->header .
            $this->body .
            $this->footer;

        wp_mail($user_email, $this->subject, $email_html, $this->email_header);
    }


    //   Contest Mode Status Change Notify all users
    public function email_contact_us($data)
    {
        $userFullName = $data['full-name'];
        $userEmail = $data['email-address'];
        $phone = $data['phone-no'];
        $message = $data['message'];

        $this->subject = "Image One Planet - New Inquiry From User";

        $this->email_header[] = "";
        $this->email_header[] = "From: Image One Planet <" . $userEmail . ">";
        $this->email_header[] = "Return-Path: <" . $userEmail . ">";
        $this->email_header[] = 'MIME-Version: 1.0' . "\r\n";
        $this->email_header[] = 'Content-Type: text/html; charset=UTF-8';

        global $obj;

        $this->body = '<td bgcolor="#ffffff" style="padding: 70px 30px ;">
                         <table  cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                               <td>
                                    <h3> Hello, Admin</h3>
                                    
                                    <p>A new Inquiry from a new user.</p>
                                    <br>
                                    <p><b>Name: </b>'.$userFullName.'</p>
                                    <p><b>Email: </b>'.$userEmail.'</p>
                                    <p><b>Phone: </b>'.$phone.'</p>
                                    <p><b>Message: </b>'.$message.'</p>
                               </td>                              
                              </tr>
                         </table>
                        </td>';

        $email_html = $this->header .
            $this->body .
            $this->footer;

        wp_mail($this->admin_email, $this->subject, $email_html, $this->email_header);


    }
}



// Initialize
global $email_template;
$email_template = new Email_Templates;