<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



Class Login_check
{

    /**
     * Page Accessible to Only Logged In Users
     **/
    public function redirect_user_not_logged_in()
    {
        $isUserLoggedIn = is_user_logged_in();
        if(!$isUserLoggedIn){
            $this->page_restriction_redirect();
        }
    }



    /**
     * Access of page is allowed to particular roles only
     **/
    public function page_access_to_roles($page_allowed)
    {
        $this->redirect_user_not_logged_in();

        if( is_user_logged_in() ) {
            $user = wp_get_current_user();
            $role = ( array ) $user->roles;
            $user_role = $role[0];
            if(!in_array($user_role,$page_allowed)){
                $this->page_restriction_redirect();
            }
        } else {
            return false;
        }

    }


    public function get_logged_in_user_role(){
        $user = wp_get_current_user();
        $role = ( array ) $user->roles;
        return $user_role = $role[0];
    }



    /**
     * Art Page is only Accessible to the User who uploaded it
     * Else Redirect
     **/
    public function art_page_access_to_correct_user($art_id="")
    {
        $this->redirect_user_not_logged_in();
        if($art_id == ""){
            $art_id = get_the_ID();
        }
        $user_id = get_post_meta( $art_id, "art_user_id", true);
        $logged_in_user_id = get_current_user_id();

        if( trim($user_id) != $logged_in_user_id) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * Edit Gallery Image is only Accessible to the User who uploaded it
     * Else Redirect
     **/
    public function gallery_page_access_to_correct_user($gallery_id)
    {
        $this->redirect_user_not_logged_in();
        $user_id = get_post_meta( $gallery_id, "participant_user_id", true);
        $logged_in_user_id = get_current_user_id();

        if( trim($user_id) != $logged_in_user_id) {
            return false;
        } else {
            return true;
        }
    }



    /**
     * Redirect to "access-denied" page
     **/
    public function page_restriction_redirect(){
        $redirectUrl = site_url()."/access-denied/";
        wp_redirect( $redirectUrl );
        exit;
    }

}
//global $loginCheck;
$loginCheck = new Login_check();