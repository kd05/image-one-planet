<?php
if (!defined('ABSPATH')) {
    exit;
}




add_action( 'init', 'create_tag_taxonomies');
function create_tag_taxonomies() {

    $labels = array(
        'name' => _x( 'Submission Tags', 'taxonomy general name' ),
        'singular_name' => _x( 'Submission Tag', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Submission Tags' ),
        'popular_items' => __( 'Popular Submission Tags' ),
        'all_items' => __( 'All Submission Tags' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Submission Tag' ),
        'update_item' => __( 'Update Submission Tag' ),
        'add_new_item' => __( 'Add New Submission Tag' ),
        'new_item_name' => __( 'New Submission Tag Name' ),
        'separate_items_with_commas' => __( 'Separate tags with commas' ),
        'add_or_remove_items' => __( 'Add or remove tags' ),
        'choose_from_most_used' => __( 'Choose from the most used tags' ),
        'menu_name' => __( 'Submission Tags' ),
    );

    register_taxonomy('submission_tag',array('contest_submission'),array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array( 'slug' => 'submission_tag' ),
    ));

}



