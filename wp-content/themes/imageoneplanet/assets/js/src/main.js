/**
 * T A B L E   O F   C O N T E N T S
 *
 * @author      Geek Power Web Design
 * @version     1.0
 *
 * XX. ON LOAD
 *
 * 01. ASSIGN FUNCTIONS
 * 01.01. TOGGLE MOBILE MENU
 * 01.02. TOGGLE SUBMENU
 * 01.03. BUILD MOBILE MENU
 * 01.04. MOVE JURY SLIDER
 * 01.05. INITIALIZE WOOKMARK
 * 01.06. POPULATE GALLERY LIGHTBOX
 * 01.07. TOGGLE GALLERY LIGHTBOX
 *
 * 02. TRIGGER FUNCTIONS
 * 02.01. MOBILE-RELATED TRIGGERS
 */




$(window).on('load', function () {

    /**
     * XX. ON LOAD
     * Function to run on load
     */


    if ($('.wookmark-container').length) {

        $('.wookmark-container').each(function() {
            initializeWookmark($(this));
        });
    }


});


function initializeWookmark(wookmarkContainer) {

    // https://www.npmjs.com/package/wookmark

    var wookmarkOptions = {
        align: 'center',
        autoResize: true,
        container: $(wookmarkContainer),
        flexibleWidth: 0,
        offset: 3,
        resizeDelay: 50,
        verticalOffset: 20
    };

    if ($(window).outerWidth() > 1000) {
        wookmarkOptions.itemWidth = '33%';
    } else if ($(window).outerWidth() > 550) {
        wookmarkOptions.itemWidth = '48%';
        wookmarkOptions.verticalOffset = 15;
    } else {
        wookmarkOptions.itemWidth = '100%';
        wookmarkOptions.verticalOffset = 10;
    }

    $(wookmarkContainer).wookmark(wookmarkOptions);
}


$(document).ready(function () {

    /**
     * 01. ASSIGN FUNCTIONS
     * Assigns functions when the document is ready.
     */
    var windowWidth = $(window).outerWidth();
    var header      = $('header.imone-header');
    var mobileIcon  = $('#mobile-icon');
    var mobileMenu  = $('#mobile-menu');


    /**
     * 01.01. TOGGLE MOBILE MENU
     * Opens and closes the mobile menu
     */
    function toggleMobileMenu() {

        if (!mobileMenu.hasClass('active') && !mobileMenu.hasClass('animating')) {

            mobileIcon.add(mobileMenu).addClass('active');
            $('body').addClass('body-fixed');
        } else if (!mobileMenu.hasClass('animating')) {

            mobileIcon.add(mobileMenu).removeClass('active');
            $('body').removeClass('body-fixed');
        }
    }


    /**
     * 01.02. TOGGLE SUBMENU
     * Opens/closes the mobile menu's submenus
     *
     * @param       parent        element         The parent element of the submenu to be manipulated
     */
    function toggleSubmenu(parent) {

        if (!parent.hasClass('open') && !parent.hasClass('animating')) {

            parent.addClass('animating open');
            parent.find('.sub-menu').slideDown(400, function() {
                parent.removeClass('animating');
            });
        } else if (!parent.hasClass('animating')) {

            parent.addClass('animating').removeClass('open');
            parent.find('.sub-menu').slideUp(400, function() {
                parent.removeClass('animating');
            });
        }
    }


    /**
     * 01.03. BUILD MOBILE MENU
     * Builds and deconstructs the mobile menu based on current screen size
     */
    function buildMobileMenu() {

        // make sure we're not moving things chaotically
        header.addClass('moving');

        var menuInner   = mobileMenu.find('.mobile-menu-inner');
        var headerInner = header.find('.header-nav');
        var iconMarkup  = '<span class="submenu-icon"><i class="fas fa-angle-down"></i></span>';

        // if below max threshold
        if (windowWidth <= 1210) {

            // move main nav to mobile menu
            if (!mobileMenu.find('.nav').length) {
                menuInner.append(header.find('.nav'));
                menuInner.find('.nav .menu-item-has-children').append(iconMarkup);
                console.log("Hello");
            }

            // move action-nav to mobile menu
            if (windowWidth < 651) {
                if (!mobileMenu.find('.action-nav').length) {
                    menuInner.prepend(header.find('.action-nav'));
                }
            }
        } else if (windowWidth > 650) {

            // if above minimum threshold

            // move action-nav back to header
            if (mobileMenu.find('.action-nav').length) {
                headerInner.append(mobileMenu.find('.action-nav'));
            }

            // move main nav back to header
            if (windowWidth >= 1210) {
                if (mobileMenu.find('.nav').length) {
                    mobileMenu.find('.nav .submenu-icon').remove();
                    headerInner.prepend(mobileMenu.find('.nav'));
                }
            }
        }

        // create interval to remove class and allow this function to be run again
        setTimeout(function() {
            header.removeClass('moving');
        }, 150);
    }


    /**
     * 01.04. MOVE JURY SLIDER
     * Shifts the Jury slider on the contest page
     */
    function moveJurySlider(control) {

        var newPosition;
        var sliderTrack          = $('.juror-inner-wrapper');
        var currentTrackPosition = parseInt(sliderTrack.css('left').split('px')[0]);
        var numOfUnits           = sliderTrack.find('.juror-unit').length;
        var interval             = $('.juror-unit').outerWidth() + 50;

        if (!sliderTrack.hasClass('animating')) {

            if (control.hasClass('right') && (-(currentTrackPosition / interval)) !== (numOfUnits - 1)) {
                newPosition = (currentTrackPosition - interval) + 'px';
            } else if (control.hasClass('left') && currentTrackPosition !== 0) {
                newPosition = (currentTrackPosition + interval) + 'px';
            } else {
                newPosition = currentTrackPosition;
            }

            sliderTrack.addClass('animating').css('left', newPosition);

            setTimeout(function() {
                sliderTrack.removeClass('animating');
            }, 350)
        }
    }


    /**
     * 01.05. INITIALIZE WOOKMARK
     * Initializes Wookmark layout for specified container
     *
     * @param       wookmarkContainer       element         The container to be wookmark'd
     */
    // function initializeWookmark(wookmarkContainer) {
    //
    //     // https://www.npmjs.com/package/wookmark
    //
    //     var wookmarkOptions = {
    //         align: 'center',
    //         autoResize: true,
    //         container: $(wookmarkContainer),
    //         flexibleWidth: 0,
    //         offset: 3,
    //         resizeDelay: 50,
    //         verticalOffset: 20
    //     };
    //
    //     if ($(window).outerWidth() > 1000) {
    //         wookmarkOptions.itemWidth = '33%';
    //     } else if ($(window).outerWidth() > 550) {
    //         wookmarkOptions.itemWidth = '48%';
    //         wookmarkOptions.verticalOffset = 15;
    //     } else {
    //         wookmarkOptions.itemWidth = '100%';
    //         wookmarkOptions.verticalOffset = 10;
    //     }
    //
    //     $(wookmarkContainer).wookmark(wookmarkOptions);
    // }


    /**
     * 01.06. POPULATE GALLERY LIGHTBOX
     * Populates the lightbox with info pulled from the thumbnail
     *
     * @param           thumb           element         The thumbnail to open the lightbox for
     */
    function populateGalleryLightbox(thumb) {

        var voteMarkup;
        var voteURL  = thumb.attr('data-instagram');
        var viewInstaURL  = thumb.attr('data-instagram-view');
        var lightbox = $('.gallery-lightbox');


        if (voteURL) {
            voteMarkup  = '<div class="vote-wrapper">';
            voteMarkup +=   '<a href="' + voteURL + '" target="_blank" class="button green vote-button">';
            voteMarkup +=       '<i class="fab fa-instagram"></i>';
            voteMarkup +=       '<span>VOTE ON INSTAGRAM</span>';
            voteMarkup +=   '</a>';
            voteMarkup += '</div>';

            lightbox.addClass('voteable').find('.lightbox-details').append(voteMarkup);
        }

        // alert(viewInstaURL);
        if (viewInstaURL != "#" && viewInstaURL != "") {
            voteMarkup  = '<div class="vote-wrapper">';
            voteMarkup +=   '<a href="' + viewInstaURL + '" target="_blank" class="button green vote-button">';
            voteMarkup +=       '<i class="fab fa-instagram"></i>';
            voteMarkup +=       '<span>VIEW ON INSTAGRAM</span>';
            voteMarkup +=   '</a>';
            voteMarkup += '</div>';

            lightbox.addClass('voteable').find('.lightbox-details').append(voteMarkup);
        }

        lightbox.find('.image-wrapper img').attr('src', thumb.attr('data-full-img'));
        lightbox.find('.image-wrapper img').attr('alt', thumb.attr('data-img-title'));
        lightbox.find('.art-title').text(thumb.attr('data-img-title'));
        lightbox.find('.artist').text(thumb.attr('data-artist-name'));
    }


    /**
     * 01.07. TOGGLE GALLERY LIGHTBOX
     * Toggles the opening/closing of the gallery lightbox
     */
    function toggleGalleryLightbox() {

        var emptyLightbox = false;
        var lightbox      = $('.gallery-lightbox');

        if (!lightbox.hasClass('animating')) {

            if (lightbox.hasClass('show')) {
                lightbox.removeClass('show').addClass('animating');
                emptyLightbox = true;
            } else {
                lightbox.addClass('show');
                $('body').addClass('body-fixed');
            }
        }

        setTimeout(function() {

            lightbox.removeClass('animating');

            if (emptyLightbox) {
                lightbox.find('.art-title, .artist').empty();
                lightbox.find('.image-wrapper img').attr('src', '');
                lightbox.removeClass('voteable').find('.vote-wrapper').remove();
                $('body').removeClass('body-fixed');
            }
        }, 400);
    }



    /**
     * 02. TRIGGER FUNCTIONS
     * Assigns triggers for functions.
     */

    /**
     * 02.01. MOBILE-RELATED TRIGGERS
     * Triggers related to mobile menu
     */

    // Build Mobile Menu - run immediately to check screen size
    buildMobileMenu();

    $(window).on('resize', function() {

        // hide wookmark layouts so that jarriness of layout change is hidden
        if ($('.wookmark-container').length) {
            $('.wookmark-initialised').removeClass('wookmark-initialised');
        }

        // run functions on an interval to avoid chaos
        setTimeout(function() {

            // Build Mobile Menu - if that function hasn't recently run
            if (!header.hasClass('moving')) {
                windowWidth = $(window).outerWidth();
                buildMobileMenu();
            }

            // Toggle Mobile Menu - close menu on resize
            if (mobileMenu.hasClass('active')) {
                toggleMobileMenu();
            }

            // re-initialize wookmark
            if ($('.wookmark-container').length) {

                $('.wookmark-container').each(function() {
                    initializeWookmark($(this));
                });
            }
        }, 300);

        if ($('#contest-page').length) {
            $('.juror-inner-wrapper').css('left', 0);
        }
    });

    // Toggle Mobile Menu - on icon click
    $('body').on('click', '#mobile-icon', toggleMobileMenu);

    // Toggle Submenu
    $('body').on('click', '.submenu-icon', function() {
        toggleSubmenu($(this).parent('li'));
    });

    // Move Jury Slider
    if ($('#contest-page').length) {

        $('.down-arrow').on('click', function() {
            $('html, body').animate({
                scrollTop: $('#scroll-target').offset().top
            }, 800);
        });

        $('body').on('click', '.jury-slider-wrapper .control', function() {
            moveJurySlider($(this));
        });

        $('body').on('keydown', function(e) {

            if (e.keyCode === 27 || e.which === 27) {

                if ($('.gallery-lightbox').hasClass('show')) {
                    $('.gallery-lightbox').removeClass('show');
                    $('body').removeClass('body-fixed');
                }
            }
        });
    }

    if ($('.gallery-lightbox').length) {

        $('.open-lightbox').on('click', function() {

            populateGalleryLightbox($(this));
            toggleGalleryLightbox();
            $('body').addClass('body-fixed');
        });

        $('.close-lightbox, .gallery-overlay').on('click', function() {
            toggleGalleryLightbox();
        });
    }











    $('.magnific-popup').magnificPopup({
        type: 'image',
        // other options
        gallery: {
            // options for gallery
            enabled: true
        },
        image: {
            // options for image content type
            titleSrc: function(item) {
                return '<h4> Author Name: ' + item.el.attr('author-data') + '&nbsp; | &nbsp; ID#: '+ item.el.attr('id-data')+'</h4> ' +
                    '<p>Title: ' + item.el.attr('title')+'</p>';
            }
        }
    });



    /**
     * User Edit Profile
     */
    $("#edit_profile_submit").on("click",edit_profile_submit);





    /**
     * User Art Upload
     */
    $("#user_art_submit").on("click",user_art_upload);

    /**
     * User Art Edit
     */
    $("#user_art_edit").on("click",user_art_edit);

    /**
     * User Art Delete
     */
    $(".delete-user-art").on("click",user_art_delete);



    /**
     * Submit Art to Submission
     */
    $(".submit-to-contest").on("click",submit_art_to_contest);


    /**
     * Create Account
     */
    $(".btn-create-account").on("click",create_account);



    /**
     * Profile Gallery Featured Image Update
     */
    $(".pg-image-type").on("click",change_featured_image);

    /**
     * Profile Gallery Featured Image Update
     */
    $(".pg-listing-image-type").on("click",change_listing_gallery_image);

    /**
     * Add Profile Gallery - Images
     */
    $("#add_prof_gallery").on("click",add_prof_gallery);

    /**
     * Edit Profile Gallery - Images
     */
    $("#edit_prof_gallery").on("click",edit_prof_gallery);


    /**
     * Profile Gallery Image - Interest Submit
     */
    $("#gallery_interest_submit").on("click",gallery_interest_submit);


    /**
     * Profile Gallery Delete
     */
    $(".delete-profile-gallery").on("click",profile_gallery_delete);


    /**
     * Contact Us Form
     */
    $("#contact-submit").on("click",contact_submit);

    /**
     * Home Page Featured User
     */
    if($(".home-featured-wrapper").length){
        setInterval(home_page_featured_users, 5000);
    }


    // Search Pop Up
    $(".header-search-icon,.search-pop-up-overlay,.close-btn").on("click",search_pop_up);
    $(".search-submit").on("click",function () {
        $("form.search-form").submit();
    });





    if($("#paymentFrm").length){
        $( "#package-id" ).change(show_payment_detail);
        $( ".coupon_code" ).keyup(show_payment_detail);
    }
    $(document).on('submit','#paymentFrm',stripe_submit);


    // ********************
    // * Functions
    // ********************


    function search_pop_up(){
        // alert(1);
        $(".search-pop-up-container").toggleClass("pop-up-on");
        $(".search-pop-up-overlay").toggleClass("overlay-pop-up-on");
        $( ".search-field" ).focus();
    }


    function home_page_featured_users(){
        $.ajax({
            url: website_urls.ajax_url,
            type: 'POST',
            // dataType: 'json',
            data : {
                action : "home_page_featured_user"
            },
            success: function(result){
                console.log("Console Success : "+result);
                $(".home-featured-wrapper").hide().html(result).fadeIn(1500);
            },
            error: function (response) {
               console.log("Error Home Page Show Featured User");
            }
        });
    }




    function user_art_upload(){
        event.preventDefault();

        var form = $('#add-user-art-form')[0];

        var error = 0;
        var msg = "";


        $('.add-user-art-form .required').each(function(){
            var fieldValue = $(this).val();
            if(fieldValue == ""){
                error = 1;
                msg = "<p>Please Enter all the Fields (*)</p>";
            }
        });


        if(error == 0){
            var file_validation = file_upload_validation("user-image-featured");
            error = file_validation.error;
            msg = file_validation.msg;
        }


        if(error == 1){
            set_message_alert(msg);
            scroll_to("message-alert");
        }

        else {
            $('.message-alert').html("");

            var security = $("#security-user-art").val();
            var action = $("#action").val();
            var title = $("#image-title").val();
            var description = $("#image-description").val();
            var year = $("#image-year-taken").val();

            var form_data = new FormData(form);
            var file_data = $('#user-image-featured').prop('files')[0];

            form_data.append('security', security);
            form_data.append('file', file_data);
            form_data.append('action', action);
            form_data.append('title', title);
            form_data.append('description', description);
            form_data.append('year', year);

            show_loading();
            $.ajax({

                type: "POST",
                data: form_data,
                url: website_urls.ajax_url,
                cache: false,
                // dataType: 'json',
                contentType: false,
                processData: false,

                success: function(response){
                    console.log(response);
                    hide_loading();

                    if(response == "file_error"){
                        set_message_alert("<p>"+$(".file_error").text()+"</p>");
                        scroll_to("message-alert");
                    }
                    else if(response == "file_type_error"){
                        set_message_alert("<p>"+$(".file_type_error").text()+"</p>");
                        scroll_to("message-alert");
                    }
                    else if(response == "file_size_error"){
                        set_message_alert("<p>"+$(".file_size_error").text()+"</p>");
                        scroll_to("message-alert");
                    }
                    else if(response == "art_upload_limit_exceeds"){
                        set_message_alert("<p>"+$(".art_upload_limit_exceeds").text()+"</p>");
                        scroll_to("message-alert");
                    }
                    else if(response == "success"){
                        set_message_alert("<p class='success'>"+$(".art_upload_success").text()+"</p>");
                        scroll_to("message-alert");
                        document.getElementById("add-user-art-form").reset();
                    }
                },
                error: function (response) {
                    hide_loading();
                    console.log('---error---');

                    set_message_alert("<p> Error: "+$(".file_error").text()+"</p>");
                    scroll_to("message-alert");
                }

            });
        }

    }


    function change_featured_image(){
        event.preventDefault();
        console.log("Event : Change Fatured Image");

        show_loading();
        var gallery_id = $(this).attr("data-prof-gallery-id");
        var element_id = $(this).attr("id");
        $.ajax({
            url: website_urls.ajax_url,
            type: 'POST',
            dataType: 'json',
            data : {
                action : "update_featured_image",
                gallery_id : gallery_id,
            },
            success: function(result){
                console.log("Console Success : "+result);
                hide_loading();
                if(result.success == 0){
                    set_message_alert("<p>"+result.msg+"</p>");
                    scroll_to("message-alert");
                }
                if(result.success == 1){
                    // set_message_alert("<p class='success'>"+result.msg+"</p>");
                    // scroll_to("message-alert");
                    alert(result.msg);
                    $("#"+element_id).removeClass("fas far").addClass(result.icon);

                }
            },
            error: function (response) {
                hide_loading();
                set_message_alert("<p>"+$('.prof_gallery_edit_error').text()+"</p>");
                scroll_to("message-alert");
            }
        });
    }


    function change_listing_gallery_image(){
        event.preventDefault();
        console.log("Event : Change Listing Gallery Image");

        show_loading();
        var gallery_id = $(this).attr("data-prof-gallery-id");
        var element_id = $(this).attr("id");
        $.ajax({
            url: website_urls.ajax_url,
            type: 'POST',
            dataType: 'json',
            data : {
                action : "update_listing_gallery_image",
                gallery_id : gallery_id,
            },
            success: function(result){
                console.log("Console Success : "+result);
                hide_loading();
                if(result.success == 0){
                    set_message_alert("<p>"+result.msg+"</p>");
                    scroll_to("message-alert");
                }
                if(result.success == 1){
                    // set_message_alert("<p class='success'>"+result.msg+"</p>");
                    // scroll_to("message-alert");
                    alert(result.msg);
                    $("#"+element_id).removeClass("fas far").addClass(result.icon);

                }
            },
            error: function (response) {
                hide_loading();
                set_message_alert("<p>"+$('.prof_gallery_edit_error').text()+"</p>");
                scroll_to("message-alert");
            }
        });
    }



    function add_prof_gallery(){
        event.preventDefault();
        console.log("Event : Add Prof Gallery");
        var form = $('#add-prof-gallery-form')[0];
        var error = 0;
        var msg = "";


        $('.add-prof-gallery-form .required').each(function(){
            var fieldValue = $(this).val();
            if(fieldValue == ""){
                error = 1;
                msg = "<p>Please Enter all the Fields (*)</p>";
            }
        });


        if(error == 0){
            var file_validation = file_upload_validation("prof-gallery-image");
            error = file_validation.error;
            msg = file_validation.msg;
        }


        if(error == 1){
            set_message_alert(msg);
            scroll_to("message-alert");
        }

        else {
            $('.message-alert').html("");

            var security = $("#security").val();
            var action = $("#action").val();
            var title = $("#image-title").val();
            var description = $("#image-description").val();
            var featured = $('.image-type:checked').val();
            var year = $("#image-year-taken").val();
            var status = $("#image-status").val();
            var price = $("#image-price").val();
            var tag = $("#image-tag").val();
            var form_data = new FormData(form);
            var file_data = $('#prof-gallery-image').prop('files')[0];
            var data = $('#add-prof-gallery-form').serialize();

            // alert(tag);
            form_data.append('security', security);
            form_data.append('file', file_data);
            form_data.append('action', action);
            form_data.append('title', title);
            form_data.append('description', description);
            form_data.append('featured', featured);
            form_data.append('year', year);
            form_data.append('status', status);
            form_data.append('price', price);
            form_data.append('tag', tag);

            show_loading();
            $.ajax({

                type: "POST",
                data: form_data,
                url: website_urls.ajax_url,
                cache: false,
                // dataType: 'json',
                contentType: false,
                processData: false,

                success: function(response){
                    console.log(response);
                    hide_loading();

                    if(response == "file_error"){
                        set_message_alert("<p>"+$(".file_error").text()+"</p>");
                        scroll_to("message-alert");
                    }
                    else if(response == "file_type_error"){
                        set_message_alert("<p>"+$(".file_type_error").text()+"</p>");
                        scroll_to("message-alert");
                    }
                    else if(response == "file_size_error"){
                        set_message_alert("<p>"+$(".file_size_error").text()+"</p>");
                        scroll_to("message-alert");
                    }
                    else if(response == "prof_gallery_upload_limit_exceeds"){
                        set_message_alert("<p>"+$(".prof_gallery_upload_limit_exceeds").text()+"</p>");
                        scroll_to("message-alert");
                    }
                    else if(response == "prof_gallery_featured_exceeds"){
                        set_message_alert("<p>"+$(".prof_gallery_featured_exceeds").text()+"</p>");
                        scroll_to("message-alert");
                    }
                    else if(response == "prof_gallery_listing_exceeds"){
                        set_message_alert("<p>"+$(".prof_gallery_listing_exceeds").text()+"</p>");
                        scroll_to("message-alert");
                    }
                    else if(response == "success"){
                        set_message_alert("<p class='success'>"+$(".art_upload_success").text()+"</p>");
                        scroll_to("message-alert");
                        document.getElementById("add-prof-gallery-form").reset();
                    }
                },
                error: function (response) {
                    hide_loading();
                    console.log('---error---');

                    set_message_alert("<p> Error: "+$(".file_error").text()+"</p>");
                    scroll_to("message-alert");
                }

            });
        }

    }


    function edit_prof_gallery(){
        event.preventDefault();
        var error = 0;
        var msg = "";


        $('.edit-prof-gallery-form .required').each(function(){
            var fieldValue = $(this).val();
            if(fieldValue == ""){
                error = 1;
                msg = "<p>Please Enter all the Fields (*)</p>";
            }
        });


        if(error == 1){
            set_message_alert(msg);
            scroll_to("message-alert");
        } else {
            $('.message-alert').html("");

            var security = $("#security").val();
            var action = $("#action").val();
            var data = $('#edit-prof-gallery-form').serialize();
            show_loading();
            $.ajax({
                url: website_urls.ajax_url,
                type: 'POST',
                dataType: 'json',
                data : {
                    action : action,
                    data : data,
                    security : security,
                },
                success: function(result){
                    console.log("Console Success : "+result);
                    hide_loading();
                    if(result.success == 0){
                        set_message_alert("<p>"+result.msg+"</p>");
                        scroll_to("message-alert");
                    }
                    if(result.success == 1){
                        set_message_alert("<p class='success'>"+result.msg+"</p>");
                        scroll_to("message-alert");
                    }
                },
                error: function (response) {
                    hide_loading();
                    set_message_alert("<p>"+$('.error_create_account').text()+"</p>");
                    scroll_to("message-alert");
                }
            });
        }

    }


    if($(".edit-profile-container").length) {
        // $( "#birth-date" ).datepicker();

        $("input[name='usertype']").change(function(e){
            var usertype = $(this).val();
            $(".select-user-cat").addClass("hidden");
            $("."+usertype+"-cat").removeClass("hidden");
        });
    }


    function edit_profile_submit(){
        event.preventDefault();
        console.log("Event : Edit Profile");
        var form = $('#edit-profile-form')[0];
        var error = 0;
        var msg = "";

        $('.edit-profile-form .valid-url').each(function(){
            url_validate = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
            var fieldValue = $(this).val();
            if(fieldValue != ""){
                if(!url_validate.test(fieldValue)){
                    error = 1;
                    field_title = $(this).attr("placeholder");
                    msg = "<p>"+field_title+" is invalid. (ex. http://www.google.com)</p>";
                }
            }
        });

        $('.edit-profile-form .required').each(function(){
            var fieldValue = $(this).val();
            if(fieldValue == ""){
                error = 1;
                msg = "<p>Please Enter all the Fields (*)</p>";
            }
        });

        var file_check = check_already_file_uploaded("headshot_id");
        if(error == 0 && file_check != 1){
            var file_validation = file_upload_validation("headshot-image");
            error = file_validation.error;
            msg = file_validation.msg;
        }

        if(error == 0){
            if (!$("input[name='usertype']:checked").val()) {
                error = 1;
                msg = "<p>Check User Type</p>";
            }
        }

        if(error == 1){
            set_message_alert(msg);
            scroll_to("message-alert");
        }
        else {
            $('.message-alert').html("");
            var security = $("#security").val();
            var action = $("#action").val();
            var form_data = new FormData(form);
            var file_data = $('#headshot-image').prop('files')[0];
            form_data.append('security', security);
            form_data.append('file', file_data);
            form_data.append('action', action);

            show_loading();
            $.ajax({
                type: "POST",
                data: form_data,
                url: website_urls.ajax_url,
                cache: false,
                // dataType: 'json',
                contentType: false,
                processData: false,

                success: function(response){
                    console.log(response);
                    hide_loading();
                    if(response == "select_file"){
                        set_message_alert("<p>"+$(".select_file").text()+"</p>");
                    }
                    else if(response == "file_type_error"){
                        set_message_alert("<p>"+$(".file_type_error").text()+"</p>");
                    }
                    else if(response == "file_size_error"){
                        set_message_alert("<p>"+$(".file_size_error").text()+"</p>");
                    }
                    else if(response == "success"){
                        set_message_alert("<p class='success'>"+$(".art_upload_success").text()+"</p>");
                        window.location.href = website_urls.site_url+"/edit-profile/?edit-profile=success";
                    }
                    scroll_to("message-alert");

                },
                error: function (response) {
                    hide_loading();
                    console.log('---error---');
                    set_message_alert("<p> Error: "+$(".file_error").text()+"</p>");
                    scroll_to("message-alert");
                }
            });
        }
    }




    function user_art_edit(){
        event.preventDefault();

        var form = $('#add-user-art-form')[0];

        var error = 0;
        var msg = "";


        $('.edit-user-art-form .required').each(function(){
            var fieldValue = $(this).val();
            if(fieldValue == ""){
                error = 1;
                msg = "<p>Please Enter all the Fields (*)</p>";
            }
        });


        if(error == 1){
            set_message_alert(msg);
            scroll_to("message-alert");
        } else {
            $('.message-alert').html("");

            var security = $("#security-user-art").val();
            var action = $("#action").val();
            var data = $('#edit-user-art-form').serialize();
            show_loading();
            $.ajax({
                url: website_urls.ajax_url,
                type: 'POST',
                dataType: 'json',
                data : {
                    action : action,
                    data : data,
                    security : security,
                },
                success: function(result){
                    console.log("Console Success : "+result.success);
                    console.log("Console Success : "+result);
                    hide_loading();
                    if(result.success == 0){
                        set_message_alert("<p>"+result.msg+"</p>");
                        scroll_to("message-alert");
                    }
                    if(result.success == 1){
                        set_message_alert("<p class='success'>"+result.msg+"</p>");
                        scroll_to("message-alert");
                    }
                },
                error: function (response) {
                    hide_loading();
                    set_message_alert("<p>"+$('.error_create_account').text()+"</p>");
                    scroll_to("message-alert");
                }
            });
        }

    }



    function user_art_delete(){

        console.log($(this).attr("delete-art-id"));
        var delete_id = $(this).attr("delete-art-id");
        var art_title = $(this).attr("delete-art-title");
        var success_redirect = $(this).attr("redirect-success");

        if(confirm("Are you sure you want to delete '"+art_title+ "' Art?")){

            $.ajax({
                url: website_urls.ajax_url,
                type: 'POST',
                dataType: 'json',
                data : {
                    action : 'delete_user_art',
                    delete_id : delete_id,
                },
                success: function(result){
                    console.log(result.success);
                    if(result.success == 1){
                        window.location.href = success_redirect+"/?delete_success=1";
                    }
                    if(result.success == 0){
                        set_message_alert("<p>"+result.msg+"</p>");
                        scroll_to("message-alert");
                    }
                },
                error: function (response) {
                    console.log('---error---');
                }
            });

        }
        else{
            return false;
        }

    }



    function submit_art_to_contest(event){
        event.preventDefault();
        console.log("Event : User Submit Art to Contest");

        if(confirm("Are you sure you want to submit this art to the contest?")){

            var art_id = $(".art_id").val();
            var security = $("#security-contest-submission").val();

            show_loading();
            $.ajax({
                url: website_urls.ajax_url,
                type: 'POST',
                dataType: 'json',
                data : {
                    action : 'submit_art_to_contest',
                    art_id : art_id,
                    security : security
                },
                success: function(result){
                    console.log("Console Success : "+result.success);
                    hide_loading();
                    if(result.success == 0){
                        set_message_alert("<p>"+result.msg+"</p>");
                        scroll_to("message-alert");
                    }
                    if(result.success == 1){
                        set_message_alert("<p class='success'>"+result.msg+"</p>");
                        scroll_to("message-alert");
                    }
                },
                error: function (response) {
                    hide_loading();
                    console.log('Console Error : Something went wrong.');
                }
            });

        }
        else{
            return false;
        }

    }



    function create_account(event) {
        event.preventDefault();
        console.log("Event : Create Account");
        var error = 0;
        var msg = "";


        var is_checked = document.getElementById("agree-condition").checked;
        if(is_checked == false){
            error = 1;
            msg = "<p>"+$('.agree_fail').text()+"</p>";
        }

        var password = $("#user_password").val();
        var confirm_password = $("#confirm_password").val();

        if(password != confirm_password){
            error = 1;
            msg = "<p>Password confirmation doesn't match password</p>";
        }

        $('.create-account-form .chk-email').each(function(){
            var fieldValue = $(this).val();
            var re = /\S+@\S+\.\S+/;
            if(re.test(fieldValue) == false){
                error = 1;
                msg = "<p>Please Enter Valid Email Address</p>";
            }
        });

        $('.create-account-form .required').each(function(){
            var fieldValue = $(this).val();
            if(fieldValue == ""){
                error = 1;
                msg = "<p>Please Enter all the Fields (*)</p>";
            }
        });


        if(error == 1){
            set_message_alert(msg);
            scroll_to("message-alert");
        } else {
            $('.message-alert').html("");

            var security = $("#security-create-account").val();
            var data = $('.create-account-form').serialize();
            show_loading();
            $.ajax({
                url: website_urls.ajax_url,
                type: 'POST',
                dataType: 'json',
                data : {
                    action : 'create_account',
                    data : data,
                    security : security,
                },
                success: function(result){
                    console.log("Console Success : "+result.success);
                    console.log("Console Success : "+result);
                    hide_loading();
                    if(result.success == 0){
                        set_message_alert("<p>"+result.msg+"</p>");
                        scroll_to("message-alert");
                    }
                    if(result.success == 1){
                        // set_message_alert("<p class='success'>"+result.msg+"</p>");
                        window.location.href = website_urls.site_url+"/login-page/?account-create=success";
                        // scroll_to("message-alert");
                        // document.getElementById("create-account-form").reset();
                    }
                },
                error: function (response) {
                    hide_loading();
                    set_message_alert("<p>"+$('.error_create_account').text()+"</p>");
                    scroll_to("message-alert");
                }
            });
        }

    }



    function gallery_interest_submit(event) {
        event.preventDefault();
        console.log("Event : Gallery Interest");
        var error = 0;
        var msg = "";

        $('#profile-gallery-interest .chk-email').each(function(){
            var fieldValue = $(this).val();
            var re = /\S+@\S+\.\S+/;
            if(re.test(fieldValue) == false){
                error = 1;
                msg = "<p>Please Enter Valid Email Address</p>";
            }
        });

        $('#profile-gallery-interest .required').each(function(){
            var fieldValue = $(this).val();
            if(fieldValue == ""){
                error = 1;
                msg = "<p>Please Enter all the Fields (*)</p>";
            }
        });


        if(error == 1){
            set_message_alert(msg);
            scroll_to("message-alert");
        } else {
            $('.message-alert').html("");

            var security = $("#security-user-gallery-form").val();
            var data = $('#profile-gallery-interest').serialize();
            show_loading();
            $.ajax({
                url: website_urls.ajax_url,
                type: 'POST',
                // dataType: 'json',
                data : {
                    action : 'gallery_interest',
                    data : data,
                    security : security,
                },
                success: function(result){
                    console.log("Console Success : "+result);
                    hide_loading();
                    set_message_alert("<p class='success'>"+$('.profile_gallery_inquiry_success').text()+"</p>");
                    scroll_to("message-alert");
                    document.getElementById("profile-gallery-interest").reset();
                },
                error: function (response) {
                    hide_loading();
                    set_message_alert("<p>"+$('.profile_gallery_inquiry_error').text()+"</p>");
                    scroll_to("message-alert");
                }
            });
        }

    }



    function profile_gallery_delete(event){
        event.preventDefault();
        console.log("Event : Delete Profile Gallery Image");

        console.log($(this).attr("delete-prof-gallery-id"));
        var delete_id = $(this).attr("delete-prof-gallery-id");
        var art_title = $(this).attr("delete-prof-gallery-title");
        var success_redirect = $(this).attr("redirect-success");

        if(confirm("Are you sure you want to delete '"+art_title+ "'?")){

            $.ajax({
                url: website_urls.ajax_url,
                type: 'POST',
                dataType: 'json',
                data : {
                    action : 'delete_user_prof_gallery',
                    delete_id : delete_id,
                },
                success: function(result){
                    console.log(result);
                    if(result.success == 0){
                        set_message_alert("<p>"+result.msg+"</p>");
                        scroll_to("message-alert");
                    }
                    if(result.success == 1){
                        window.location.href = success_redirect+"/?delete_success=1";
                    }
                },
                error: function (response) {
                    console.log('---error---');
                }
            });

        }
        else{
            return false;
        }

    }


    /*************************************************
     ***         Stripe Buy Credit
     ************************************************/

    Stripe.setPublishableKey($(".stripe_publishable_key").text());
    function stripeResponseHandler(status, response) {
        if (response.error) {
            //enable the submit button
            $('#payBtn').removeAttr("disabled");
            //display the errors on the form
            $(".message-alert").html("<p>"+response.error.message+"</p>");
        } else {
            var form$ = $("#paymentFrm");
            //get token id
            var token = response['id'];
            //insert the token into the form
            form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
            //submit form to the server

            // form$.get(0).submit();
            var security = $("#security-stripe-pay").val();
            var action = $("#action").val();
            var data = form$.serialize();
            show_loading();
            $.ajax({
                url: website_urls.ajax_url,
                type: 'POST',
                dataType: 'json',
                data : {
                    action : action,
                    data : data,
                    security : security,
                },
                success: function(result){
                    console.log("Console Success : "+result.success);
                    console.log("Console Success : "+result);
                    hide_loading();
                    if(result.success == 0){
                        set_message_alert("<p>"+result.msg+"</p>");
                        scroll_to("message-alert");
                    }
                    if(result.success == 1){
                        set_message_alert("<p class='success'>"+result.msg+"</p>");
                        scroll_to("message-alert");
                        document.getElementById("paymentFrm").reset();
                    }
                },
                error: function (response) {
                    console.log("Console Error : "+ response);
                    hide_loading();
                    set_message_alert("<p>"+$('.error_processing_payment').text()+"</p>");
                    scroll_to("message-alert");
                }
            });


        }
    }


    function stripe_submit(event) {
        //disable the submit button to prevent repeated clicks
        event.preventDefault();
        console.log("Event : Stripe Payment");
        var error = 0;
        var msg = "";

        $('#paymentFrm .required').each(function(){
            var fieldValue = $(this).val();
            if(fieldValue == ""){
                error = 1;
                msg = "<p>Please Enter all the Fields (*)</p>";
            }
        });



        // $('#payBtn').attr("disabled", "disabled");


        if(error == 0) {
            //create single-use token to charge the user
            Stripe.createToken({
                number: $('.card-number').val(),
                cvc: $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler);
        }
        else{
            set_message_alert(msg);
            scroll_to("message-alert");
        }

        //submit from callback
        return false;
    }


    function show_payment_detail(){
        console.log("Here");
        var package_id = $("#package-id").val();
        var coupon_code = $(".coupon_code").val();


        $.ajax({
            url: website_urls.ajax_url,
            type: 'POST',
            dataType: 'json',
            data : {
                action : "show_coupon_code_detail",
                package_id : package_id,
                coupon_code : coupon_code
            },
            success: function(result){
                console.log("Console Success : "+result);
                if(result.success == 0){
                    console.log(result.msg);
                    $(".payment-message").html("<p>"+result.msg+"</p>");
                    // scroll_to("payment-message");
                }
                if(result.success == 1){
                    $(".payment-message").html("<p class='success'>"+result.msg+"</p>");
                }
                if(result.success == 2){
                    $(".payment-message").html(result.msg);
                }

            },
            error: function (response) {
                console.log(response + "Error Console Coupon");
            }
        });
    }





    function contact_submit(event){
        // alert(1);
        event.preventDefault();
        var error = 0;
        var msg = "";

        $('.contact-form .chk-email').each(function(){
            var fieldValue = $(this).val();
            var re = /\S+@\S+\.\S+/;
            if(re.test(fieldValue) == false){
                error = 1;
                msg = "<p>Please Enter Valid Email Address</p>";
            }
        });

        $('.contact-form .required').each(function(){
            var fieldValue = $(this).val();
            if(fieldValue == ""){
                error = 1;
                msg = "<p>Please Enter all the Fields (*)</p>";
            }
        });

        if(error == 1){
            set_message_alert(msg);
            scroll_to("message-alert");
        } else {
            set_message_alert("");

            var security = $("#security").val();
            var data = $('#form-contact-page').serialize();
            show_loading();
            $.ajax({
                url: website_urls.ajax_url,
                type: 'POST',
                data : {
                    action : 'contact_form',
                    data : data,
                    security : security,
                },
                success: function(result){
                    console.log(result);
                    hide_loading();
                    set_message_alert("<p class='success'>"+$(".success_contact").text()+"</p>");
                    scroll_to("message-alert");
                    $('#form-contact-page').trigger("reset");
                }
            });
        }

    }




    // ***************************
    // * Common Functions
    // ***************************

    function file_upload_validation(fileUploadId){
        var extensions = ["image/png", "image/jpeg", "image/jpg"];
        var file_data = $('#'+fileUploadId).prop('files')[0];
        var returnArray = { error : 0, "msg" : "" }

        if ($('#'+fileUploadId).get(0).files.length === 0) {
            returnArray = { error : 1, "msg" : "<p>"+$(".select_file").text()+"</p>" }
            return returnArray;
        }

        if(extensions.indexOf(file_data.type) == -1){
            returnArray = { error : 1, "msg" : "<p>"+$(".file_type_error").text()+"</p>" }
            return returnArray;
        }
        return returnArray;
    }


    function check_already_file_uploaded(fileId){
        if($("#"+fileId).length) {
            if($("#"+fileId).val() == "" || $("#"+fileId).val() <= 0){  return 0; }
        }
        else{  return 0; }
        return 1;
    }




    function set_message_alert(msg){
        $('.message-alert').html(msg);
    }


    function scroll_to(class_scroll){
        var topToScrollTo = $("."+class_scroll).offset().top;
        $("html, body").stop().animate({ scrollTop: topToScrollTo - 100}, 1000);
    }


    function show_loading(){
        $("body").css("opacity",0.6);
        $(".loading-image").removeClass("hidden");
    }


    function hide_loading() {
        $("body").css("opacity", 1);
        $(".loading-image").addClass("hidden");
    }




});