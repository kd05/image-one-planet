<?php
/**
 * FOOTER TEMPLATE FILE
 *
 */

$menu_locations = get_nav_menu_locations();
?>

</div> <!-- /custom-container [from header.php] -->
<footer class="imone-footer">
    <div class="inner-container">
        <div class="footer-nav">
            <div class="col">
                <?php $menu1 = get_term($menu_locations['footer-menu-1'], 'nav_menu');  ?>
                <h5>Navigate</h5>
                <?php
                wp_nav_menu($args = array(
                    'theme_location' => 'footer-menu-1',
                    'container'      => false,
                    'depth'          => 1
                ));
                ?>
            </div>
            <div class="col">
                <?php $menu2 = get_term($menu_locations['footer-menu-2'], 'nav_menu');  ?>
                <h5>Photo Contest</h5>
                <?php
                wp_nav_menu($args = array(
                    'theme_location' => 'footer-menu-2',
                    'container'      => false,
                    'depth'          => 1
                ));
                ?>
            </div>
            <div class="col">
                <?php $menu3 = get_term($menu_locations['footer-menu-3'], 'nav_menu');  ?>
                <h5><?php echo $menu3->name; ?></h5>
                <?php
                wp_nav_menu($args = array(
                    'theme_location' => 'footer-menu-3',
                    'container'      => false,
                    'depth'          => 1
                ));
                ?>
            </div>
            <div class="col">
                <?php $menu4 = get_term($menu_locations['footer-menu-4'], 'nav_menu');  ?>
                <h5><?php echo $menu4->name; ?></h5>
                <?php
                wp_nav_menu($args = array(
                    'theme_location' => 'footer-menu-4',
                    'container'      => false,
                    'depth'          => 1
                ));
                ?>
            </div>


            <div class="col social-col">
                <?php
                wp_nav_menu($args = array(
                    'theme_location' => 'footer-social-menu',
                    'menu-class'     => 'footer-social',
                    'link_before'    => '<span>',
                    'link_after'     => '</span>',
                    'container'      => false,
                    'depth'          => 1
                ));
                ?>
            </div>
        </div>
        <div class="footer-details">
            <div class="copyright">
                <span>&copy; Image One Planet <?php echo date('Y'); ?></span>
                <span><a href="<?php echo site_url(); ?>/privacy-policy">Privacy Policy</a></span>
                <span><a href="<?php echo site_url(); ?>/terms-and-conditions">Terms and Conditions</a></span>
            </div>
            <div class="gp-credit">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/geekpower-logo.svg" alt="Geek Power logo">
                <span>Website by <a href="https://www.in-toronto-web-design.ca/" target="_blank">Geek Power Web Design in Toronto</a></span>
            </div>
        </div> <!-- /footer-details -->
    </div>
</footer>





<?php
global $obj;
$site_messages = $obj->get_site_messages();
foreach ($site_messages as $message_key => $message){
?>    <span class="<?php echo $message_key; ?> hidden" ><?php echo $message; ?></span>
<?php
}
?>
<span class="stripe_publishable_key hidden" ><?php echo STRIPE_PUBLISHABLE_KEY; ?></span>

<img src="<?php echo get_template_directory_uri(); ?>/assets/images/loading.gif" class="loading-image hidden" alt="Dots">
<?php wp_footer(); ?>


<script type="text/javascript" src="https://js.stripe.com/v2/"></script>


</div> <!-- /custom-site-wrapper -->
</div> <!-- /custom-site-outer -->
</body>
</html>