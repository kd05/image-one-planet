<?php
/**
 * Template Name: User Submissions - Current Contest
 * Template Post Type: Page
 *
 */

$loginCheck->redirect_user_not_logged_in();
//$current_contest_id = $obj->get_active_contest_id();
//$user_submissions = $obj->get_user_submissions($current_contest_id);
$meta_array = array("art_user_id" => get_current_user_id() );
$user_submissions =  $obj->get_post_by_multiple_meta("contest_submission", $meta_array,-1);


get_header();
the_post();

?>

    <section class="page-content">
        <?php get_template_part( 'template-parts/user-sidebar-menu/user', 'sidebar' ); ?>
        <h1><?php the_title(); ?></h1>


            <?php  if(count($user_submissions) > 0) { ?>
            <div class="art-listing-wrapper">
                <?php foreach ($user_submissions as $submission) {
                        $submission_id = $submission->ID;
                        $title = get_the_title($submission_id);
                        $description = substr($obj->get_the_content_by_id($submission_id),0,100)."...";
                        $submission_img = current(wp_get_attachment_image_src( get_post_thumbnail_id($submission_id), 'medium' ));
                        $submission_img_full = current(wp_get_attachment_image_src( get_post_thumbnail_id($submission_id), 'custom-size-2000' ));
                        $year_taken = get_post_meta( $submission_id, "image-year-taken", true );
                    ?>

                    <div class="art-single-container">
                        <div class="art-single">
                            <div class="art-img open-lightbox voteable"
                                 data-full-img="<?php echo $submission_img_full; ?>"
                                 data-img-title="<?php echo $title; ?>">
                                <img src="<?php echo $submission_img; ?>" alt="">
                            </div>
                            <h5><?php echo $title; ?></h5>
                            <p><?php echo nl2br($description); ?></p>
                            <p><b>Year Taken :</b> <?php echo $year_taken; ?></p>
                        </div>
                    </div>

                    <?php } ?>
            </div>
        <?php } else {
            ?>   <div class="message-alert"><p><?php  echo $obj->get_site_messages("no_record"); ?></p></div>   <?php
        }    ?>


    </section>

<?php
get_template_part('template-parts/gallery/gallery', 'lightbox');
get_footer();
?>