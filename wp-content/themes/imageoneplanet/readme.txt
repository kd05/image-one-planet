Stripe Payment
-------------------

Added
--------

themes/(imageoneplanet)/Stripe-data
themes/(imageoneplanet)/Stripe-lib
themes/(imageoneplanet)/stripe-init.php


Updated - File
---------------------

themes/(imageoneplanet)/Stripe-lib/Stripe.php    (getDefaultCABundlePath() Function)







/*********************************************
 *      Post Type's Post metas
 **********************************************/


User Profile Gallery
--------------------------

1)  image-type
2)  listing-image-type
2)  image-year
3)  image-status
4)  image-price
5)  participant_user_id
6)  image-size
7)  image-frame


User Art
------------

1) image-year-taken
2) art_user_id



Contest Submissions
----------------------

1) image-year-taken
2) art_user_id
3) submission_social_link


User Payment
------------------

1)  payment_user_id
2)  payment_contest_id
3)  payment_type
4)  payment_price
5)  payment_tax
6)  upgrade_membership_time
7)  membership_end_date
8)  credit_package
9)  payment_stripe_id
10) payment_stripe_customer
11) payment_stripe_balance_transaction
12) payment_stripe_currency
13) payment_stripe_receipt_url
14) payment_total_price
15) payment_coupon_applied
16) payment_coupon_code
17) payment_coupon_percentage
18) payment_coupon_discount
19) payment_final_price









/************************************
 * Taxonomy Term metas
 *************************************/


 Contest Category
 ----------------------


 1)     contest_status
 2)     contest_mode
 3)     upload_main_image_id
 4)     contest_deadline
 5)     prize_data_gold
 6)     prize_data_silver
 6)     prize_data_bronze
 6)     sponsor_image_id
 7)     jury_image_1_id
 8)     jury_title_1
 9)     jury_website_1
 10)    jury_image_2_id
 11)    jury_title_2
 12)    jury_website_2
 13)    jury_image_3_id
 14)    jury_title_3
 15)    jury_website_3
 16)    jury_image_4_id
 17)    jury_title_4
 18)    jury_website_4
 19)    jury_image_5_id
 20)    jury_title_5
 21)    jury_website_5



Payment Type
-----------------------

1)  payment_type
2)  payment_price
3)  upgrade_membership_time
4)  credit_package






/************************************
 * Users Profile - User meta data
 *************************************/

1)  country
2)  website-link
3)  facebook-link
4)  instagram-link
5)  birth-date
6)  featured
7)  artist-category
8)  photographer-category
9)  headshot
10) gallery_name






Work TODO:

1) Cron File Run
2) Need live Stripe Keys (Secret & Publishable key)
3) Inner Pages Left
4) Home Page Add Art for the featured user
5)







Design TODO:

1) Profile Page
2) Edit Profile
8) Artists /photographer







Doubts

-------------------

1) Pop Up images





---------------------

Template

gallery/

Only -> mostpopular & top20

others??

-------------------------

Seventh Panel in the Contest Template??

------------------------------
