<?php
/**
 * Template Name: Contact Page
 * Template Post Type: Page
 *
 */

get_header();
the_post();



?>

    <div class="main-container" id="submit-entry">
        <section class="page-content">
            <h1><?php the_title(); ?></h1>


            <div class="upload-art-container">
                <form id="form-contact-page" class="contact-form">
                    <div class="message-alert"></div>

                    <input type="hidden" id="security" value="<?php echo wp_create_nonce('security-contact-nonce'); ?>">

                    <!-- Submit button-->
                    <div class="input-wrapper">
                        <label for="full-name">Full Name*</label>
                        <input type="text"  class="required" placeholder="Full Name*"  name="full-name" id="full-name" value="">
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper">
                        <label for="email-address">Email*</label>
                        <input type="email"  class="required chk-email" placeholder="Email*" name="email-address" id="email-address" value="">
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper">
                        <label for="phone-no">Phone Number*</label>
                        <input type="text"  class="required" placeholder="Phone Number*" name="phone-no" id="phone-no" value="">
                    </div> <!-- /input-wrapper -->

                    <div class="input-wrapper">
                        <label for="message">Your Message</label>
                        <textarea name="message" placeholder="Your Message" id="message"></textarea>
                    </div> <!-- /input-wrapper -->

                    <div class="button-wrapper">
                        <input type="button" value="Send" tabindex="5" id="contact-submit" name="contact-submit" class="thread-button button blue" />
                    </div> <!-- /button-wrapper -->
                </form>

                <div class="instruction-page-content">
                    <?php  the_content(); ?>
                </div>

            </div>

        </section>
    </div>  <!-- /main-container -->

<?php get_footer(); ?>