<?php


//phpinfo();
require_once ('wp-config.php');
global $obj;

//$user_roles = array("participant","jury");
$user_query = $obj->get_all_users_excludin_administrator();

if ( ! empty( $user_query->results ) ) {
    foreach ($user_query->results as $user) {

        $user_id = $user->ID;
        $author_info = get_userdata($user_id);
        $featured_user = esc_attr( get_the_author_meta( 'featured', $user_id) );
        $todays_date = $obj->convert_date_format();
        if($featured_user == "yes"){
            echo "<br>Todays date: ".$todays_date;
            echo '<h3>' . $author_info->first_name . ' ' . $author_info->last_name . " --User id :".$user_id.'</h3>';

            //            Check if Admin has set the use cron igonre yes
            if(esc_attr( get_the_author_meta( 'cron-ignore', $user_id ) ) != "yes") {
                echo $user_membership_end_date = $obj->get_user_featured_membership_end_date($user_id);
                if(!$user_membership_end_date || ($user_membership_end_date  && $todays_date > $user_membership_end_date)){
                    echo '<h2>Expired..</h2>';
                    $email_template->user_id = $user_id;
                    $email_template->email_user_featured_member_expired();
                    update_user_meta( $user_id, 'featured', "no" );
                }
            }

        }
        //        Make sure if somehow by mistake Admin turns featured user to Non Featured User
        else{
            $user_membership_end_date = $obj->get_user_featured_membership_end_date($user_id);
            if(($user_membership_end_date  && $todays_date < $user_membership_end_date)){
                echo '<h2>Set to Featured..</h2>';
                update_user_meta( $user_id, 'featured', "yes" );
            }
        }
        echo "<hr>";
    }
}
